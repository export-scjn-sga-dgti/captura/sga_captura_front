import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProcesamientoTextoService } from 'src/app/services/procesamiento-texto.service';

import { Materia } from 'src/app/models/normativaModel';

@Component({
  selector: 'app-clasificacion-materia',
  templateUrl: './clasificacion-materia.component.html',
  styleUrls: ['./clasificacion-materia.component.scss']
})
export class ClasificacionMateriaComponent implements OnInit {

  header:string = ""
  options: any
  catalogo: any = []
  displayModal: boolean = false
  clasificacionRequerid = {
    materiaRegulacion: false
  }
  materiaActual: Materia = new Materia()
  data: any;
  viewDisplay: boolean = false;
  catalogos: any = {};
  tipo: string = '';
  rubro: string = '';
  isEditMateria: boolean = false;
  isRequerid: boolean = false;

  @Output() closeMateria = new EventEmitter<{
    materia: Materia;
    open: boolean;
  }>();
  @Input() set openModal(data) {
    if (data) {
      this.materiaActual = new Materia()
      this.tipo = data.tipo;
      this.viewDisplay = data.open;
      this.isEditMateria = false;
      this.isRequerid = false;
      this.header = "Nueva clasificación"
      this.catalogo = data.catalogo
    }

  }

  get openModal() {
    console.log(this.data)
    return this.data;
  }
  @Output() closeEditMateria = new EventEmitter<{
    materia: Materia;
    open: boolean;
  }>();
  @Input() set dataMateria(data: any) {
    if (data) {
      this.catalogo = data.catalogo
      this.viewDisplay = data.open;
      this.materiaActual = new Materia()
      this.materiaActual = data.materia
      this.isEditMateria = true;
      this.isRequerid = false;
      this.header = "Editar clasificación"

    }
  }

  get dataRubro() {
    return null;
  }

  constructor(
      private textProcService: ProcesamientoTextoService,
    ) {}

  ngOnInit(): void {

  }

  crearMateria() {

    if(!this.materiaActual.materiaRegulacion){
      console.log("No existe")
    }
    this.materiaActual.temas = this.materiaActual.temas?this.materiaActual.temas.toString().split(","):[]

    if (this.materiaActual.materiaRegulacion) {
      this.viewDisplay = false;
      let dataInsert = {
        materia: this.materiaActual,
        open: false,
      };
      this.closeMateria.emit(dataInsert);
    } else {
      this.clasificacionRequerid.materiaRegulacion = true
    }
  }

  editMateria() {
    this.viewDisplay = false;
    this.materiaActual.temas = this.materiaActual.temas?this.materiaActual.temas.toString().split(","):[]
    let dataInsert = {
      materia: this.materiaActual,
      open: false,
    };
    this.isEditMateria = false;
    this.closeEditMateria.emit(dataInsert);
  }

  public validateChips(data: any) {
    let input = document.getElementsByClassName('p-chips-input-token')[0].childNodes[0] as HTMLElement
    input['value'] = ''
  }

}
