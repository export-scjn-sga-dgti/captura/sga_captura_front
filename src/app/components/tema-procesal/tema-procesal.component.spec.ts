import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemaProcesalComponent } from './tema-procesal.component';

describe('TemaProcesalComponent', () => {
  let component: TemaProcesalComponent;
  let fixture: ComponentFixture<TemaProcesalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemaProcesalComponent ],
      imports:[CommonModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemaProcesalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
