import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-tema-procesal',
  templateUrl: './tema-procesal.component.html',
  styleUrls: ['./tema-procesal.component.scss']
})
export class TemaProcesalComponent implements OnInit {

  
  options: any
  displayModal: boolean = false

  @Input()  set modal(data:any){
    if(data?.open){    
      this.options = data
      this.displayModal = data.open
    }
  }

  get modal(){
    return this.options
  }

  constructor() { }

  ngOnInit(): void {
  }

  closeModal(){

  }

}
