import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { RubrosEfectoSentencia } from 'src/app/models/votacionDerechosHumanos';
import { ProcesamientoTextoService } from 'src/app/services/procesamiento-texto.service';

@Component({
  selector: 'app-rubros-tematicos',
  templateUrl: './rubros-tematicos.component.html',
  styleUrls: ['./rubros-tematicos.component.scss'],
})
export class RubrosTematicosComponent implements OnInit {
  data: any;
  viewDisplay: boolean = false;
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  catalogos: any = {};
  tipo: string = '';
  rubroTematico: RubrosEfectoSentencia = new RubrosEfectoSentencia();
  rubro: string = '';
  isEditRubro: boolean = false;
  isRequerid: boolean = false;
  favor: boolean
  contra: boolean

  @Output() closeRubro = new EventEmitter<{
    tipo: string;
    rubro: RubrosEfectoSentencia;
    open: boolean;
  }>();
  @Input() set openModal(data) {
    if (data) {
      this.rubroTematico = new RubrosEfectoSentencia();
      this.tipo = data.tipo;
      this.viewDisplay = data.open;
      this.isEditRubro = false;
      this.isRequerid = false;
      this.contra = false
      this.favor = false
    }
  }

  get openModal() {
    return this.data;
  }
  @Output() closeEditRubro = new EventEmitter<{
    tipo: string;
    rubro: RubrosEfectoSentencia;
    open: boolean;
  }>();
  @Input() set dataRubro(data: any) {
    if (data) {
      this.viewDisplay = data.open;
      this.rubroTematico = new RubrosEfectoSentencia();
      this.rubroTematico = data.rubros;
      this.isRequerid = false;
      this.isEditRubro = data.edicion;
      this.isRequerid = false;
    }
  }

  get dataRubro() {
    return null;
  }

  constructor(private textProcService: ProcesamientoTextoService) {}

  ngOnInit(): void {}

  agregarRubro() {
    if (this.rubro.includes('\n')) {
      const linesRubros = this.rubro.split('\n');
      linesRubros.forEach((texto) => {
        if (texto != '') this.rubroTematico.rubrosTematicos.push(texto);
      });
    } else {
      if (this.rubro.trim())
        this.rubroTematico.rubrosTematicos.push(this.rubro);
    }
    this.rubro = '';
  }

  crearRubroTematico() {
    if (
      this.rubroTematico.textoVotacion &&
      (this.rubroTematico.votacionEnContra.length >= 1 ||
        this.rubroTematico.votacionFavor.length >= 1)
    ) {
      this.rubroTematico.tipoRubro =
        this.tipo === 'efectos'
          ? 'rubrosEfectoSentencia'
          : 'rubrosExtencionInvalidez';
      this.viewDisplay = false;
      let dataInsert = {
        tipo: this.tipo,
        rubro: this.rubroTematico,
        open: false,
      };
      this.closeRubro.emit(dataInsert);
    } else {
      const panel = document
        .getElementById('panel')
        .scrollIntoView({ behavior: 'smooth' });
      this.isRequerid = true;
    }
  }

  editRubroTematico() {
    this.viewDisplay = false;
    let dataInsert = {
      tipo: this.tipo,
      rubro: this.rubroTematico,
      open: false,
    };
    this.isEditRubro = false;
    this.closeEditRubro.emit(dataInsert);
  }

  processText() {
    if (this.rubroTematico.textoVotacion) {
      let data = {
        text: this.rubroTematico.textoVotacion,
        fechaResolucion: '',
      };
      this.textProcService.processTex(data).subscribe(
        (resp) => {
          this.rubroTematico.ministros = resp.ministros;
          this.rubroTematico.votacionEnContra = resp.ministrosEnContra
            ? resp.ministrosEnContra
            : [];
          this.rubroTematico.votacionFavor = resp.ministrosAFavor
            ? resp.ministrosAFavor
            : [];
        },
        (error) => {
          console.log('Ha ocurrdio algo inesperado');
        }
      );
    }
  }

  markAllFavor({checked}){
    if(checked){
      this.rubroTematico.votacionFavor = this.rubroTematico.ministros
      this.rubroTematico.votacionEnContra = []
    } else {
      this.rubroTematico.votacionFavor = []
    }
  }

  markAllContra({checked}){
    if(checked){
      this.rubroTematico.votacionEnContra = this.rubroTematico.ministros
      this.rubroTematico.votacionFavor = []
    } else {
      this.rubroTematico.votacionEnContra = []
    }
  }

}
