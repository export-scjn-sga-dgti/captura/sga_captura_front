import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemaFondoComponent } from './tema-fondo.component';

describe('TemaFondoComponent', () => {
  let component: TemaFondoComponent;
  let fixture: ComponentFixture<TemaFondoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemaFondoComponent ],
      imports:[CommonModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemaFondoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
