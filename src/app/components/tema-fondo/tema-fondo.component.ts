import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-tema-fondo',
  templateUrl: './tema-fondo.component.html',
  styleUrls: ['./tema-fondo.component.scss']
})
export class TemaFondoComponent implements OnInit {

  options: any
  displayModal:boolean = false
  @Input()  set modal(data:any){
    if(data?.open){
      this.options = data
      this.displayModal = data.open
    }
  }

  get modal(){
    return this.options
  }

  constructor() { }

  ngOnInit(): void {
  }

  closeModal(){}

}
