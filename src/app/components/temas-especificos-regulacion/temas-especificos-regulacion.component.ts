import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-temas-especificos-regulacion',
  templateUrl: './temas-especificos-regulacion.component.html',
  styleUrls: ['./temas-especificos-regulacion.component.scss']
})
export class TemasEspecificosRegulacionComponent implements OnInit {
  header:string = ""
  options: any
  displayModal: boolean = false
  clasificacionRequerid = {
    materiaRegulacion: false
  }
  data: any;
  viewDisplay: boolean = false;
  especificos: any = [];
  tipo: string = '';
  rubro: string = '';
  isEditTema: boolean = false;
  isRequerid: boolean = false;
  tema:string="";
  requerid:boolean=false

  constructor() { }

  ngOnInit(): void {
  }

  @Output() closeTema = new EventEmitter<{
    tema:string,
    open: boolean;
  }>();
  @Input() set openModal(data) {
    this.tema = ""
    this.data = data

    if (data) {
      this.tipo = data.tipo;
      this.viewDisplay = data.open;
      this.isEditTema = false;
      this.isRequerid = false;
      this.header = "Nuevo tema"
      this.especificos = data.catalogo
    }
  }

  @Output() closeEditTema = new EventEmitter<{
    index:number,
    tema:string,
    open: boolean;
  }>();
  @Input() set dataTema(data: any) {
    this.data = data
    if (data) {
      this.tema = data.materia;
      this.viewDisplay = data.open;
      this.isEditTema = true;
      this.isRequerid = false;
      this.header = "Editar tema"
      this.especificos = data.catalogo
    }
  }

  crearTema() {
    if(this.tema){
      this.viewDisplay = false;
      let dataInsert = {
        tema: this.tema,
        open: false,
      };
      this.closeTema.emit(dataInsert);
      this.tema = ""
    }else{
      this.requerid=true
    }
  }

  editTema() {
    if(this.tema){
      this.viewDisplay = false;
      let dataInsert = {
        index: this.data.index,
        tema: this.tema,
        open: false,
      };
      this.closeEditTema.emit(dataInsert);
      this.isEditTema = false;
      this.tema = ""
    }else{

    }
  }

}
