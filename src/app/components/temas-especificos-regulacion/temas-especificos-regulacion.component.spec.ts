import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemasEspecificosRegulacionComponent } from './temas-especificos-regulacion.component';

describe('TemasEspecificosRegulacionComponent', () => {
  let component: TemasEspecificosRegulacionComponent;
  let fixture: ComponentFixture<TemasEspecificosRegulacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemasEspecificosRegulacionComponent ],
      imports: [CommonModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemasEspecificosRegulacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
