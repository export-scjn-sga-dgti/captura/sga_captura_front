import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';

const config = {
  "accept": "Si",
  "reject": "No",
  "choose": "Choose",
  "upload": "Subir",
  "cancel": "Cancelar",
  "dayNames": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
  "dayNamesShort": ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
  "dayNamesMin": ["Dom","Lun","Mar","Mie","Jue","Vie","Sab"],
  "monthNames": ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
  "monthNamesShort": ["Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
  "dateFormat": "dd-mm-yy",
  "firstDayOfWeek": 0,
  "today": "Hoy",
  "weekHeader": "Sm",
  "weak": 'Semana',
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  
  constructor(private confiPrng: PrimeNGConfig){

  }

  ngOnInit(): void {
      this.confiPrng.setTranslation(config)
      this.confiPrng.ripple = true;
  }

}
