import { Inject, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { SpinnerService } from './services/spinner.service';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class InterceptorHttp implements HttpInterceptor {
  constructor(
    private spinner: SpinnerService,
    @Inject(LOCAL_STORAGE)
    private storageService: WebStorageService
  ) { }

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    this.spinner.setData = true;
    let token: string = ""


    if (environment.activeKeycloak) {
      token = this.storageService.get('token');
    } else {
      token =
        'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJhZDVVUWNuNFROMjdmakVoSk5nSEs4TDB5M19wdXNKaGowV09TY0xEVUxrIn0.eyJqdGkiOiIwM2IzNjZiZC01ZDhlLTQwNWMtYjViZi0wZWU0MGJiOTIyNDMiLCJleHAiOjE2NzAzNjkwOTAsIm5iZiI6MCwiaWF0IjoxNjcwMzQwMjkwLCJpc3MiOiJodHRwOi8vYWNjb3VudHMuc2Nqbi5wamYuZ29iLm14L2F1dGgvcmVhbG1zL3Njam4iLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiYWE3YzVlMDktYjc5MS00N2Q5LWEwMTEtYWY1MWVkMmE3NzM2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoic2VydmljaW9zLXVnYWNqIiwibm9uY2UiOiJkOTJhNmEzMy04NDE5LTRkNTQtOTc0ZS00OTdkNmMyNGIxYmIiLCJhdXRoX3RpbWUiOjE2NzAzNDAyODcsInNlc3Npb25fc3RhdGUiOiIzZTRkNzc1MS02Y2MzLTQ4YjMtODUxYi0wYjU5YTgxMDM0NGEiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHA6Ly8xNzIuMTYuMjE0LjIzIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJQR0RBVF9JTlNUSVRVQ0lPTkVTX0FMVEEiLCJQR0RBVF9SRVNQVUVTVEFTX0VESUNJT04iLCJQR0RBVF9JTlNUSVRVQ0lPTkVTX0NPTlNVTFRBIiwiUEdEQVRfUEVSU09OQVNfQUxUQSIsIlBHREFUX1RBQkxFUk9fQ09OU1VMVEEiLCJQR0RBVF9BUkVBU19FRElDSU9OIiwiUEdEQVRfQVJFQVNfQ09OU1VMVEEiLCJQR0RBVF9BU1VOVE9FVElRVUVUQVNfRURJQ0lPTiIsIlJPTEVfVVNFUiIsIm9mZmxpbmVfYWNjZXNzIiwiUEdEQVRfQVNVTlRPQU5FWE9TX0FMVEEiLCJQR0RBVF9QRVJTT05BU19DT05TVUxUQSIsInVtYV9hdXRob3JpemF0aW9uIiwiUEdEQVRfQVNVTlRPU19DT05TVUxUQSIsIlBHREFUX1RVUk5PU19BTFRBIiwiUEdEQVRfVVNFUiIsIlBHREFUX1BST1lFQ1RPU19FRElDSU9OIiwiUEdEQVRfUEVSU09OQVNfRURJQ0lPTiIsIlBHREFUX0FSRUFTX0FMVEEiLCJQR0RBVF9NRVNBXzEwMDAzMTYiLCJST0xFX1NFTkRFUiIsIlBHREFUX1RVUk5PU19FRElDSU9OIiwiUEdEQVRfUFJPWUVDVE9TX0FMVEEiLCJQR0RBVF9JTlNUSVRVQ0lPTkVTX0VESUNJT04iLCJQR0RBVF9DT0xBQk9SQURPUkVTX0FMVEEiLCJQR0RBVF9BU1VOVE9TX0VESUNJT04iLCJQR0RBVF9TT0xJQ0lUVURFU19FRElDSU9OIiwiUEdEQVRfQVNVTlRPQU5FWE9TX0JBSkEiLCJQR0RBVF9CVVNDQURPUiIsIlBHREFUX1NPTElDSVRVREVTX0NPTlNVTFRBIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6IkFEUklBTiBNQVJUSU4gVE9MRURPIEJBVVRJU1RBIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYXRvbGVkb2IiLCJnaXZlbl9uYW1lIjoiQURSSUFOIE1BUlRJTiIsImZhbWlseV9uYW1lIjoiVE9MRURPIEJBVVRJU1RBIiwiZW1haWwiOiJhdG9sZWRvYkBtYWlsLnNjam4uZ29iLm14In0.FxvWIKUD7FB6n54wbXsRL3hiVtof6-xcjSUTAJBIZb4UuQolNzQ2zv3CxPsqRjqdBrqrvv__aSNifZIwH4JRGArYp-iTj0xc4IBdOPK-bziPNuD_CmH7LSBEZsgxdmV1cvDiIATk1OGSGB3M31Dv2WE1cLsn3Qr5q-XQ1MGCDOTEMCaBG5d2tVZf8Wxh90TgN_Viwknc9mXj2R9SvdWjFEcthYFFEKDNbno6v4IV-BcAjNEjhzPpeJqVe3rVmhLAbBwRX_7gaelM_GLmSPRKDfYdaLaBjBcRpsRwnpHgN0O8C5N5vMRJ9tPEtvnFRIHwAcv3bZjWTY0rMMXh9fXQRw';
    }

    let headers: { [name: string]: string | string[] } = {
      accept: 'application/json',
      'cache-control':
        'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
      pragma: 'no-cache',
      expires: '0',
    };

    headers = {
      Authorization: `Bearer ${token}`,
      ...headers,
    };

    request = request.clone({
      headers: new HttpHeaders(headers),
    });

    return next.handle(request).pipe(
      finalize(() => {
        this.spinner.setData = false;
      })
    );
  }
}
