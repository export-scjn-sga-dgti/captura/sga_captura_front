export function Validate(expediente:string){
    const validate = new RegExp("^[0-9]{1,6}/[0-9]{4}$")
    return validate.test(expediente)
}