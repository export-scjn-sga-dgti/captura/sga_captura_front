export class Catalogo{
    id: string
    nombre:string
    catalogo:string
    info:string
    modulo:string
}

export class Modulos {
  modulo: string;
}

export class ConverStringArray{
    proccessArray(nameCatalogo:Catalogo[]):string[]{
        let catalogo: string[] = []
        nameCatalogo.forEach(data =>{
            catalogo.push(data.nombre.trim())
        })
        return catalogo
    }

    proccessArrayColeccionCampo(nameCatalogo:Catalogo[]):string[]{
      let catalogo: string[] = []
      nameCatalogo.forEach(data =>{
          catalogo.push(data.info)
      })
      return catalogo
    }

    proccessArrayModulo(nameCatalogo:Modulos[]):string[]{
      let catalogo: string[] = []
      nameCatalogo.forEach(data =>{
          catalogo.push(data.modulo)
      })
      return catalogo
  }
}
