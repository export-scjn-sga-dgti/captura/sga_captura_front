import { TestBed } from '@angular/core/testing';

import { TaquigraficaApiService } from './taquigrafica-api.service';

describe('TaquigraficaApiService', () => {
  let service: TaquigraficaApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaquigraficaApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
