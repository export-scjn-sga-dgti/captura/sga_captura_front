import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Acuerdo, AllAcuerdoModel } from '../models/acuerdosModel';

const api: string = environment.url

@Injectable({
  providedIn: 'root'
})
export class AcuerdosApiService {

  private urlAcuerdos:string = `${api}/api/sga/acuerdos/`
  private httpHeaders = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  )
  constructor(private http:HttpClient) { }

  getAllAcuerdos(pagina: number = 1 , size: number = 10, filtros?: string): Observable<any>{
    let urlWithFilter: string = ""
    filtros ? 
      urlWithFilter = `${this.urlAcuerdos}?page=${pagina}&size=${size}&filtros=${filtros.slice(0,filtros.length-1)}`
      :
      urlWithFilter = `${this.urlAcuerdos}?page=${pagina}&size=${size}`
    return this.http.get<AllAcuerdoModel>(urlWithFilter,{headers: this.httpHeaders})

  }

  getAcuerdoByID(id: string): Observable<any>{
    return this.http.get<Acuerdo>(`${this.urlAcuerdos}${id}`,{headers: this.httpHeaders})
  }

  postAcuerdo(acuerdo: Acuerdo): Observable<Object>{
    return this.http.post<Object>(this.urlAcuerdos,acuerdo,{headers: this.httpHeaders})
  }

  updateAcuerdo(acuerdo: Acuerdo): Observable<any>{
    return this.http.put<Acuerdo>(this.urlAcuerdos,acuerdo,{headers: this.httpHeaders})
  }

  deleteAcuerdoByID(id: string):Observable<any>{
    return this.http.delete<Acuerdo>(`${this.urlAcuerdos}${id}`,{headers: this.httpHeaders})
  }

}
