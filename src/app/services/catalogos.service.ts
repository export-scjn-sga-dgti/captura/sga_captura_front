import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Catalogo } from '../models/catalogos';

const apiUrl: string = `${environment.url}`;

@Injectable({
  providedIn: 'root',
})
export class CatalogosService {
  private urlSentencia: string = `${apiUrl}/api/sga/catalogo/sentencias`;
  private urlTaquigraficas: string = `${apiUrl}/api/sga/catalogo/versionesT`;
  private urlVoto: string = `${apiUrl}/api/sga/catalogo/votos`;
  private urlNormativa: string = `${apiUrl}/api/sga/catalogo/diversa`;
  private urlSeguimiento: string = `${apiUrl}/api/sga/catalogo/seguimiento`;
  private urlAcuerdos: string = `${apiUrl}/api/sga/catalogo/acuerdos`;
  private urlCatalog: string = `${apiUrl}/api/sga/catalogo/nombre/`;
  private urlCreateCatalogo: string = `${apiUrl}/api/sga/catalogo/`;
  private urlCatalogoByColeccionCampo: string = `${apiUrl}/api/sga/catalogo/campo-coleccion/`;

  constructor(private http: HttpClient) { }

  getCatalogosSentencias(): Observable<any> {
    return this.http.get(this.urlSentencia);
  }

  getCatalogosTaquigraficas(fecha: string): Observable<any> {
    return this.http.get(`${this.urlTaquigraficas}?fecha=${fecha}`);
  }

  getCatalogosVotos(fecha?: string): Observable<any> {
    return this.http.get(fecha ? `${this.urlVoto}?fecha=${fecha}` : `${this.urlVoto}`);
  }

  getCatalogosNormativas(): Observable<any> {
    return this.http.get(this.urlNormativa);
  }

  getCatalogosSeguimiento(): Observable<any> {
    return this.http.get(this.urlSeguimiento);
  }


  getCatalogosAcuerdos(fecha?: string): Observable<any> {
    return this.http.get(fecha ? `${this.urlAcuerdos}?fecha=${fecha}` : `${this.urlAcuerdos}`);
  }

  getCatalogosByColeccionCampo(info: string, coleccion: string, campo: string): Observable<any> {
    return this.http.get(`${this.urlCatalogoByColeccionCampo}?info=${info}&coleccion=${coleccion}&campo=${campo}`);
  }

  getCatalogoByName(id: string): Observable<Catalogo[]> {
    return this.http.get<Catalogo[]>(`${this.urlCatalog}${id}`);
  }

  postCatalogo(data: Catalogo): Observable<Catalogo> {
    return this.http.post<Catalogo>(this.urlCreateCatalogo, data);
  }

  updateCatalogo(data: Catalogo): Observable<Catalogo> {
    return this.http.put<Catalogo>(`${this.urlCreateCatalogo}${data.id}`, data);
  }

  deleteCatalogo(id: string): Observable<boolean> {
    return this.http.delete<boolean>(`${this.urlCreateCatalogo}${id}`);
  }



}
