import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';

declare var Keycloak: any;

@Injectable({
  providedIn: 'root',
})
export class KeycloakService {
  private keycloakAuth: any;
  public isCon: boolean;

  constructor(
    @Inject(LOCAL_STORAGE)
    private sessionStorage: WebStorageService
  ) {}

  init(): Promise<any> {
    return new Promise((resolve, reject) => {
      //  console
      const config = {
        url: 'https://accounts.scjn.gob.mx/auth',
        realm: 'scjn',
        clientId: 'servicios-ugacj',
        credentials: {
          secret: 'f3924fe0-f485-45a0-a843-33a4f1028761',
        },
      };
      this.keycloakAuth = new Keycloak(config);
      this.keycloakAuth
        .init({
          checkLoginIframe: false,
          initOptions: {
            onLoad: 'check-sso',
            silentCheckSsoRedirectUri:
              window.location.origin + '/assets/silent-check-sso.html',
          },
        })
        .success((e) => {
          this.sessionStorage.set('token', this.keycloakAuth.token);
          this.isCon = e;
          resolve(e);
        })
        .error(() => {
          reject();
        });
    });
  }

  setToken(): string {
    return this.keycloakAuth.token;
  }

  getToken(): string {
    return this.sessionStorage.get('token');
  }

  login() {
    return this.keycloakAuth.createLoginUrl();
  }

  tokenParsed() {
    return this.keycloakAuth.idTokenParsed;
  }

  getActivate(): boolean {
    return this.keycloakAuth.authenticated;
  }

  logout() {
    this.keycloakAuth.logout();
    localStorage.removeItem("username");
    localStorage.removeItem("initialname");
  }
}
