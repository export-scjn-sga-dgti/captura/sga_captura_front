import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AllTaquigraficas, CreateAsuntoAbordado, CreateTaquigrafica, Taquigrafica } from '../models/taquigraficaModel';

const api: string = `${environment.url}`

@Injectable({
  providedIn: 'root'
})

export class TaquigraficaApiService {

  private urlTaquigraficas: string = `${api}/api/sga/versiones_taquigraficas/`;
  private urlAsuntoAbordado: string = `${api}/api/sga/asuntos_abordados/`;
  private urlTema:string = `${api}/api/sga/asuntos_abordados/vtaq/`

  private httpHeaders = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  )
  constructor(private http: HttpClient) { }

  getAllTaquigraficas(page: number = 1, size: number = 10, filtros?: string): Observable<AllTaquigraficas> {
    let urlWithFilter: string = ""
    filtros ?
      urlWithFilter = `${this.urlTaquigraficas}?page=${page}&size=${size}&filtros=${filtros.slice(0,filtros.length-1)}`
      :
      urlWithFilter = `${this.urlTaquigraficas}?page=${page}&size=${size}`
      ;
    return this.http.get<AllTaquigraficas>(urlWithFilter, { headers: this.httpHeaders })
  }

  getTaquigraficaByID(id: string): Observable<Taquigrafica> {
    return this.http.get<Taquigrafica>(`${this.urlTaquigraficas}${id}`, { headers: this.httpHeaders })
  }

  deleteTaquigrafica(id: string): Observable<boolean> {
    return this.http.delete<boolean>(`${this.urlTaquigraficas}${id}`, { headers: this.httpHeaders })
  }

  postTaquigrafica(taquigrafica: CreateTaquigrafica): Observable<any> {
    return this.http.post<any>(this.urlTaquigraficas, taquigrafica, { headers: this.httpHeaders })
  }

  putTaquigrafica(taquigrafica: CreateTaquigrafica): Observable<any> {
    return this.http.put<any>(this.urlTaquigraficas, taquigrafica, { headers: this.httpHeaders })
  }

  postAsuntoAbordado(asunto: CreateAsuntoAbordado): Observable<any> {
    return this.http.post<any>(this.urlAsuntoAbordado, asunto, { headers: this.httpHeaders })
  }

  getAsuntoAbordadoByID(idVt: string, idAsunto: string): Observable<any> {
    return this.http.get<any>(`${this.urlAsuntoAbordado}${idVt}?idAsunto=${idAsunto}`)
  }

  deleteAsuntoAbordado(idVaq: string, idAsunto: string): Observable<boolean> {
    return this.http.delete<boolean>(`${this.urlAsuntoAbordado}${idVaq}/${idAsunto}`)
  }

  updateAsuntoAbordado(asunto: CreateAsuntoAbordado): Observable<any> {
    return this.http.put<any>(this.urlAsuntoAbordado, asunto, { headers: this.httpHeaders })
  }

  deleteTema(idVtaq:string,idAsunto:string,type:string,tema:string):Observable<any>{
    return this.http.delete<any>(`${this.urlTema}${idVtaq}/asunto/${idAsunto}/tipotema/${type}/tema/${tema}`)
  }

  updateTema(idVtaq:string,idAsunto:string,type:string,temas:string[]){
    return this.http.put<any>(`${this.urlTema}${idVtaq}/asunto/${idAsunto}/tipotema/${type}`,temas)
  }

}
