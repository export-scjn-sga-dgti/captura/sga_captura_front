import { TestBed } from '@angular/core/testing';

import { SentenciaApiService } from './sentencia-api.service';

describe('SentenciaApiService', () => {
  let service: SentenciaApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SentenciaApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
