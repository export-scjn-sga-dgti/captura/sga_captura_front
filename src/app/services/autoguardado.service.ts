import { Injectable } from '@angular/core';
import { Sentencia } from '../models/sentenciaModel';

@Injectable({
  providedIn: 'root'
})
export class AutoguardadoService {

  sentenciaData: Sentencia = new Sentencia()

  constructor() { }

  set infoSentencia(sentencia: Sentencia){
    this.sentenciaData = sentencia
  }

  get infoSentencia(): Sentencia{
    return this.sentenciaData
  }

}
