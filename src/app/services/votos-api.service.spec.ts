import { TestBed } from '@angular/core/testing';

import { VotosApiService } from './votos-api.service';

describe('VotosServiceService', () => {
  let service: VotosApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VotosApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
