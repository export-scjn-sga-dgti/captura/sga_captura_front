import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Seguimiento } from '../models/seguimiento';

@Injectable({
  providedIn: 'root'
})
export class SeguimientoService {

  private urlSeguimiento = `${environment.url}/api/sga/seguimiento/`

  constructor(private http: HttpClient) { }

  getAllSeguimiento(page:number = 1,size: number = 10,filtros?: string):Observable<Seguimiento>{
    return this.http.get<Seguimiento>(`${this.urlSeguimiento}?page=${page}&size=${size}${filtros ? `&filtros=${filtros}`:''}`)
  }

  getReporteExcel(): Observable<any>{
    return this.http.get(`${this.urlSeguimiento}informe-tematicas-asuntos`, {responseType: 'blob'});
  }

}
