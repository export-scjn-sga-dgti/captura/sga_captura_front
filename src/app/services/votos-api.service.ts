import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Sentencia, Votacion } from '../models/sentenciaModel';
import { Voto, AllVotoModel } from '../models/votosModel';

const api: string = environment.url

@Injectable({
  providedIn: 'root'
})

export class VotosApiService {

  private urlVoto:string = `${api}/api/sga/voto`
  private urlAutocompletado = `${api}/api/sga/autocompletado`
  private httpHeaders = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  )

  constructor(private http:HttpClient) {}

  getAllVotos(pagina: number = 1 , size: number = 10, filtros?: string): Observable<AllVotoModel>{
    let urlWithFilter: string = ""
    filtros ?
      urlWithFilter = `${this.urlVoto}?page=${pagina}&size=${size}&filtros=${filtros.slice(0,filtros.length-1)}`
      :
      urlWithFilter = `${this.urlVoto}?page=${pagina}&size=${size}`

    return this.http.get<AllVotoModel>(urlWithFilter,{headers: this.httpHeaders})
  }

  getVotoByID(id: string): Observable<Voto>{
    return this.http.get<Voto>(`${this.urlVoto}/${id}`,{headers: this.httpHeaders})
  }

  getVotoBySentencia(tipoAsunto: string, numExpediente: string): Observable<Voto>{
    let url: string = ""
    url = `${this.urlVoto}/votoBySentencia?tipoAsunto=${tipoAsunto}&numExpediente=${numExpediente}`

    return this.http.get<Voto>(url,{headers: this.httpHeaders})
  }

  postVoto(voto: Voto): Observable<Object>{
    return this.http.post<Object>(this.urlVoto,voto,{headers: this.httpHeaders})
  }

  updateVoto(voto: Voto): Observable<Voto>{
    return this.http.put<Voto>(this.urlVoto,voto,{headers: this.httpHeaders})
  }

  deleteVotoByID(id: string):Observable<any>{
    return this.http.delete<any>(`${this.urlVoto}/${id}`,{headers: this.httpHeaders})
  }

  validaSentencia(tipoAsunto: string, numExpediente: string): Observable<Sentencia>{
    let url: string = ""
    url = `${this.urlVoto}/sentencia?tipoAsunto=${tipoAsunto}&numExpediente=${numExpediente}`

    return this.http.get<Sentencia>(url,{headers: this.httpHeaders})
  }

  obtieneVotacion(id: string): Observable<Votacion>{
    let url: string = ""
    url = `${this.urlVoto}/votacionById?id=${id}`

    return this.http.get<Votacion>(url,{headers: this.httpHeaders})
  }
}
