import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  isLoading: boolean = false

  constructor() { }

  set setData(load:boolean){
    this.isLoading = load
  }

  get getData():boolean{
    return this.isLoading
  }

}
