import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Procesamiento, TextoProcesamiento } from '../models/procesamientoText';

@Injectable({
  providedIn: 'root',
})
export class ProcesamientoTextoService {
  private urlProcesamiento: string = `${environment.url}/api/sga/procesamiento-texto`;
  private httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
  });
  constructor(private http: HttpClient) {}

  setFechaResolucion(date: string) {
    localStorage.setItem('fecha', date);
  }

  getFechaResolucion(): string {
    return localStorage.getItem('fecha');
  }

  processTex(proceso: TextoProcesamiento): Observable<Procesamiento> {
    proceso.fechaResolucion = this.getFechaResolucion();
    return this.http.post<Procesamiento>(this.urlProcesamiento, proceso, {
      headers: this.httpHeaders,
    });
  }
}
