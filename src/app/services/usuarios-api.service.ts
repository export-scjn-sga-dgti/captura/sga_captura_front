import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Usuario } from '../models/usuarioModel';

const api: string = environment.url;

@Injectable({
  providedIn: 'root'
})
export class UsuariosApiService {
  private urlUsuarios = `${api}/api/sga/usuario/`;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  getAllUsuarios(
    page: number = 1,
    size: number = 10,
    filtros?: string
  ): Observable<Usuario> {
    let urlWithFilter: string = '';
    filtros
      ? (urlWithFilter = `${this.urlUsuarios}?page=${page}&size=${size}&filtros=${filtros}`)
      : (urlWithFilter = `${this.urlUsuarios}?page=${page}&size=${size}`);
    return this.http.get<Usuario>(urlWithFilter, {
      headers: this.httpHeaders,
    });
  }

  getUsuarioByID(id:string):Observable<Usuario>{
    return this.http.get<Usuario>(`${this.urlUsuarios}${id}`, {
      headers: this.httpHeaders,
    });
  }
  getUsuarioByUsername(username: string): Observable<Usuario> {
    return this.http.get<Usuario>(`${this.urlUsuarios}username/${username}`, {
      headers: this.httpHeaders,
    });
  }

  saveUsuario(usuario:Usuario):Observable<Usuario>{
    return this.http.post<Usuario>(`${this.urlUsuarios}`,usuario, {
      headers: this.httpHeaders,
    });
  }

  updateUsuario(usuario:Usuario):Observable<Usuario>{
    return this.http.put<Usuario>(this.urlUsuarios,usuario, {
      headers: this.httpHeaders,
    });
  }

  deleteUser(id:string):Observable<boolean>{
    return this.http.delete<any>(`${this.urlUsuarios}${id}`)
  }

}
