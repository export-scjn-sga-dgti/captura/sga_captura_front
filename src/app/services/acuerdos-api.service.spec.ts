import { TestBed } from '@angular/core/testing';

import { AcuerdosApiService } from './acuerdos-api.service';

describe('AcuerdosApiService', () => {
  let service: AcuerdosApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AcuerdosApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
