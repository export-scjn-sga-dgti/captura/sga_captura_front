import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor(private toast: MessageService) {}
  /**
   * @description
   *  Método para mostrar el toastr personalizado de acuerdo a los parámetros recibidos
   * @param severity: string -> success | warn | error
   * @param summary: string -> titulo
   * @param detail: string -> descripción
   */
  showToast(severity: string, summary: string, detail: string) {
    this.toast.add({
      key: 'alerts',
      severity: severity,
      summary: summary,
      detail: detail,
    });
  }
}
