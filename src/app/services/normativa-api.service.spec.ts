import { TestBed } from '@angular/core/testing';

import { NormativaApiService } from './normativa-api.service';

describe('NormativaApiService', () => {
  let service: NormativaApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NormativaApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
