import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DiversaNormativa, Normativa, Materia, AllNormativaModel } from '../models/normativaModel';

@Injectable({
  providedIn: 'root'
})

export class NormativaApiService {

  private urlNormativa:string = `${environment.url}/api/sga/diversa-normativa/`
  private urlMateriaRegulacion:string = `${environment.url}/api/sga/materia-regulacion/`

  private httpHeaders = new HttpHeaders(
    { 'Content-Type': 'application/json' }
  )

  constructor(private http:HttpClient) {}

  getAllNormativa(pagina: number = 1 , size: number = 10, filtros?: string): Observable<AllNormativaModel>{
    let urlWithFilter: string = ""
    filtros ? 
      urlWithFilter = `${this.urlNormativa}normativa/?page=${pagina}&size=${size}&filtros=${filtros}`
      :
      urlWithFilter = `${this.urlNormativa}normativa/?page=${pagina}&size=${size}`

    return this.http.get<AllNormativaModel>(urlWithFilter,{headers: this.httpHeaders})
  }

  getNormativaByID(id: string): Observable<DiversaNormativa>{
    return this.http.get<DiversaNormativa>(`${this.urlNormativa}${id}`,{headers: this.httpHeaders})
  }

  postNormativa(Normativa: Normativa): Observable<Object>{
    return this.http.post<Object>(this.urlNormativa,Normativa,{headers: this.httpHeaders})
  }

  updateNormativa(Normativa: Normativa): Observable<Normativa>{
    return this.http.put<Normativa>(this.urlNormativa,Normativa,{headers: this.httpHeaders})
  }

  deleteNormativaByID(id: string):Observable<any>{
    return this.http.delete<any>(`${this.urlNormativa}${id}`,{headers: this.httpHeaders})
  }


  getMateriaRegulacionByID(id: string): Observable<Materia>{
    return this.http.get<Materia>(`${this.urlMateriaRegulacion}${id}`,{headers: this.httpHeaders})
  }

  postMateriaRegulacion(Normativa: Materia): Observable<Object>{
    return this.http.post<Object>(this.urlMateriaRegulacion,Normativa,{headers: this.httpHeaders})
  }

  updateMateriaRegulacion(Normativa: Materia): Observable<Normativa>{
    return this.http.put<Normativa>(`${this.urlMateriaRegulacion}`,Normativa,{headers: this.httpHeaders})
  }

  deleteMateriaRegulacionByID(id: string):Observable<any>{
    return this.http.delete<any>(`${this.urlMateriaRegulacion}${id}`,{headers: this.httpHeaders})
  }
}
