import { TestBed } from '@angular/core/testing';

import { SaveFiltersService } from './save-filters.service';

describe('SaveFiltersService', () => {
  let service: SaveFiltersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaveFiltersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
