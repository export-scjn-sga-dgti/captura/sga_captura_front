import { TestBed } from '@angular/core/testing';

import { ProcesamientoTextoService } from './procesamiento-texto.service';

describe('ProcesamientoTextoService', () => {
  let service: ProcesamientoTextoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcesamientoTextoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
