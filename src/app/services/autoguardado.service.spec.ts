import { TestBed } from '@angular/core/testing';

import { AutoguardadoService } from './autoguardado.service';

describe('AutoguardadoService', () => {
  let service: AutoguardadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutoguardadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
