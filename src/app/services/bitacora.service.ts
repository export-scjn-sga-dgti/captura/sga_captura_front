import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BitacoraModel } from '../models/bitacora';

@Injectable({
  providedIn: 'root',
})
export class BitacoraService {
  private urlBitacora: string = `${environment.url}/api/sga/bitacora/`;

  constructor(private http: HttpClient) {}

  getAllBitacora(
    page: number,
    size: number,
    filtros?: string
  ): Observable<BitacoraModel> {
    let urlWithFilter: string = '';
    filtros
      ? (urlWithFilter = `${this.urlBitacora}?page=${page}&size=${size}&filtros=${filtros}`)
      : (urlWithFilter = `${this.urlBitacora}?page=${page}&size=${size}`);

    return this.http.get<BitacoraModel>(urlWithFilter);
  }

  getBitacoraById(id: string): Observable<any> {
    return this.http.get<any>(`${this.urlBitacora}${id}`);
  }
}
