import { Injectable } from '@angular/core';
import { SaveFilters } from '../models/save-filters';

@Injectable({
  providedIn: 'root'
})
export class SaveFiltersService {

  filters: SaveFilters = {} as SaveFilters

  constructor() { }

  set data(filter: SaveFilters){
    this.filters = filter
  }

  get data(): SaveFilters{
    return this.filters
  }
}
