import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DataHojas, HojasVotacion } from '../models/hojasModel';
import {
  AllSentenciaModel,
  Sentencia,
} from '../models/sentenciaModel';
import {
  SentenciaRegistro,
} from '../models/sentenciaRegistro';

const api: string = environment.url;

@Injectable({
  providedIn: 'root',
})

export class SentenciaApiService {
  private urlSentencia = `${api}/api/sga/sentencia`;
  private urlSentenciaVotacion = `${api}/api/sga/tablaVotaciones`;
  private urlRubrosTematicos = `${api}/api/sga/rubroTablaVotacion/`;
  private urlHojas = `${api}/api/sga/listado-hojas`;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  private urlReclamacion = `${api}/api/sga/reclamado`;
  private urlUpdateRubro = `${api}/api/sga/tablaVotaciones/orden-rubros`

  constructor(private http: HttpClient) { }

  sentencia: {
    id: string;
    model: SentenciaRegistro;
    modific: boolean;
  } = {} as {
    id: string;
    model: SentenciaRegistro;
    modific: boolean;
  };

  public set dataSentencia(data: {
    id: string;
    model: SentenciaRegistro;
    modific: boolean;
  }) {
    this.sentencia = data;
  }

  public get dataSentencia(): {
    id: string;
    model: SentenciaRegistro;
    modific: boolean;
  } {
    return this.sentencia;
  }

  getAllSentencias(
    page: number = 1,
    size: number = 10,
    filtros?: string
  ): Observable<AllSentenciaModel> {
    let urlWithFilter: string = '';
    filtros
      ? (urlWithFilter = `${this.urlSentencia}?page=${page}&size=${size}&filtros=${filtros.slice(0,filtros.length-1)}`)
      : (urlWithFilter = `${this.urlSentencia}?page=${page}&size=${size}`);
    return this.http.get<AllSentenciaModel>(urlWithFilter, {
      headers: this.httpHeaders,
    });
  }

  postSentencia(sentencia: Sentencia): Observable<Object> {
    return this.http.post<Object>(this.urlSentencia, sentencia, {
      headers: this.httpHeaders,
    });
  }

  putSentencia(sentencia: SentenciaRegistro): Observable<any> {
    return this.http.put<Object>(this.urlSentencia, sentencia, {
      headers: this.httpHeaders,
    });
  }

  getSentenciaByID(id: string): Observable<SentenciaRegistro> {
    return this.http.get<SentenciaRegistro>(`${this.urlSentencia}/${id}`, {
      headers: this.httpHeaders,
    });
  }

  deleteSentenciaByID(id: string): Observable<any> {
    return this.http.delete<any>(`${this.urlSentencia}/${id}`, {
      headers: this.httpHeaders,
    });
  }

  postSentenciaVotacion(votacion: any): Observable<any> {
    return this.http.post<any>(this.urlSentenciaVotacion, votacion, {
      headers: this.httpHeaders,
    });
  }

  putSentenciaVotacion(votacion: any): Observable<any> {
    return this.http.put<any>(this.urlSentenciaVotacion, votacion, {
      headers: this.httpHeaders,
    });
  }

  getSentenciaVotacion(id: string): Observable<any> {
    return this.http.get<any>(`${this.urlSentenciaVotacion}/${id}`);
  }

  deleteSentenciaVotacion(
    idVotacion: string,
    idSentencia: string
  ): Observable<any> {
    return this.http.delete<any>(
      `${this.urlSentenciaVotacion}/votacion/${idVotacion}/sentencia/${idSentencia}`
    );
  }

  postRubroTematico(rubro: any): Observable<any> {
    return this.http.post<any>(this.urlRubrosTematicos, rubro, {
      headers: this.httpHeaders,
    });
  }

  putRubroTematico(rubro: any): Observable<any> {
    return this.http.put<any>(this.urlRubrosTematicos, rubro, {
      headers: this.httpHeaders,
    });
  }

  deleteRubroTematico(idRubro: string, idVotacion: string, idSentencia: string): Observable<any> {
    return this.http.delete<any>(
      `${this.urlRubrosTematicos}rubro/${idRubro}/votacion/${idVotacion}/sentencia/${idSentencia}`
    );
  }

  getHojasVotacion(data: DataHojas): Observable<HojasVotacion[]> {
    return this.http.post<HojasVotacion[]>(this.urlHojas, data);
  }

  getReclamacion(data: DataHojas): Observable<string> {
    return this.http.post<string>(this.urlReclamacion, data);
  }

  updateOrderRubro(data: { id: string, rubrosTematicosPrincipal: string[] }): Observable<boolean> {
    return this.http.put<boolean>(this.urlUpdateRubro, data, { headers: this.httpHeaders })
  }

}
