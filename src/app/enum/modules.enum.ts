export enum Modules{
    admin = 'administrador',
    sentencia = 'sentencia',
    votos = 'votos',
    taquigraficas = 'taquigrafica',
    normativa = 'normativa',
    seguimiento = 'seguimiento',
    acuerdos = 'acuerdos',
    bitacora = 'bitacora',
    catalogo = 'catalogos'
}