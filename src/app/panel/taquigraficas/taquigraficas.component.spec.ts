import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TaquigraficasComponent } from './taquigraficas.component';

describe('TaquigraficasComponent', () => {
  let component: TaquigraficasComponent;
  let fixture: ComponentFixture<TaquigraficasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaquigraficasComponent ],
      imports: [HttpClientTestingModule,CommonModule,RouterTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaquigraficasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
