import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConverStringArray } from 'src/app/helper/converArrayObject';
import { ScrollTop } from 'src/app/helper/scrollTop';
import { CreateTaquigrafica } from 'src/app/models/taquigraficaModel';
import { CatalogosService } from 'src/app/services/catalogos.service';

import { TaquigraficaApiService } from 'src/app/services/taquigrafica-api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-taquigrafica-registro',
  templateUrl: './taquigrafica-registro.component.html',
  styleUrls: ['./taquigrafica-registro.component.scss'],
  providers: [ConverStringArray],
})
export class TaquigraficaRegistroComponent implements OnInit {
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  scroll: ScrollTop = new ScrollTop();
  catalogos: any = {};
  taquigrafica: CreateTaquigrafica = new CreateTaquigrafica();
  requerid = {
    fecha: false,
    duracion: false,
  };
  validPonente: boolean = true;
  ministros: any[] = []

  constructor(
    private catalogoService: CatalogosService,
    private taquigraficaService: TaquigraficaApiService,
    private conver: ConverStringArray,
    private route: Router,
    private datePipe: DatePipe,
    private toast: ToastService
  ) {
    this.scroll.scrollTo();
  }
  ngOnInit(): void {
    const fecha = this.datePipe.transform(new Date(), 'dd-MM-yyyy');
    this.catalogoService.getCatalogosTaquigraficas(fecha).subscribe((data) => {
      this.ministros = data.ministros
      data.ministros = this.conver.proccessArray(data.ministros);
      this.catalogos = data;
    });
  }

  createTaquigrafica() {
    if (!this.taquigrafica.duracionSesion || !this.taquigrafica.fechaSesion) {
      this.requerid.duracion = this.taquigrafica.duracionSesion ? false : true;
      this.requerid.fecha = this.taquigrafica.fechaSesion ? false : true;
      document.getElementById('version').scrollIntoView({ behavior: 'smooth' });
      this.toast.showToast(
        'error',
        'Validación',
        'Campo(s) son requerido(s) para su creación'
      );
    } else {
      if (this.validPonente) {
        this.taquigraficaService.postTaquigrafica(this.taquigrafica).subscribe(
          (data) => {
            if (data.versionTaquigrafica.id) {
              this.toast.showToast(
                'success',
                'Creación',
                'Se ha creado exitosamente la versión taquigráfica'
              );
              this.route
                .navigateByUrl('/', { skipLocationChange: true })
                .then((success) =>
                  this.route.navigate([
                    '/v-taquigraficas/edicion',
                    data.versionTaquigrafica.id,
                  ])
                );
            }
          },
          (error) => {}
        );
      }
    }
  }

  getMinistros(fecha: string) {
    this.catalogoService.getCatalogosTaquigraficas(fecha).subscribe((data) => {
      this.ministros = data.ministros
      data.ministros = this.conver.proccessArray(data.ministros);
      this.catalogos = data;
      this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion =
        data.ministros;
    });
  }

  removeAusente(name: string) {
    const indexOf =
      this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion.findIndex(
        (ministro) => ministro == name
      );
    if (indexOf != -1) {
      this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion.splice(
        indexOf,
        1
      );
      const acutal = this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion;
      this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion = [];
      setTimeout(() => {
        this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion = acutal;
      }, 200);
    }
  }

  removePresente(name: string) {
    const indexOf =
      this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion.findIndex(
        (ministro) => ministro == name
      );
    if (indexOf != -1) {
      this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion.splice(
        indexOf,
        1
      );
      const acutal =
        this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion;
      this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion = [];
      setTimeout(() => {
        this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion = acutal;
      }, 200);
    }
  }

  validatePonente(ponente: string) {
    if (this.taquigrafica.nombrePresidenteDecano) {
      const index = this.catalogos.ministros.findIndex(
        (name) => name == ponente
      );
      console.log(index);

      this.validPonente = index != -1 ? true : false;
    } else {
      this.validPonente = true;
    }
  }
}
