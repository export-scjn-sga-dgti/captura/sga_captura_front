import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfirmationService } from 'primeng/api';
import { ScrollTop } from 'src/app/helper/scrollTop';
import { Validate } from 'src/app/helper/validateExpediente';
import { UsuariosApiService } from 'src/app/services/usuarios-api.service';
import { Usuario } from 'src/app/models/usuarioModel';
import {
  CreateAsuntoAbordado,
  Taquigrafica,
} from 'src/app/models/taquigraficaModel';
import { CatalogosService } from 'src/app/services/catalogos.service';

import { TaquigraficaApiService } from 'src/app/services/taquigrafica-api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-taquigrafica-asuntos',
  templateUrl: './taquigrafica-asuntos.component.html',
  styleUrls: ['./taquigrafica-asuntos.component.scss'],
})
export class TaquigraficaAsuntosComponent implements OnInit {
  temaProcesal: any;
  temaFondo: any = {};
  userData: Usuario;
  camposRestricted: string[] = [];
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    idField: 'nombre',
    textField: 'nombre',
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  scroll: ScrollTop = new ScrollTop();
  catalogos: any = {};
  idTaquigrafica: string = '';
  sesion: string = '';
  asuntoAbordado: CreateAsuntoAbordado = new CreateAsuntoAbordado();
  acumulado: string = '';
  fondo: string = '';
  procesal: string = '';
  versionTaquigrafica: Taquigrafica = new Taquigrafica();
  isEditAsunto: boolean = false;
  positionAsunto: number = -1;
  indexOfProcesal: number = -1;
  indexOfFondo: number = -1;
  indexOfAcumulado: number = -1;
  username: string;
  usernameInitials: string;
  requerid = {
    asunto: false,
    expediente: false,
    expedienteAC: false,
    ponente: false,
  };
  idAsunto: string = '';
  isAsunto: boolean = false;
  validAutocomplet: boolean[] = [true, true];
  isView: boolean = false
  constructor(
    private catalogoService: CatalogosService,
    private param: ActivatedRoute,
    private vTaquigraficaService: TaquigraficaApiService,
    private confirmationService: ConfirmationService,
    private toast: ToastService,
    private usuarioService: UsuariosApiService,
    private router: Router
  ) {
    this.scroll.scrollTo();

    this.username = window.localStorage.getItem('username');
    this.usernameInitials = window.localStorage.getItem('usernameInitials');

    this.param.paramMap.subscribe((param) => {
      if (param.has('id')) {
        this.idTaquigrafica = param.get('id');
        this.vTaquigraficaService
          .getTaquigraficaByID(this.idTaquigrafica)
          .subscribe((res) => {
            this.sesion = ' Fecha sesión: ' + res.vtaq.fechaSesion;
            this.versionTaquigrafica = res;
            this.catalogoService
              .getCatalogosTaquigraficas(res.vtaq.fechaSesion)
              .subscribe((data) => {
                this.catalogos = data;
                if (!param.has('idAsunto')) {
                  this.catalogos.ministros.forEach((data: any) => {
                    this.asuntoAbordado.vt_asuntos.numeroMinistrosParticiparonEnAsunto.push(
                      {
                        nombre: data.nombre,
                        cantidad: 0,
                      }
                    );
                  });
                }
              });
          });

        if (param.has('idAsunto')) {
          this.idAsunto = param.get('idAsunto');
          this.isAsunto = true;
          this.getAsuntoByID(this.idAsunto);
        }
      }

      this.param.queryParamMap.subscribe(querys => {
        if (querys.has('visualizar')) {
          this.isView = true
        }
      })
    });
  }

  ngOnInit(): void {
    this.usuarioService.getUsuarioByUsername(this.username).subscribe((data) => {
      this.userData = data;
      for (let index in data.modulos) {
        if (data.modulos[index].modulo === 'taquigrafica') {
          if (data.modulos[index].camposRestricted.length > 0) {
            this.camposRestricted = data.modulos[index].camposRestricted;
          }
        }
      }
    });
  }

  newTema(tema: string) {
    if (tema.includes('procesal')) {
      this.temaProcesal = {
        open: true,
        option: 'new',
      };
    } else {
      this.temaFondo = {
        open: true,
        option: 'new',
      };
    }
  }

  addAcumulado() {
    if (this.acumulado) {
      if (Validate(this.acumulado)) {
        this.asuntoAbordado.vt_asuntos.acumulados.push(this.acumulado);
        this.acumulado = '';
        this.requerid.expedienteAC = false;
      } else {
        this.requerid.expedienteAC = true;
        this.toast.showToast(
          'error',
          'Validación',
          'Número de expediente no valido'
        );
      }
    }
  }

  addTemaProcesal() {
    if (!this.procesal) return;
    if (this.procesal.includes('\n')) {
      let lines: string[] = this.procesal.split('\n');
      lines.forEach((text) => {
        if (text === '') return;
        this.asuntoAbordado.vt_asuntos.temasProcesales.push(text);
      });
    } else {
      this.asuntoAbordado.vt_asuntos.temasProcesales.push(this.procesal);
    }
    if (this.isEditAsunto) {
      this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'procesal', this.asuntoAbordado.vt_asuntos.temasProcesales).subscribe(
        data => {
          if (data) {
            this.toast.showToast('success', 'Creación', 'Tema procesal ha sido creado con éxito');
          }
        }
      )
    }
    const data = this.asuntoAbordado.vt_asuntos.temasProcesales
    this.asuntoAbordado.vt_asuntos.temasProcesales = []
    setTimeout(() => {
      this.asuntoAbordado.vt_asuntos.temasProcesales = data
    }, 300)
    this.procesal = '';
  }

  addTemaFondo() {
    if (!this.fondo) return;
    if (this.fondo.includes('\n')) {
      let lines: string[] = this.fondo.split('\n');
      lines.forEach((text) => {
        if (text === '') return;
        this.asuntoAbordado.vt_asuntos.temasFondo.push(text);
      });
    } else {
      this.asuntoAbordado.vt_asuntos.temasFondo.push(this.fondo);
    }
    if (this.isEditAsunto) {
      this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'fondo', this.asuntoAbordado.vt_asuntos.temasFondo).subscribe(
        data => {
          if (data) {
            this.toast.showToast('success', 'Creación', 'Tema de fondo ha sido creado con éxito');
          }
        }
      )
    }
    const data = this.asuntoAbordado.vt_asuntos.temasFondo
    this.asuntoAbordado.vt_asuntos.temasFondo = []
    setTimeout(() => {
      this.asuntoAbordado.vt_asuntos.temasFondo = data
    }, 300)

    this.fondo = '';
  }

  trackBy(index: number, items: string): any {
    return items;
  }

  addAsunto() {
    if (
      !this.asuntoAbordado.asunto.numeroExpediente ||
      !this.asuntoAbordado.asunto.asuntoAbordado ||
      !this.asuntoAbordado.vt_asuntos.ponente
    ) {
      this.requerid.asunto = !this.asuntoAbordado.asunto.asuntoAbordado
        ? true
        : false;
      this.requerid.expediente = !this.asuntoAbordado.asunto.numeroExpediente
        ? true
        : false;
      this.requerid.ponente = !this.asuntoAbordado.vt_asuntos.ponente
        ? true
        : false;
      this.toast.showToast(
        'error',
        'Campos requeridos',
        'Numero de expediente, asunto abordado y ponente son requeridos'
      );
      document
        .getElementById('data-asunto')
        ?.scrollIntoView({ behavior: 'smooth' });
    } else if (Validate(this.asuntoAbordado.asunto.numeroExpediente)) {
      this.asuntoAbordado.idVersionTaqui = this.versionTaquigrafica.vtaq.id;
      if (this.validAutocomplet.filter((m) => m == false).length === 0) {
        if (this.asuntoAbordado.vt_asuntos.numeroMinistrosParticiparonEnAsunto.filter(m => m.cantidad > 0).length > 0) {
          this.vTaquigraficaService
            .postAsuntoAbordado(this.asuntoAbordado)
            .subscribe((resp) => {
              if (resp.statusCode == 200) {
                this.toast.showToast(
                  'success',
                  'Creado',
                  'Asunto abordado se ha creado con éxito'
                );
                this.router.navigate([
                  '/v-taquigraficas/edicion/',
                  this.idTaquigrafica,
                ]);
                return;
              }
              if (resp.statusCode == 400) {
                this.toast.showToast('error', 'Error', resp.message);
              }
            });
        } else {
          this.toast.showToast(
            'error',
            'Participación requerida',
            'Al menos una participación'
          );
          document
            .getElementById('list-ministros')
            ?.scrollIntoView({ behavior: 'smooth' });
        }
      }
    } else {
      this.requerid.asunto = false;
      this.requerid.expediente = true;
      document
        .getElementById('data-asunto')
        ?.scrollIntoView({ behavior: 'smooth' });
      this.toast.showToast(
        'error',
        'Validación',
        'Número de expediente no valido'
      );
    }
  }

  ministrosForSeason() {
    this.catalogos.ministros.forEach((data: any) => {
      this.asuntoAbordado.vt_asuntos.numeroMinistrosParticiparonEnAsunto.push({
        nombre: data.nombre,
        cantidad: 0,
      });
    });
  }

  async getAsuntoByID(id: string) {
    try {
      const data = await this.vTaquigraficaService
        .getAsuntoAbordadoByID(this.idTaquigrafica, id)
        .toPromise()
        .then((success) => {
          return success;
        });
      this.asuntoAbordado = data;
      this.isEditAsunto = true;
      document
        .getElementById('data-asunto')
        ?.scrollIntoView({ behavior: 'smooth' });
    } catch {
      this.toast.showToast(
        'error',
        'Error',
        'Ha ocurrido algo inesperado al obtener el asunto.'
      );
    }
  }

  cancelEditAunto() {
    this.asuntoAbordado = new CreateAsuntoAbordado();
    this.ministrosForSeason();
    this.isEditAsunto = false;
    this.positionAsunto = -1;
    document
      .getElementById('list_asuntos')
      ?.scrollIntoView({ behavior: 'smooth' });
  }

  updateAsunto() {
    this.vTaquigraficaService
      .updateAsuntoAbordado(this.asuntoAbordado)
      .subscribe(
        (resp) => {
          if (resp.statusCode == 200) {
            this.isEditAsunto = false;
            this.router.navigate([
              '/v-taquigraficas/edicion/',
              this.idTaquigrafica,
            ]);
            this.toast.showToast(
              'success',
              'Actualización',
              'Asunto abordado ha sido actualizado con éxito'
            );
            this.asuntoAbordado = new CreateAsuntoAbordado();
            return;
          }

          if (resp.statusCode == 400) {
            this.toast.showToast('error', 'Error', resp.message);
          }
        },
        (error) => {
          this.toast.showToast(
            'error',
            'Error',
            'Ha ocurrido algo inesperado al actualizar el asunto abordado'
          );
        }
      );
  }

  setTemaProcesal(texto: string, index: number) {
    this.procesal = texto;
    this.indexOfProcesal = index;
  }

  setTemaFondo(texto: string, index: number) {
    this.fondo = texto;
    this.indexOfFondo = index;
  }

  updateTemaProcesal() {
    this.asuntoAbordado.vt_asuntos.temasProcesales[this.indexOfProcesal] =
      this.procesal;
    this.indexOfProcesal = -1;
    this.procesal = '';
    if (this.isEditAsunto) {
      this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'procesal', this.asuntoAbordado.vt_asuntos.temasProcesales).subscribe(
        data => {
          if (data) {
            this.toast.showToast('success', 'Actualización', 'Tema procesal ha sido actualizado con éxito');
          }
        }
      )
    }

    const data = this.asuntoAbordado.vt_asuntos.temasProcesales
    this.asuntoAbordado.vt_asuntos.temasProcesales = []
    setTimeout(() => {
      this.asuntoAbordado.vt_asuntos.temasProcesales = data
    }, 300)
  }

  updateTemaFondo() {
    this.asuntoAbordado.vt_asuntos.temasFondo[this.indexOfFondo] = this.fondo;
    this.indexOfFondo = -1;
    this.fondo = '';
    if (this.isEditAsunto) {
      this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'fondo', this.asuntoAbordado.vt_asuntos.temasFondo).subscribe(data => {
        if (data) {
          this.toast.showToast('success', 'Actualización', 'Tema de fondo ha sido actualizado con éxito');
        }
      })
    }
    const data = this.asuntoAbordado.vt_asuntos.temasFondo
    this.asuntoAbordado.vt_asuntos.temasFondo = []
    setTimeout(() => {
      this.asuntoAbordado.vt_asuntos.temasFondo = data
    }, 300)
  }

  cancelTema(type: string) {
    if (type == 'fondo') {
      this.indexOfFondo = -1;
      this.fondo = '';
    } else {
      this.indexOfProcesal = -1;
      this.procesal = '';
    }
  }

  editAcumulado(expediente: string, index: number) {
    this.indexOfAcumulado = index;
    this.acumulado = expediente;
  }

  cancelAcumulado() {
    this.indexOfAcumulado = -1;
    this.acumulado = '';
    this.requerid.expedienteAC = false;
  }

  updateAcumulado() {
    if (this.acumulado) {
      if (Validate(this.acumulado)) {
        this.asuntoAbordado.vt_asuntos.acumulados[this.indexOfAcumulado] =
          this.acumulado;
        this.acumulado = '';
        this.indexOfAcumulado = -1;
        this.requerid.expedienteAC = false;
      } else {
        this.requerid.expedienteAC = true;
        this.toast.showToast(
          'error',
          'Validación',
          'Número de expediente no valido'
        );
      }
    }
  }

  validCatalogo(tipo: string) {
    switch (tipo) {
      case 'asunto':
        if (this.asuntoAbordado.asunto.asuntoAbordado) {
          const index = this.catalogos.asuntoAbordado.findIndex(
            (data) => this.asuntoAbordado.asunto.asuntoAbordado == data.nombre
          );
          this.validAutocomplet[0] = index != -1 ? true : false;
        } else {
          this.validAutocomplet[0] = true;
        }
        break;
      case 'ponente':
        if (this.asuntoAbordado.vt_asuntos.ponente) {
          const index = this.catalogos.ministros.findIndex(
            (data) => this.asuntoAbordado.vt_asuntos.ponente == data.nombre
          );
          this.validAutocomplet[1] = index != -1 ? true : false;
        } else {
          this.validAutocomplet[1] = true;
        }
        break;
    }
  }

  deleteTemaFondo(index: number, ev: Event, text: string) {
    this.confirmationService.confirm({
      target: ev.target,
      icon: 'pi pi-trash',
      acceptIcon: 'pi pi-trash',
      rejectIcon: 'pi pi-times',
      message: '¿Estás seguro de eliminar permanentemente el tema?',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        if (this.isEditAsunto) {
          this.vTaquigraficaService.deleteTema(this.idTaquigrafica, this.idAsunto, 'fondo', text).subscribe(data => {
            this.asuntoAbordado.vt_asuntos.temasFondo.splice(index, 1);
            const dataService = this.asuntoAbordado.vt_asuntos.temasFondo
            this.asuntoAbordado.vt_asuntos.temasFondo = []
            setTimeout(() => {
              this.asuntoAbordado.vt_asuntos.temasFondo = dataService
            }, 300)
            this.toast.showToast('success', 'Eliminación', 'Tema de fondo ha sido eliminado con éxito');
          })
        } else {
          this.asuntoAbordado.vt_asuntos.temasFondo.splice(index, 1);
          const data = this.asuntoAbordado.vt_asuntos.temasFondo
          this.asuntoAbordado.vt_asuntos.temasFondo = []
          setTimeout(() => {
            this.asuntoAbordado.vt_asuntos.temasFondo = data
          }, 300)
        }

      },
    });
  }

  deleteTemaProcesal(index: number, ev: Event, text: string) {
    this.confirmationService.confirm({
      target: ev.target,
      icon: 'pi pi-trash',
      acceptIcon: 'pi pi-trash',
      rejectIcon: 'pi pi-times',
      message: '¿Estás seguro de eliminar permanentemente el tema?',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        if (this.isEditAsunto) {
          this.vTaquigraficaService.deleteTema(this.idTaquigrafica, this.idAsunto, 'procesal', text).subscribe(data => {
            this.asuntoAbordado.vt_asuntos.temasProcesales.splice(index, 1);
            const dataService = this.asuntoAbordado.vt_asuntos.temasProcesales;
            this.asuntoAbordado.vt_asuntos.temasProcesales = [];
            setTimeout(() => {
              this.asuntoAbordado.vt_asuntos.temasProcesales = dataService;
            }, 300)
            this.toast.showToast('success', 'Eliminación', 'Tema procesal ha sido eliminado con éxito');
          })
        } else {
          this.asuntoAbordado.vt_asuntos.temasProcesales.splice(index, 1);
          const data = this.asuntoAbordado.vt_asuntos.temasProcesales;
          this.asuntoAbordado.vt_asuntos.temasProcesales = [];
          setTimeout(() => {
            this.asuntoAbordado.vt_asuntos.temasProcesales = data;
          }, 300)
        }

      },
    });
  }

  saveTemaProcesal() {
    if (this.isEditAsunto && !this.isView) {
      this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'procesal', this.asuntoAbordado.vt_asuntos.temasProcesales).subscribe(data => {
        if (data) {
          this.toast.showToast('success', 'Actualización', 'Tema procesal ha sido actualizado con éxito');
        }
      }
      )
    }
  }

  saveTemaFondo() {
    if (this.isEditAsunto && !this.isView) {
      this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'fondo', this.asuntoAbordado.vt_asuntos.temasFondo).subscribe(data => {
        if (data) {
          this.toast.showToast('success', 'Actualización', 'Tema de fondo ha sido actualizado con éxito');
        }
      }
      )
    }
  }


  movedToProcesal(ev: Event, index: number, text: string) {

    this.confirmationService.confirm({
      target: ev.target,
      icon: 'pi pi-angle-double-up',
      message: '¿Estás seguro de mover el tema de fondo a tema procesal?',
      acceptIcon: 'pi pi-save',
      rejectIcon: 'pi pi-times',
      accept: () => {
        this.asuntoAbordado.vt_asuntos.temasProcesales.push(text);
        const currentProcesal = this.asuntoAbordado.vt_asuntos.temasProcesales;
        this.asuntoAbordado.vt_asuntos.temasProcesales = [];

        this.asuntoAbordado.vt_asuntos.temasFondo.splice(index, 1);
        const currentFondo = this.asuntoAbordado.vt_asuntos.temasFondo;
        this.asuntoAbordado.vt_asuntos.temasFondo = [];

        if (this.isEditAsunto && !this.isView) {
          this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'procesal', currentProcesal).subscribe(data => {
            if (data) {
              this.toast.showToast('success', 'Actualización', 'Tema procesal ha sido actualizado con éxito');
              this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'fondo', currentFondo).subscribe(data => {
                if (data) {
                  this.toast.showToast('success', 'Actualización', 'Tema de fondo ha sido actualizado con éxito');
                }
              }
              )
            }
          })
        }

        setTimeout(() => {
          this.asuntoAbordado.vt_asuntos.temasProcesales = currentProcesal;
          this.asuntoAbordado.vt_asuntos.temasFondo = currentFondo;
        }, 300)
      }
    })


  }

  movedToFondo(ev: Event, index: number, text: string) {

    this.confirmationService.confirm({
      target: ev.target,
      icon: 'pi pi-angle-double-down',
      message: '¿Estás seguro de mover el tema procesal a tema de fondo?',
      acceptIcon: 'pi pi-save',
      rejectIcon: 'pi pi-times',
      accept: () => {
        this.asuntoAbordado.vt_asuntos.temasFondo.push(text);
        const currentFondo = this.asuntoAbordado.vt_asuntos.temasFondo;
        this.asuntoAbordado.vt_asuntos.temasFondo = [];

        this.asuntoAbordado.vt_asuntos.temasProcesales.splice(index, 1);
        const currentProcesal = this.asuntoAbordado.vt_asuntos.temasProcesales;
        this.asuntoAbordado.vt_asuntos.temasProcesales = [];

        if (this.isEditAsunto && !this.isView) {
          this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'fondo', currentFondo).subscribe(data => {
            if (data) {
              this.toast.showToast('success', 'Actualización', 'Tema de fondo ha sido actualizado con éxito');

              this.vTaquigraficaService.updateTema(this.idTaquigrafica, this.idAsunto, 'procesal', currentProcesal).subscribe(data => {
                if (data) {
                  this.toast.showToast('success', 'Actualización', 'Tema procesal ha sido actualizado con éxito');

                }
              })
            }
          }
          )
        }

        setTimeout(() => {
          this.asuntoAbordado.vt_asuntos.temasProcesales = currentProcesal;
          this.asuntoAbordado.vt_asuntos.temasFondo = currentFondo;
        }, 300)
      }
    })
  }


}
