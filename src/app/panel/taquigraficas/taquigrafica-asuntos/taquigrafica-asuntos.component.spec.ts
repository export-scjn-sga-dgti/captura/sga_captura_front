import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TaquigraficaAsuntosComponent } from './taquigrafica-asuntos.component';

describe('TaquigraficaAsuntosComponent', () => {
  let component: TaquigraficaAsuntosComponent;
  let fixture: ComponentFixture<TaquigraficaAsuntosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaquigraficaAsuntosComponent ],
      imports: [HttpClientTestingModule,CommonModule,RouterTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaquigraficaAsuntosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
