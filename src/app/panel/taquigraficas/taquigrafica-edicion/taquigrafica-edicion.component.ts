import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfirmationService } from 'primeng/api';
import { ConverStringArray } from 'src/app/helper/converArrayObject';
import { ScrollTop } from 'src/app/helper/scrollTop';
import { Asunto, CreateTaquigrafica } from 'src/app/models/taquigraficaModel';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { UsuariosApiService } from 'src/app/services/usuarios-api.service';
import { Usuario} from 'src/app/models/usuarioModel';
import { TaquigraficaApiService } from 'src/app/services/taquigrafica-api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-taquigrafica-edicion',
  templateUrl: './taquigrafica-edicion.component.html',
  styleUrls: ['./taquigrafica-edicion.component.scss'],
  providers: [ConverStringArray],
})
export class TaquigraficaEdicionComponent implements OnInit {
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  scroll: ScrollTop = new ScrollTop();
  catalogos: any = {};
  idTaquigrafoca: string = '';
  taquigrafica: CreateTaquigrafica = new CreateTaquigrafica();
  sesion: string = '';
  asuntos: Asunto[] = [];
  validPonente: boolean = true;
  userData: Usuario;
  username: string;
  usernameInitials: string;
  camposRestricted: string[] = [];
  isView: boolean = false;

  ministros: any [] = []
  constructor(
    private catalogoService: CatalogosService,
    private taquigraficaService: TaquigraficaApiService,
    private conver: ConverStringArray,
    private param: ActivatedRoute,
    private router: Router,
    private toast: ToastService,
    private usuarioService: UsuariosApiService,
    private confirmationService: ConfirmationService
  ) {
    this.scroll.scrollTo();

    this.username = localStorage.getItem('username');
   this.usernameInitials = localStorage.getItem('usernameInitials');

    this.param.paramMap.subscribe((param) => {
      this.idTaquigrafoca = param.get('id');

      this.taquigraficaService
        .getTaquigraficaByID(this.idTaquigrafoca)
        .subscribe((data) => {
          this.sesion = ' Fecha sesión: ' + data.vtaq.fechaSesion;
          this.taquigrafica = data.vtaq;
          this.asuntos = data.asuntos;
          this.catalogoService
            .getCatalogosTaquigraficas(data.vtaq.fechaSesion)
            .subscribe((data) => {
              this.ministros = data.ministros
              data.ministros = this.conver.proccessArray(data.ministros);
              this.catalogos = data;
            });
        });

        this.param.queryParamMap.subscribe(querys => {
          if (querys.has('visualizar')) {
            this.isView = true
          }
        })
    });
  }
  ngOnInit(): void {

    this.usuarioService.getUsuarioByUsername(this.username).subscribe((data) => {
      this.userData = data;
      for(let index in data.modulos){
        if(data.modulos[index].modulo === 'taquigrafica'){
          if(data.modulos[index].camposRestricted.length>0){
            this.camposRestricted = data.modulos[index].camposRestricted;
          }
        }
      }
    });

  }

  updateTaquigrafica() {
    if (this.validPonente) {
      this.taquigraficaService.putTaquigrafica(this.taquigrafica).subscribe(
        (resp) => {
          if (resp.statusCode == 200) {
            this.toast.showToast(
              'success',
              'Actualización',
              'Se ha actualizado la versión taquigráfica'
            );
            this.router.navigate(['/v-taquigraficas']);
          }
        },
        (erro) => {}
      );
    }
  }

  deleteAsunto(event: Event, idAsunto: string, index: number) {
    this.confirmationService.confirm({
      icon: 'pi pi-trash',
      acceptIcon: 'pi pi-trash',
      rejectIcon: 'pi pi-times',
      acceptButtonStyleClass: 'btn-danger',
      message: '¿Estás seguro de eliminar permanentemente el asunto?',
      target: event.target,
      accept: () => {
        this.taquigraficaService
          .deleteAsuntoAbordado(this.idTaquigrafoca, idAsunto)
          .subscribe((success) => {
            if (success) {
              this.asuntos.splice(index, 1);
              this.toast.showToast(
                'success',
                'Eliminado',
                'Asunto abordado ha sido eliminado con éxito'
              );
            }
          });
      },
      reject: () => {},
    });
  }

  removeAusente(name: string) {
    const indexOf =
      this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion.findIndex(
        (ministro) => ministro == name
      );
    if (indexOf != -1) {
      this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion.splice(
        indexOf,
        1
      );
      const acutal = this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion;
      this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion = [];
      setTimeout(() => {
        this.taquigrafica.nombreMinistrasMinistrosAusentesEnSesion = acutal;
      }, 200);
    }
  }

  removePresente(name: string) {
    const indexOf =
      this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion.findIndex(
        (ministro) => ministro == name
      );
    if (indexOf != -1) {
      this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion.splice(
        indexOf,
        1
      );
      const acutal =
        this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion;
      this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion = [];
      setTimeout(() => {
        this.taquigrafica.nombreMinistrasMinistrosPresentesEnSesion = acutal;
      }, 200);
    }
  }

  validatePonente(ponente: string) {
    if (this.taquigrafica.nombrePresidenteDecano) {
      const index = this.catalogos.ministros.findIndex(
        (name) => name == ponente
      );
      this.validPonente = index != -1 ? true : false;
    } else {
      this.validPonente = true;
    }
  }
}
