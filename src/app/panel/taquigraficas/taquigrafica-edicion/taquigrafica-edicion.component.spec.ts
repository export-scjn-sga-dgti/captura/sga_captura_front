import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaquigraficaEdicionComponent } from './taquigrafica-edicion.component';

describe('TaquigraficaEdicionComponent', () => {
  let component: TaquigraficaEdicionComponent;
  let fixture: ComponentFixture<TaquigraficaEdicionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaquigraficaEdicionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaquigraficaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
