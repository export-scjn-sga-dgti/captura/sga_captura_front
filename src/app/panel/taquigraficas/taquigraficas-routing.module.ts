import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaquigraficaAsuntosComponent } from './taquigrafica-asuntos/taquigrafica-asuntos.component';
import { TaquigraficaEdicionComponent } from './taquigrafica-edicion/taquigrafica-edicion.component';
import { TaquigraficaRegistroComponent } from './taquigrafica-registro/taquigrafica-registro.component';
import { TaquigraficasComponent } from './taquigraficas.component';

const routes: Routes = [
  { path: '', component: TaquigraficasComponent },
  { path: 'registro', component: TaquigraficaRegistroComponent },
  { path: 'edicion/:id', component: TaquigraficaEdicionComponent },
  { path: 'edicion/:id/asunto', component: TaquigraficaAsuntosComponent },
  {
    path: 'edicion/:id/asunto/:idAsunto',
    component: TaquigraficaAsuntosComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TaquigraficasRoutingModule {}
