import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { Validate } from 'src/app/helper/validateExpediente';
import { SaveFilters } from 'src/app/models/save-filters';
import { AllTaquigraficas, Content } from 'src/app/models/taquigraficaModel';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { SaveFiltersService } from 'src/app/services/save-filters.service';

import { TaquigraficaApiService } from 'src/app/services/taquigrafica-api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-taquigraficas',
  templateUrl: './taquigraficas.component.html',
  styleUrls: ['./taquigraficas.component.scss'],
})
export class TaquigraficasComponent implements OnInit {
  page: number = 1;
  catalogos: any = {};
  taquigraficas: AllTaquigraficas = new AllTaquigraficas();
  filterVTaq: Filter = new Filter();
  validAsunto: boolean = false;
  validExpediente: boolean = false;
  isFilter: boolean;
  size: number = 10;
  filters: string = '';
  constructor(
    private catalogosService: CatalogosService,
    private vTaquigraficasService: TaquigraficaApiService,
    private confimationService: ConfirmationService,
    private toast: ToastService,
    private datePipe: DatePipe,
    private saveFilters: SaveFiltersService
  ) {    
    if(this.saveFilters.filters.module == 'taquigrafica'){
      const data = this.saveFilters.filters.filter.split(',')
      data.forEach(keys =>{
        const k = keys.split(':')
        this.filterVTaq[k[0]] = k[1]
      })
      this.isFilter = true
      this.filters = this.saveFilters.filters.filter
      this.paramsTaquigraficas(10,1,this.filters);
    } else {
      this.saveFilters.filters = {} as SaveFilters
      this.paramsTaquigraficas();
    }
   
  }

  ngOnInit(): void {
    const date = this.datePipe.transform(new Date(), 'dd-MM-yyyy');
    this.catalogosService.getCatalogosTaquigraficas(date).subscribe((data) => {
      this.catalogos = data;
    });
  }

  private paramsTaquigraficas(
    size: number = 10,
    page: number = 1,
    filtros?: string
  ) {
    this.vTaquigraficasService
      .getAllTaquigraficas(page, size, filtros)
      .subscribe(
        (data) => {
          this.taquigraficas = data;
        },
        (error) => {}
      );
  }

  public deleteVersionTaquigrafica(event: Event, version: Content) {
    this.confimationService.confirm({
      target: event.target,
      icon: 'pi pi-trash',
      acceptIcon: 'pi pi-trash',
      rejectIcon: 'pi pi-times',
      message:
        '¿Estás seguro de eliminar permanentemente la versión taquigráfica?',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.vTaquigraficasService
          .deleteTaquigrafica(version.id)
          .subscribe((resp) => {
            if (resp) {
              const index = this.taquigraficas.content.indexOf(version);
              this.taquigraficas.content.splice(index, 1);
              this.toast.showToast(
                'success',
                'Versión taquigráfica eliminado con éxito',
                'Eliminado'
              );
            }
          });
      },
    });
  }

  public searchFilter() {
    if (!this.validAsunto && !this.validExpediente) {
      let filters: string = '';
      filters += this.filterVTaq.asuntoAbordado
        ? `asuntoAbordado:${this.filterVTaq.asuntoAbordado},`
        : '';
      filters += this.filterVTaq.numeroExpediente
        ? `numeroExpediente:${this.filterVTaq.numeroExpediente},`
        : '';
      filters += this.filterVTaq.fechaSesion
        ? `fechaSesion:${this.filterVTaq.fechaSesion},`
        : '';
      this.filters = filters;
      this.saveFilters.filters.module = "taquigrafica"
      this.saveFilters.filters.filter = this.filters
      this.vTaquigraficasService
        .getAllTaquigraficas(1, this.taquigraficas.size, filters)
        .subscribe(
          (data) => {
            this.isFilter = true;
            this.taquigraficas = data;
          },
          (error) => {}
        );
    }
  }

  public onPageChange(page: number) {
    this.vTaquigraficasService
      .getAllTaquigraficas(page, this.taquigraficas.size, this.filters)
      .subscribe(
        (data) => {
          this.taquigraficas = data;
        },
        (error) => {}
      );
  }

  public validCamp(field: string): void {
    switch (field) {
      case 'asunto':
        if (this.filterVTaq.asuntoAbordado) {
          const index = this.catalogos.asuntoAbordado.findIndex(
            (data: any) => data.nombre === this.filterVTaq.asuntoAbordado
          );
          this.validAsunto = index != -1 ? false : true;
        } else {
          this.validAsunto = false;
        }
        break;
      case 'num':
        if (this.filterVTaq.numeroExpediente) {
          this.validExpediente = !Validate(this.filterVTaq.numeroExpediente);
        } else {
          this.validExpediente = false;
        }
        break;
    }
  }

  public calculateSizePage(){
    if(this.taquigraficas.size*(1+this.taquigraficas.number)>=this.taquigraficas.totalElements){
      return this.taquigraficas.totalElements;
    }else{
      return (this.taquigraficas.size*(1+this.taquigraficas.number));
    }
  }

  public clearFilter() {
    this.filterVTaq = new Filter();
    this.isFilter = false;
    this.filters = "";
    this.saveFilters.filters = {} as SaveFilters
    this.paramsTaquigraficas();
  }

  public onChangeSize({ value }) {
    this.size = value;
    this.vTaquigraficasService
      .getAllTaquigraficas(
        this.taquigraficas.number + 1,
        this.size,
        this.filters
      )
      .subscribe(
        (data) => {
          this.taquigraficas = data;
        },
        (error) => {}
      );
  }
}

export class Filter {
  fechaSesion: string;
  asuntoAbordado: string;
  numeroExpediente: string;
}
