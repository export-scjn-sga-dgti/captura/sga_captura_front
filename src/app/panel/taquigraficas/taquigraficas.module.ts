import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { TaquigraficasRoutingModule } from './taquigraficas-routing.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { FieldsetModule } from 'primeng/fieldset';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TaquigraficaAsuntosComponent } from './taquigrafica-asuntos/taquigrafica-asuntos.component';
import { TaquigraficaRegistroComponent } from './taquigrafica-registro/taquigrafica-registro.component';
import { TemaProcesalComponent } from 'src/app/components/tema-procesal/tema-procesal.component';
import { TemaFondoComponent } from 'src/app/components/tema-fondo/tema-fondo.component';
import { DialogModule } from 'primeng/dialog';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { TaquigraficaEdicionComponent } from './taquigrafica-edicion/taquigrafica-edicion.component';
import { InputMaskModule } from 'primeng/inputmask';
import { FormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmationService, MessageService } from 'primeng/api';
import { OrderListModule } from 'primeng/orderlist';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { TooltipModule } from 'primeng/tooltip';


@NgModule({
  declarations: [
    TaquigraficaRegistroComponent,
    TaquigraficaAsuntosComponent,
    TemaProcesalComponent,
    TemaFondoComponent,
    TaquigraficaEdicionComponent
  ],
  imports: [
    CommonModule,
    TaquigraficasRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    CheckboxModule,
    DropdownModule,
    ButtonModule,
    CalendarModule,
    FieldsetModule,
    InputSwitchModule,
    DialogModule,
    BreadcrumbModule,
    InputMaskModule,
    FormsModule,
    PipesModule,
    ToastModule,
    ConfirmPopupModule,
    OrderListModule,
    DropdownModule,
    TooltipModule  
  ],
  providers: [
    ConfirmationService,
    MessageService,
    DatePipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TaquigraficasModule { }
