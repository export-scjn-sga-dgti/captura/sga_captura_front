import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ToastModule } from 'primeng/toast';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputSwitchModule } from 'primeng/inputswitch';
import { UsuarioEdicionComponent } from './usuario-edicion/usuario-edicion.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  declarations: [
    UsuarioEdicionComponent],
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    CheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    InputSwitchModule,
    BreadcrumbModule,
    ToastModule,
    ConfirmPopupModule,
    NgMultiSelectDropDownModule,
    DialogModule
  ],
})
export class UsuariosModule { }
