import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from 'src/app/models/usuarioModel';
import { UsuariosApiService } from 'src/app/services/usuarios-api.service';
import { ConverStringArray } from 'src/app/helper/converArrayObject';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ToastService } from 'src/app/services/toast.service';
import { Router } from '@angular/router';
import { ViewsModules } from 'src/app/models/modulos';
import { Modules } from 'src/app/enum/modules.enum';
import { Permisos } from 'src/app/models/asignacion';


@Component({
  selector: 'app-usuario-edicion',
  templateUrl: './usuario-edicion.component.html',
  styleUrls: ['./usuario-edicion.component.scss'],
  providers: [ConverStringArray],
})
export class UsuarioEdicionComponent implements OnInit {
  modulos: string[] = []
  userModel: Usuario = new Usuario();
  views: { info: string, text: string }[] = [
    { info: Modules.sentencia, text: 'Sentencias' },
    { info: Modules.votos, text: 'Votos' },
    { info: Modules.acuerdos, text: 'Acuerdos Generales' },
    { info: Modules.normativa, text: 'Diversa Normativa' },
    { info: Modules.taquigraficas, text: 'Versiones Taquigráficas' },
    { info: Modules.seguimiento, text: 'Seguimiento' },
    { info: Modules.catalogo, text: 'Catálogos' },
    { info: Modules.admin, text: 'Administrador' },
  ]
  userViews: any[] = []
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    idField: "info",
    textField: "text",
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  requerid = {
    username: false,
    correo: false,
    tipoAsunto: false,
  };
  permisos: {} = {
    superadmin: false,
    asignacion: false,
    restricted: false
  }
  viewModulos: ViewsModules = new ViewsModules()
  opcionesExtra: string[] = []
  asuntosTaquigraficas: any = []
  asuntosSentencias: any = []
  modules = Modules

  constructor(
    private userService: UsuariosApiService,
    private converString: ConverStringArray,
    private params: ActivatedRoute,
    private toast: ToastService,
    private router: Router,
  ) {
    this.params.paramMap.subscribe((responseData) => {

      this.userService.getUsuarioByUsername(responseData.get('correo')).subscribe((data) => {
        this.userModel = data;
        
        this.modulos = this.converString.proccessArrayModulo(
          data.modulos
        )
        let temp: any[] = []

        this.modulos.forEach(
          (modulo) => {
            switch (modulo) {
              case Modules.sentencia:
                temp.push(this.views[0])
                break;
              case Modules.votos:
                temp.push(this.views[1])
                break;
              case Modules.acuerdos:
                temp.push(this.views[2])
                break;
              case Modules.normativa:
                temp.push(this.views[3])
                break;
              case Modules.taquigraficas:
                temp.push(this.views[4])
                break;
              case Modules.seguimiento:
                temp.push(this.views[5])
                break;
              case Modules.catalogo:
                temp.push(this.views[6])
                break;
              case Modules.admin:
                temp.push(this.views[7])
                break;
              default:
                this.opcionesExtra.push(modulo)
                break;
            }
          }
        )

        this.userViews = temp

        this.userModel.modulos.forEach(
          x => {
            switch (x.modulo) {
              case Modules.normativa:
                break;
              case Modules.taquigraficas:
                this.asuntosTaquigraficas = x.asuntos
                break;
              case Modules.acuerdos:
                break;
              case Modules.votos:
                break;
              case Modules.admin:
                break;
              case Modules.sentencia:
                this.asuntosSentencias = x.asuntos
                this.permisos['restricted'] = !x.camposRestricted.includes("numeroVotacionesRealizadas")
                this.permisos['superadmin'] = x.permisos.superadmin
                this.permisos['asignacion'] = x.permisos.asignacionAsuntos
                break;
              default:
                break;
            }
            this.viewModulos[x.modulo] = this.setPermisosModulo(x.camposRestricted, x.catalogosRestricted, x.permisos)

          }
        )
      });

    });

  }

  setPermisosModulo(camposR: any, catalogosR: any, permisos: any): any {

    let reglas = {}

    camposR.forEach(
      (campo) => {
        if (campo != 'numeroVotacionesRealizadas')
          reglas[campo] = true
      }
    )

    reglas['readOnly'] = permisos.ver & permisos.acceso & permisos.crear & permisos.editar & permisos.eliminar ? false : true
    return reglas
  }

  ngOnInit(): void {
  }

  actualizaUsuario(): void {  
    this.userModel.modulos.forEach(
      (modulo) => {  
        if (!this.opcionesExtra.includes(modulo.modulo)) {
          if (!this.modulos.includes(modulo.modulo)) {
            this.userModel.modulos.splice(this.userModel.modulos.indexOf(modulo), 1)
          }

        } else {
          let restricted = []

          if (modulo.modulo == Modules.sentencia && !this.permisos['restricted']) {
            restricted.push('numeroVotacionesRealizadas')
          }

          for (let view in this.viewModulos[modulo.modulo]) {

            if (this.viewModulos[modulo.modulo][view] == false && view != 'readOnly') {
              restricted.push(view)
            }

            if (view == 'readOnly' && this.viewModulos[modulo.modulo]['readOnly'] == true) {
              modulo.permisos.acceso = true
              modulo.permisos.crear = false
              modulo.permisos.editar = false
              modulo.permisos.eliminar = false
              modulo.permisos.ver = true
              modulo.permisos.superadmin = false
              modulo.permisos.asignacionAsuntos = false
            }

          }
          modulo.camposRestricted = restricted
          modulo.permisos.superadmin = this.permisos['superadmin']
          modulo.permisos.asignacionAsuntos = this.permisos['asignacion']
        }
      }
    )
    const date = new Date()
    this.userModel.fh_modificacion = date.toISOString()
  }

  guardarCambios(): void {


    if (this.userModel.nombre != "" && this.userModel.correo != "") {
      this.actualizaUsuario()      
      this.userService.updateUsuario(this.userModel).subscribe(
        data => {
          if (data.id) {
            this.toast.showToast(
              'success',
              'Usuario',
              'Se ha actualizado con éxito'
            );
            //this.router.navigateByUrl('/', { skipLocationChange: true }).then(nav => this.router.navigate(['/usuarios/']))    

          }
        },
        (error) => {
          this.toast.showToast(
            'error',
            'Error',
            'Ha ocurrido algo inesperado al actualizar el usuario'
          );
        }
      )

    } else {
      this.toast.showToast(
        'error',
        'Validación',
        'Los campos de nombre y correo son requeridos'
      );
    }
  }

  addModuleAccess(data: { info: string, text: string }) {
    this.userViews.push(data);
    this.modulos.push(data.info);
    const isCurrent = this.userModel.modulos.filter(x => x.modulo === data.info)
    if(isCurrent.length == 0) this.userModel.modulos.push({ modulo: data.info,asuntos: [],camposRestricted: [],catalogosRestricted:[],permisos: {} as Permisos })

  }

}
