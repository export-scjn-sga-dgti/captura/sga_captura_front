import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Modules } from 'src/app/enum/modules.enum';
import { Usuario, Modulos, Permisos } from 'src/app/models/usuarioModel';
import { ToastService } from 'src/app/services/toast.service';
import { UsuariosApiService } from 'src/app/services/usuarios-api.service';

@Component({
  selector: 'app-usuario-registro',
  templateUrl: './usuario-registro.component.html',
  styleUrls: ['./usuario-registro.component.scss']
})
export class UsuarioRegistroComponent implements OnInit {

  usuarioModel: Usuario = new Usuario()
  modulos: string[] = [];
  permisos: string[] = [];
  config: boolean;
  isAsuntos: boolean;
  editVoto: boolean;
  super: boolean;
  nameModules: string[] = [
    'Sentencias', 'Votos', 'Acuerdos Generales', 'Diversa Normativa', 'Versiones Taquigráficas', 'Seguimiento', 'Catálogos', 'Administrador']

  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>()
  @Input() set options(data: boolean) {
    this.config = data
    if (data) {
      this.usuarioModel = new Usuario();
      this.modulos = [];
      this.isAsuntos = false
      this.editVoto = false
      this.super = false
    }
  }

  get options(): boolean {
    return this.config
  }

  constructor(
    private userService: UsuariosApiService,
    private toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.setPermisosDefault()
  }

  setPermisosDefault(): Permisos {
    let permisosDefault = new Permisos()

    permisosDefault.acceso = true
    permisosDefault.ver = true
    permisosDefault.crear = true
    permisosDefault.editar = true
    permisosDefault.eliminar = true
    permisosDefault.superadmin = false
    permisosDefault.asignacionAsuntos = false

    return permisosDefault
  }

  crearUsuario(): void {
    this.modulos.forEach(modulo => {
      let module = new Modulos()
      module.modulo = modulo
      module.permisos = this.setPermisosDefault()
      if (modulo == Modules.sentencia) {
        module.camposRestricted = this.permisos.includes('restricted') ? [] : ['numeroVotacionesRealizadas']
        module.permisos.superadmin = this.permisos.includes('superadmin') ? true : false
        module.permisos.asignacionAsuntos = this.permisos.includes('asignacion') ? true : false
      }
      this.usuarioModel.modulos.push(module)
    })

    if (this.usuarioModel.correo && this.usuarioModel.nombre) {
      this.userService.saveUsuario(this.usuarioModel).subscribe(res => {
        this.close.emit(false)
        this.toastService.showToast('success', 'Creación de usuario', 'Se ha creado con éxito');
      });
    }

  }

  addModulo(checked: boolean, modulo: string): void {
    if (checked) {
      switch (modulo) {
        case 'Sentencias':
          this.modulos.push(Modules.sentencia);
          break;
        case 'Versiones Taquigráficas':
          this.modulos.push(Modules.taquigraficas);
          break;
        case 'Votos':
          this.modulos.push(Modules.votos);
          break;
        case 'Acuerdos Generales':
          this.modulos.push(Modules.acuerdos);
          break;
        case 'Administrador':
          this.modulos.push(Modules.admin);
          break;
        case 'Diversa Normativa':
          this.modulos.push(Modules.normativa);
          break;
        case 'Seguimiento':
          this.modulos.push(Modules.seguimiento);
          break;
        case 'Catálogos':
          this.modulos.push(Modules.catalogo);
          break;
      }
    } else {
      this.modulos.splice(this.modulos.indexOf(modulo), 1)
    }
  }

  addOpciones(checked: boolean, permisos: string): void {
    if (checked) {
      this.permisos.push(permisos)
    } else {
      this.permisos.splice(this.modulos.indexOf(permisos), 1)
    }
  }

}
