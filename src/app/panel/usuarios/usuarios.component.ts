import { Component, OnDestroy, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { Usuario } from 'src/app/models/usuarioModel';
import { ToastService } from 'src/app/services/toast.service';
import { UsuariosApiService } from 'src/app/services/usuarios-api.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss'],
})
export class UsuariosComponent implements OnInit, OnDestroy {
  isVisible: boolean = false
  users: any
  constructor(
    private userService: UsuariosApiService,
    private confirmService: ConfirmationService,
    private toastService: ToastService
    ) {}

  ngOnInit(): void {
  this.loadAllUser()
  }

  loadAllUser(){
    this.userService.getAllUsuarios().subscribe(
      (data)=>{
        this.users = data["content"]
      }
    );
  }

  ngOnDestroy(): void {
  }

  openDialog() {
    this.isVisible = true
  }

  dissmidPanel(ev:boolean){
    this.isVisible = ev
    this.loadAllUser()
  }

  deleteUser(ev:Event,user:Usuario){
    this.confirmService.confirm({
      target: ev.target,
        message: `¿Estás seguro de eliminar al usuario ${user.nombre}?`,
        icon: 'pi pi-trash',
        acceptButtonStyleClass: 'btn-danger',
      accept: () =>{
        this.userService.deleteUser(user.id).subscribe(data =>{
          if(data){
            this.toastService.showToast('success','Eliminación de usuario','Ha sido eliminado con exito');
            this.loadAllUser()
          }
        })
      }
    })
  }

}
