import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClasificacionMateriaComponent } from 'src/app/components/clasificacion-materia/clasificacion-materia.component';
import { DiversaNormativaEdicionComponent } from './diversa-normativa-edicion/diversa-normativa-edicion.component';
import { DiversaNormativaRegistroComponent } from './diversa-normativa-registro/diversa-normativa-registro.component';
import { DiversaNormativaComponent } from './diversa-normativa.component';

const routes: Routes = [
  {path:"",component:DiversaNormativaComponent},
  {path:"registro",component:DiversaNormativaRegistroComponent},
  {path:"edicion/:idNormativa",component:DiversaNormativaEdicionComponent},
  {path: ':id/clasificacion-materia', component: ClasificacionMateriaComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiversaNormativaRoutingModule { }
