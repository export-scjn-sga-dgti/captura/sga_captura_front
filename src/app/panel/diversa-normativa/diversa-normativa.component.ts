import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { NormativaApiService } from 'src/app/services/normativa-api.service';
import { Normativa, SearchNormativa, AllNormativaModel} from 'src/app/models/normativaModel';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { SaveFiltersService } from 'src/app/services/save-filters.service';
import { SaveFilters } from 'src/app/models/save-filters';

@Component({
  selector: 'app-diversa-normativa',
  templateUrl: './diversa-normativa.component.html',
  styleUrls: ['./diversa-normativa.component.scss'],
  providers: [ ConfirmationService ]
})
export class DiversaNormativaComponent implements OnInit {
  normativaAllMod: AllNormativaModel = new AllNormativaModel();
  page: number = 1
  rows: number = 10
  normativas: Normativa[] = []
  filtrado: string = ""
  totalRegistros: number = 0
  totalPages: number = 1
  viewSearch: boolean = false
  catalogo: any = []
  busqueda: SearchNormativa = new SearchNormativa()
  content:any;
  requerid = {
    tipoInstrumento:false,
    numero: false
  }

  constructor(private messageService: MessageService,
    private catalogoService: CatalogosService,
    private normativaService:NormativaApiService,
    private confirmationService:ConfirmationService,
    private saveFilters: SaveFiltersService) { 
      this.normativaAllMod.number = 0
    }

  ngOnInit(): void {
    this.catalogoService.getCatalogosNormativas().subscribe(
      (data) =>{
        this.catalogo = data['tipoInstrumento']
      }
    )
    if(this.saveFilters.filters.module == 'diversa'){
      const data = this.saveFilters.filters.filter.split(',')
      data.forEach(keys =>{
        const k = keys.split(':')
        this.busqueda[k[0]] = k[1]
      })
      this.viewSearch = true
      this.construyeFiltro()
    } else {
      this.saveFilters.filters = {} as SaveFilters
      this.realizaBusqueda()
    }

   
  }

  construyeFiltro():void{
    this.filtrado=""
    if(!this.busqueda.tipoInstrumento && !this.busqueda.numero){
      this.requerid.numero = true
      this.requerid.tipoInstrumento = true
      this.messageService.add(
        { key: 'ks',
          severity: 'error',
          summary: 'Búsqueda',
          detail: 'Se requiere al menos un parámetro para realizar la búsqueda'
        }
        )
    }else{
      this.requerid.numero = false
      this.requerid.tipoInstrumento = false
      this.normativas = []
      if(this.busqueda.tipoInstrumento){
        this.filtrado = "tipoInstrumento: "+this.busqueda.tipoInstrumento
      }
      if(this.busqueda.numero){
        if(this.filtrado){
          this.filtrado = this.filtrado+",numero: "+this.busqueda.numero
        }else{
          this.filtrado = "numero: "+this.busqueda.numero
        }
      }
    }

    if(this.filtrado){
      this.saveFilters.filters.module = 'diversa'
      this.saveFilters.filters.filter = this.filtrado
      this.realizaBusqueda(1,this.rows,this.filtrado)
      this.viewSearch = true
    }
  }

  confirm(event: Event, normativa: Normativa) {
    this.confirmationService.confirm({
        target: event.target,
        message: '¿Estás seguro de eliminar el documento?',
        icon: 'pi pi-trash',
        acceptButtonStyleClass: 'btn-danger',
        accept: () => {
          let indice:number = this.normativas.indexOf(normativa)
          this.normativaService.deleteNormativaByID(normativa.id).subscribe(
            res =>{
              if(indice!=-1)this.normativas.splice(indice,1)
              this.messageService.add(
                { key: 'ks',
                  severity: 'success',
                  summary: 'Diversa Normativa',
                  detail: 'Ha sido eliminado con éxito'
                }
              )
            },error =>{
              this.messageService.add(
                { key: 'ks',
                  severity: 'error',
                  summary: 'Error',
                  detail: 'Ha ocurrido algo inesperado al eliminar el voto' })
            })
        },
        reject: () => {
          this.messageService.add(
            { key: 'ks',
              severity: 'info',
              summary: 'Voto',
              detail: 'Acción cancelada' })
        }
    });
  }

  restauraTabla():void{
    this.filtrado = ""
    this.viewSearch = false
    this.busqueda.numero=null
    this.busqueda.tipoInstrumento=null;
    this.saveFilters.filters = {} as SaveFilters;
    this.realizaBusqueda()
  }

  public calculateSizePage(){
    if(this.normativaAllMod.size*(1+this.normativaAllMod.number)>=this.normativaAllMod.totalElements){
      return this.normativaAllMod.totalElements;
    }else{
      return (this.normativaAllMod.size*(1+this.normativaAllMod.number));
    }
  }

  onChangeSize({ value }: any) {
    this.normativaAllMod.size = value;
    this.allNormativa(this.normativaAllMod.number + 1, this.normativaAllMod.size, this.filtrado);
  }

  private allNormativa(page: number, size: number, filtros?: string) {
    this.normativaService.getAllNormativa(page,size,filtros).subscribe(
      data =>{
        if(data.content.length>0){
          this.normativas = data.content;
          this.normativaAllMod = data;
          this.totalRegistros = filtros? data.content.length : data.totalElements
          this.totalPages = data.totalPages
        }else{
          this.totalRegistros = 0
          this.totalPages = 0
          this.normativaAllMod = data
          this.messageService.add(
          { key: 'ks',
                severity: 'info',
                summary: 'Búsqueda',
                detail: 'No se encontraron resultados sobre la búsqueda'
          })
        }
        document.querySelector('.p-datatable-scrollable-body').scroll(0, 0);
      }
    );
  }

  realizaBusqueda(page:number = 1,rows:number = 10, filtro:string = ""):void{
    this.normativaService.getAllNormativa(page,rows,filtro).subscribe(
      data =>{        
        if(data.content.length>0){
          this.normativas = data.content;
          this.normativaAllMod = data;
          this.totalRegistros = filtro? data.content.length : data.totalElements
          this.totalPages = data.totalPages
        }else{
          this.totalRegistros = 0
          this.totalPages = 0
          this.normativaAllMod = data
          this.messageService.add(
          { key: 'ks',
                severity: 'info',
                summary: 'Búsqueda',
                detail: 'No se encontraron resultados sobre la búsqueda'
          })
        }
      }
    )
  }


}
