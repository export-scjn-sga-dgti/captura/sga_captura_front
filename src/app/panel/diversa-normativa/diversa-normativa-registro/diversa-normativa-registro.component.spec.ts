import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiversaNormativaRegistroComponent } from './diversa-normativa-registro.component';

describe('DiversaNormativaRegistroComponent', () => {
  let component: DiversaNormativaRegistroComponent;
  let fixture: ComponentFixture<DiversaNormativaRegistroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiversaNormativaRegistroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiversaNormativaRegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
