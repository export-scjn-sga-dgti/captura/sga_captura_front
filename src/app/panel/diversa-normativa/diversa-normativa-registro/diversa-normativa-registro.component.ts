import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { NormativaApiService } from 'src/app/services/normativa-api.service';
import { Normativa } from 'src/app/models/normativaModel';
import { ConfirmationService } from 'primeng/api'
import { ToastService } from 'src/app/services/toast.service';


@Component({
  selector: 'app-diversa-normativa-registro',
  templateUrl: './diversa-normativa-registro.component.html',
  styleUrls: ['./diversa-normativa-registro.component.scss'],
  providers: [ConfirmationService]

})

export class DiversaNormativaRegistroComponent implements OnInit {

  normativaModel: Normativa = new Normativa()
  temasEspecificos: string = ""
  catalogos: any = {}
  files: string[] = []
  content: any
  index: number = null
  cadena: string = ""
  campos: number = 0
  requerid = {
    fechaAprobacion: false,
    numero: false,
    tipoInstrumento: false
  }

  constructor(private toastService: ToastService,
    private catalogoService: CatalogosService,
    private router: Router,
    private normativaService: NormativaApiService,
    private confirmationService: ConfirmationService,
  ) { }

  ngOnInit(): void {
    this.catalogoService.getCatalogosNormativas().subscribe(
      (data) => {
        this.catalogos = data
      }
    )
  }

  uploadFile(event: FileList) {
    this.cadena = ""
    if (event) {
      for (let i = 0; i < event.length; i++) {
        if (event[i].name.includes('.doc')) {
          this.files.push(event[i].name)
          this.cadena = this.cadena + event[i].name + ', '
        } else
          if (event[i].name.includes('.docx')) {
            this.files.push(event[i].name)
            this.cadena = this.cadena + event[i].name + ', '
          } else {
            this.toastService.showToast(
              'error',
              'Documento no válido',
              event[i].name + '\nno cumple con el formato'
            )
          }
      }
      this.normativaModel.documentos = this.files
    }
  }

  registroNormativa(): void {
    for (const property in this.requerid) {
      this.requerid[property] = !this.normativaModel[property] ? true : false
      if (!this.requerid[property]) this.campos += 1
    }
    if (this.campos < 3) {
      this.toastService.showToast(
        'error',
        'Crear Normativa',
        'Se requieren al menos los campos "Tipo  de instrumento" , "Número" y "Fecha de aprobación"'
      )
    } else {
      this.normativaService.postNormativa(this.normativaModel).subscribe(
        (data) => {
          if (data['diversaNormativa']) {
            this.toastService.showToast('success', 'Creación', 'Se ha creado exitosamente')
            this.router.navigateByUrl('/', { skipLocationChange: true }).then(nav => this.router.navigate(['/diversa-normativa/edicion/' + data['diversaNormativa'].id]))
          }
        }
        , error => {
          this.toastService.showToast('error', 'Error', 'Ha ocurrido algo inesperado al crear el registro')
        }
      )
    }

  }


  borraArchivo(event: Event, index: number) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar el documento?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.files.splice(index, 1)
        this.normativaModel.documentos = this.files
      }, reject: () => {
        this.toastService.showToast(
          'info',
          '',
          'Acción cancelada'
        )
      }
    });
  }

}
