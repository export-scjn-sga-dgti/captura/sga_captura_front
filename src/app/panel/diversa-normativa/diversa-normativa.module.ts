import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { FormsModule } from '@angular/forms';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { TreeTableModule } from 'primeng/treetable';
import { ListboxModule } from 'primeng/listbox';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog'
import { ConfirmationService, MessageService } from 'primeng/api';
import { ChipsModule } from 'primeng/chips';
import { DiversaNormativaRoutingModule } from './diversa-normativa-routing.module';
import { DiversaNormativaRegistroComponent } from './diversa-normativa-registro/diversa-normativa-registro.component';
import { DiversaNormativaEdicionComponent } from './diversa-normativa-edicion/diversa-normativa-edicion.component';
import { ClasificacionMateriaComponent } from 'src/app/components/clasificacion-materia/clasificacion-materia.component';
import { DropdownModule } from 'primeng/dropdown';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
  DiversaNormativaRegistroComponent,
  DiversaNormativaEdicionComponent,
  ClasificacionMateriaComponent],
  imports: [
    CommonModule,
    DiversaNormativaRoutingModule,
    FormsModule,
    BreadcrumbModule,
    ToastModule,
    ConfirmPopupModule,
    CalendarModule,
    TreeTableModule,
    ListboxModule,
    TableModule,
    DialogModule,
    ChipsModule,
    DropdownModule,
    NgbTooltipModule
  ],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class DiversaNormativaModule { }
