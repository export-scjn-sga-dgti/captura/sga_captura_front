import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { NormativaApiService } from 'src/app/services/normativa-api.service';
import { ConverStringArray } from "../../../helper/converArrayObject";
import { Normativa, Materia } from 'src/app/models/normativaModel';
import { ConfirmationService } from 'primeng/api'
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-diversa-normativa-edicion',
  templateUrl: './diversa-normativa-edicion.component.html',
  styleUrls: ['./diversa-normativa-edicion.component.scss'],
  providers: [ConverStringArray, ConfirmationService]
})
export class DiversaNormativaEdicionComponent implements OnInit {
  configModal: { catalogo: [], open: boolean };
  idNormativa: string = "";
  normativaModel: Normativa = new Normativa()
  catalogos: any = {}
  materias: Materia[] = []
  files: string[] = []
  breadcrumb: any = []
  index: number = -1
  materiaEdit: any;

  content: any
  cadena: string = ""
  campos: number = 0
  requerid = {
    fechaAprobacion: false,
    numero: false,
    tipoInstrumento: false
  }
  isView: boolean = false

  constructor(
    private params: ActivatedRoute,
    private toastService: ToastService,
    private normativaService: NormativaApiService,
    private router: Router,
    private confirmationService: ConfirmationService,
    private catalogoService: CatalogosService,

  ) { }

  ngOnInit(): void {

    this.catalogoService.getCatalogosNormativas().subscribe(
      (data) => {
        this.catalogos = data
      }
    )

    this.params.paramMap.subscribe(responseData => {
      this.idNormativa = responseData.get("idNormativa");
      this.params.queryParamMap.subscribe(querys => {
        if (querys.has('visualizar')) {
          this.isView = true
        }
      })
    })

    this.normativaService.getNormativaByID(this.idNormativa).subscribe(
      res => {
        this.normativaModel = res.diversaNormativa
        this.materias = res.materias
        this.breadcrumb = [
          { label: this.normativaModel.tipoInstrumento + " " + this.normativaModel.numero, routerLink: "./", queryParams: this.isView ? { visualizar: true } : {} }
        ]
        this.files = this.normativaModel.documentos ? this.normativaModel.documentos : []
      }

    )

  }

  getModal(response: {
    open: boolean;
    materia: Materia;
  }) {
    let data = {
      id: null,
      idDiversaNormativa: this.idNormativa,
      materiaRegulacion: response.materia.materiaRegulacion,
      temas: response.materia.temas.toString().split(",")
    }
    this.normativaService.postMateriaRegulacion(data).subscribe(
      (res) => {
        this.materias.push(res['materiaRegulacion'])
        this.toastService.showToast(
          'success',
          'Materia',
          'Ha sido creado con éxito',
        );
      },
      (error) => {
        this.toastService.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al crear el rubro temático',
        );
      }
    )

  }


  getModalUpdateMateria(response: {
    open: boolean;
    materia: Materia;
  }) {
    let data = {
      id: response.materia.id,
      idDiversaNormativa: this.idNormativa,
      materiaRegulacion: response.materia.materiaRegulacion,
      temas: response.materia.temas
    }

    this.normativaService.updateMateriaRegulacion(data).subscribe(
      (res) => {
        this.toastService.showToast(
          'success',
          'Materia',
          'Ha sido creado con éxito',
        );
      },
      (error) => {
        this.toastService.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al crear el rubro temático',
        );
      }
    )
  }

  uploadFile(event: FileList) {

    this.cadena = ""
    if (event) {
      for (let i = 0; i < event.length; i++) {
        if (event[i].name.includes('.doc')) {
          this.files.push(event[i].name)
          this.cadena = this.cadena + event[i].name + ', '
        } else
          if (event[i].name.includes('.docx')) {
            this.files.push(event[i].name)
            this.cadena = this.cadena + event[i].name + ', '
          } else {
            this.toastService.showToast(
              'error',
              'Documento no válido',
              event[i].name + '\nno cumple con el formato'
            )
          }
      }
      this.normativaModel.documentos = this.files
    }
  }

  editarNormativa(): void {
    for (const property in this.requerid) {
      this.requerid[property] = !this.normativaModel[property] ? true : false
      if (!this.requerid[property]) this.campos += 1
    }
    if (this.campos < 3) {
      this.toastService.showToast(
        'error',
        'Editar Normativa',
        'Se requieren al menos los campos "Tipo  de instrumento" , "Número" y "Fecha de aprobación"'
      )
    } else {
      this.normativaService.updateNormativa(this.normativaModel).subscribe(
        (data) => {
          this.toastService.showToast(
            'success',
            'Diversa Normativa',
            'Ha sido actualizado con éxito',
          );
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(nav => this.router.navigate(['/diversa-normativa/']))
        },
        (error) => {
          this.toastService.showToast('error', 'Error', 'Ha ocurrido algo inesperado al crear el registro')
        }

      )
    }
  }

  borraArchivo(event: Event, index: number) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar el documento?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.files.splice(index, 1)
        this.normativaModel.documentos = this.files
        this.toastService.showToast(
          'success',
          'Documento',
          'Ha sido eliminado con éxito',
        );
      }, reject: () => {
        this.toastService.showToast(
          'info',
          '',
          'Acción cancelada'

        )
      }
    });
  }

  displayModal() {
    const config = {
      catalogo: this.catalogos['materiaRegulacion'],
      open: true,
    };
    this.configModal = config;
  }

  edicionMateria(materia: Materia) {
    this.materiaEdit = {
      catalogo: this.catalogos['materiaRegulacion'],
      materia: materia,
      open: true,
    };
  }

  borraMaterias(event: Event, index: number) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar la materia de forma permanente?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.normativaService.deleteMateriaRegulacionByID(this.materias[index].id).subscribe(
          (data) => {
            this.materias.splice(index, 1)
            this.toastService.showToast(
              'success',
              'Clasificación',
              'Ha sido eliminada con éxito',
            );
          }, (error) => {
            this.toastService.showToast(
              'error',
              'Clasificación',
              'Ocurrió algo inesperado'
            )
          }
        )

      }, reject: () => {
        this.toastService.showToast(
          'info',
          '',
          'Acción cancelada'
        )
      }
    });
  }

}
