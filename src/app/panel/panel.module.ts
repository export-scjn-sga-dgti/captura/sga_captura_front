import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { PanelRoutingModule } from './panel-routing.module';
import { SentenciaComponent } from './sentencia/sentencia.component';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { FieldsetModule } from 'primeng/fieldset';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { TaquigraficasComponent } from './taquigraficas/taquigraficas.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { VotosComponent } from './votos/votos.component';
import { PaginatorModule } from 'primeng/paginator';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { DiversaNormativaComponent } from './diversa-normativa/diversa-normativa.component';
import { PipesModule } from '../pipes/pipes.module';
import { TooltipModule } from 'primeng/tooltip';
import { AcuerdosGeneralesComponent } from './acuerdos-generales/acuerdos-generales.component';
import { ExploracionDatosComponent } from './exploracion-datos/exploracion-datos.component';
import { AsignacionAsuntosComponent } from './asignacion-asuntos/asignacion-asuntos.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { OrderListModule } from 'primeng/orderlist';
import { DialogService, DynamicDialogModule } from 'primeng/dynamicdialog';
import { DialogModule } from 'primeng/dialog';
import { UsuarioRegistroComponent } from './usuarios/usuario-registro/usuario-registro.component';
import { InputSwitchModule } from 'primeng/inputswitch';

@NgModule({
  declarations: [
    SentenciaComponent,
    TaquigraficasComponent,
    VotosComponent,
    AcuerdosGeneralesComponent,
    ExploracionDatosComponent,
    AsignacionAsuntosComponent,
    UsuariosComponent,
    UsuarioRegistroComponent,
    DiversaNormativaComponent,
  ],
  imports: [
    CommonModule,
    PanelRoutingModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    ReactiveFormsModule,
    FormsModule,
    CheckboxModule,
    CalendarModule,
    DropdownModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgbPaginationModule,
    TooltipModule,
    ConfirmPopupModule,
    PaginatorModule,
    ToastModule,
    PipesModule,
    OrderListModule,
    DynamicDialogModule,
    DialogModule,
    InputSwitchModule
  ],
  providers: [MessageService, ConfirmationService, DatePipe, DialogService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PanelModule {}
