import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Validate } from 'src/app/helper/validateExpediente';
import { SaveFilters } from 'src/app/models/save-filters';
import { AllSentenciaModel, Sentencia } from 'src/app/models/sentenciaModel';
import { SentenciaRegistro } from 'src/app/models/sentenciaRegistro';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { SaveFiltersService } from 'src/app/services/save-filters.service';
import { SentenciaApiService } from 'src/app/services/sentencia-api.service';

@Component({
  selector: 'app-sentencia',
  templateUrl: './sentencia.component.html',
  styleUrls: ['./sentencia.component.scss'],
})
export class SentenciaComponent implements OnInit {
  size: number = 10;
  tipoAsuntoArray: { id: string; catalogo: string; nombre: string }[] = [];
  sentencias: AllSentenciaModel = new AllSentenciaModel();
  filter: Filter = new Filter();
  filtros: string = '';
  isFilter: boolean = false;
  valid: boolean[] = [true, true, true];
  arrayPonente: { id: string; catalogo: string; nombre: string }[] = [];
  constructor(
    private catalogoService: CatalogosService,
    private sentenciaService: SentenciaApiService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private saveFilters: SaveFiltersService
  ) {

    
    this.catalogoService.getCatalogosSentencias().subscribe((catalogos) => {
      this.tipoAsuntoArray = catalogos.tipoAsunto;
      this.arrayPonente = catalogos.ministros
    });

    this.sentenciaService.dataSentencia = {
      id: '',
      model: {} as SentenciaRegistro,
      modific: false,
    };
  }

  ngOnInit(): void {
    if (this.saveFilters.filters.module == 'sentencia') {
      const data = this.saveFilters.filters.filter.split(',')
      data.forEach(keys => {
        const k = keys.split(':')
        this.filter[k[0]] = k[1]
      })

      this.isFilter = true
      this.filtros = this.saveFilters.filters.filter
      this.allSentencias(1, this.size, this.filtros);
    } else {
      this.saveFilters.filters = {} as SaveFilters
      this.allSentencias(this.sentencias.number, this.size, this.filtros);
    }

  }

  private allSentencias(page: number, size: number, filtros?: string) {
    this.sentenciaService.getAllSentencias(page, size, filtros).subscribe(
      (data) => {
        this.sentencias = data;
        document.querySelector('.p-datatable-scrollable-body').scroll(0, 0);
      },
      (error) => { }
    );
  }

  public calculateSizePage() {
    if (this.sentencias.size * (1 + this.sentencias.number) >= this.sentencias.totalElements) {
      return this.sentencias.totalElements;
    } else {
      return (this.sentencias.size * (1 + this.sentencias.number));
    }
  }

  public deleteSentencia(event: Event, sentencia: Sentencia) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar el documento?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.sentenciaService.deleteSentenciaByID(sentencia.id).subscribe(
          (res) => {
            const index = this.sentencias.content.indexOf(sentencia);
            this.sentencias.content.splice(index);

            this.messageService.add({
              key: 'ks',
              severity: 'success',
              summary: 'Eliminado',
              detail: 'Se ha eliminado con éxito la sentencia',
            });
          },
          (error) => {
            this.messageService.add({
              key: 'ks',
              severity: 'error',
              summary: 'Error',
              detail: 'Ha ocurrido algo inesperado al eliminar la sentencia',
            });
          }
        );
      },
      reject: () => {
        //reject action
      },
    });
  }

  public searchByFilter() {
    if (!this.filter.numExpediente && !this.filter.tipoAsunto && !this.filter.fecha && !this.filter.ponente) return;
    if (this.valid.filter((v) => v === false).length >= 1) return;
    this.filtros = '';
    this.filtros += this.filter.numExpediente
      ? `numeroExpediente:${this.filter.numExpediente},`
      : '';
    this.filtros += this.filter.tipoAsunto
      ? `tipoAsunto:${this.filter.tipoAsunto},`
      : '';
    this.filtros += this.filter.ponente
      ? `ponente:${this.filter.ponente},`
      : '';
    this.filtros += this.filter.fecha
      ? `fechaResolucion:${this.filter.fecha},`
      : '';
    this.isFilter = true;
    this.saveFilters.filters.module = "sentencia"
    this.saveFilters.filters.filter = this.filtros
    this.allSentencias(1, this.size, this.filtros);
  }

  public clearFilter() {
    this.isFilter = false;
    this.filtros = '';
    this.filter = new Filter();
    this.saveFilters.filters = {} as SaveFilters
    this.allSentencias(1, this.size, '');
  }

  public onChangePage(page: number) {
    this.allSentencias(page, this.size, this.filtros);
  }

  onChangeSize({ value }: any) {
    this.size = value;
    this.allSentencias(this.sentencias.number + 1, this.size, this.filtros);
  }

  public validCamp(field: string): void {
    switch (field) {
      case 'asunto':
        if (this.filter.tipoAsunto) {
          const index = this.tipoAsuntoArray.findIndex(
            (data: any) => data.nombre === this.filter.tipoAsunto
          );
          this.valid[0] = index != -1 ? true : false;
        } else {
          this.valid[0] = true;
        }
        break;
      case 'num':
        if (this.filter.numExpediente) {
          this.valid[1] = Validate(this.filter.numExpediente);
        } else {
          this.valid[1] = false;
        }
        break;
      case 'ponente':
        if (this.filter.ponente) {
          const index = this.arrayPonente.findIndex(
            (data: any) => data.nombre === this.filter.ponente
          );
          this.valid[2] = index != -1 ? true : false;
        } else {
          this.valid[2] = true;
        }
        break;
    }
  }
}

export class Filter {
  numExpediente: string = '';
  tipoAsunto: string;
  ponente: string;
  fecha: string;
}
