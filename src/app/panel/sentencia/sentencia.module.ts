import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputSwitchModule } from 'primeng/inputswitch';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { TreeTableModule } from 'primeng/treetable';
import { DialogModule } from 'primeng/dialog';

import { SentenciaRoutingModule } from './sentencia-routing.module';
import { SentenciaRegistroComponent } from './sentencia-registro/sentencia-registro.component';
import { CausaImprocedenciaComponent } from './causa-improcedencia/causa-improcedencia.component';
import { DerechosHumanosComponent } from './derechos-humanos/derechos-humanos.component';
import { TipoVicioLegislativoComponent } from './tipo-vicio-legislativo/tipo-vicio-legislativo.component';
import { TipoViolacionEsferasComponent } from './tipo-violacion-esferas/tipo-violacion-esferas.component';
import { TipoViolacionPoderesComponent } from './tipo-violacion-poderes/tipo-violacion-poderes.component';
import { RubrosTematicosComponent } from 'src/app/components/rubros-tematicos/rubros-tematicos.component';
import { SentenciaEdicionComponent } from './sentencia-edicion/sentencia-edicion.component';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TooltipModule } from 'primeng/tooltip';
import { OrderListModule } from 'primeng/orderlist';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  declarations: [
    SentenciaRegistroComponent,
    CausaImprocedenciaComponent,
    DerechosHumanosComponent,
    TipoVicioLegislativoComponent,
    TipoViolacionEsferasComponent,
    TipoViolacionPoderesComponent,
    RubrosTematicosComponent,
    SentenciaEdicionComponent,
  ],
  imports: [
    CommonModule,
    SentenciaRoutingModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    ReactiveFormsModule,
    FormsModule,
    CheckboxModule,
    CalendarModule,
    DropdownModule,
    NgMultiSelectDropDownModule.forRoot(),
    InputSwitchModule,
    BreadcrumbModule,
    TreeTableModule,
    DialogModule,
    ToastModule,
    ConfirmPopupModule,
    NgbTooltipModule,
    TooltipModule,
    OrderListModule,
    PipesModule
  ],
  providers: [ConfirmationService, MessageService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SentenciaModule {}
