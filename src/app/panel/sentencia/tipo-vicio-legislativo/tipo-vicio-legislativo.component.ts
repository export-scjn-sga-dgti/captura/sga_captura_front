import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfirmationService } from 'primeng/api';
import { ConverStringArray } from 'src/app/helper/converArrayObject';
import { ScrollTop } from 'src/app/helper/scrollTop';
import {
  RubrosEfectoSentencia,
  VotacionTipoVicioProcesoLegislativoAnalizadoenSentencia,
} from 'src/app/models/sentenciaRegistro';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ProcesamientoTextoService } from 'src/app/services/procesamiento-texto.service';
import { SentenciaApiService } from 'src/app/services/sentencia-api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-tipo-vicio-legislativo',
  templateUrl: './tipo-vicio-legislativo.component.html',
  styleUrls: ['./tipo-vicio-legislativo.component.scss'],
  providers: [ConverStringArray],
})
export class TipoVicioLegislativoComponent implements OnInit {
  scroll: ScrollTop = new ScrollTop();
  configRubros: any;
  catalogos: any = {};
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  votacionPlanteada: VotacionTipoVicioProcesoLegislativoAnalizadoenSentencia =
    new VotacionTipoVicioProcesoLegislativoAnalizadoenSentencia();
  rubro: string = '';
  indexOfRubro: number = -1;
  idSentencia: string = '';
  rubrosEdit: any;
  idSentenciaVotacion: string = '';
  isEdit: boolean = false;
  indexOfRubrosSentencia: number = -1;
  asunto: string = '';
  isView: boolean = false
  tipoVicioProcesoLegislativo = [];
  configDropdownVicio: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    textField: 'nombre',
    idField: 'nombre',
    allowSearchFilter: true,
    enableCheckAll: false,
    searchPlaceholderText: 'Buscar'
  }

  constructor(
    private catalogosService: CatalogosService,
    private conver: ConverStringArray,
    private sentenciaService: SentenciaApiService,
    private router: Router,
    private param: ActivatedRoute,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private textProcService: ProcesamientoTextoService
  ) {
    this.scroll.scrollTo();

    this.param.paramMap.subscribe((route) => {
      this.idSentencia = route.get('id');

      this.sentenciaService
        .getSentenciaByID(this.idSentencia)
        .subscribe((data) => {
          this.textProcService.setFechaResolucion(data.fechaResolucion);
          this.asunto =
            ' ' + data.tipoAsunto + ' ' + data.numeroExpediente + ' - ';
        });

      this.idSentenciaVotacion = route.get('idVotacion');

      if (route.has('idVotacion')) {
        this.getVotacion();
        this.isEdit = true;
      }

      this.param.queryParamMap.subscribe(query => {
        if (query.has('visualizar')) {
          this.isView = true
        }
      })
    });
  }

  ngOnInit(): void {
    this.catalogosService.getCatalogosSentencias().subscribe((data) => {
      data.tipoSentencia = this.conver.proccessArray(data.tipoSentencia);
      this.catalogos = data;
    });


    this.catalogosService.getCatalogoByName("sentencia_tipoVicioProcesoLegislativo").subscribe((data) => {
      if (data) {
        this.tipoVicioProcesoLegislativo = [];
        data.forEach(xx => {
          this.tipoVicioProcesoLegislativo.push(xx.nombre);
        });
      }
      //console.log(this.tipoVicioProcesoLegislativo);
    });


  }

  displayRubros(rubroRelacion: string) {
    const rubro = {
      tipo: rubroRelacion,
      open: true,
      edicion: false
    };
    this.configRubros = rubro;
  }

  getModal(response: {
    tipo: string;
    open: boolean;
    rubro: RubrosEfectoSentencia;
  }) {
    if (this.votacionPlanteada.sentidoResolucion) response.rubro.sentido = this.votacionPlanteada.sentidoResolucion;
    if (this.votacionPlanteada.tipoSentencia) response.rubro.tipoSentencia = this.votacionPlanteada.tipoSentencia;
    let data = {
      idVotacion: this.idSentenciaVotacion,
      idSentencia: this.idSentencia,
      rubro: response.rubro,
    };
    this.sentenciaService.postRubroTematico(data).subscribe(
      (res) => {
        if (response.tipo == 'efectos') {
          this.votacionPlanteada.rubrosEfectoSentencia.push(res);
        } else {
          this.votacionPlanteada.rubrosExtencionInvalidez.push(res);
        }
        this.toast.showToast(
          'success',
          'Rubro temático',
          'Ha sido creado con éxito'
        );
      },
      (error) => {
        this.toast.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al crear el rubro temático'
        );
      }
    );
  }

  crearVicioPlanteado() {
    const data = {
      idSentencia: this.idSentencia,
      votacion: this.votacionPlanteada,
    };

    if (this.votacionPlanteada.votacionFavor.length > 0 || this.votacionPlanteada.votacionEnContra.length > 0) {
      this.sentenciaService.postSentenciaVotacion(data).subscribe((res) => {
        if (res.id) {
          this.toast.showToast(
            'success',
            'Creación',
            'Se ha creado exitosamente la votación'
          );
          this.router
            .navigateByUrl('/', { skipLocationChange: true })
            .then((success) => {
              this.router.navigate([
                '/sentencia',
                this.idSentencia,
                'tipo-vicio-legislativo',
                res.id,
              ]);
            });
        }
      });

    } else {
      this.toast.showToast(
        'error',
        'Votación requerida',
        'Al menos una votación se requiere'
      );
    }
  }

  updateVicioPlanteado() {

    this.votacionPlanteada.idSentencia = this.idSentencia;

    if (this.votacionPlanteada.sentidoResolucion != 'FUNDADA' && this.votacionPlanteada.rubrosEfectoSentencia.length > 0) return this.validateVotacioneEfectos()
    if (!this.votacionPlanteada.tipoSentencia.includes('EXTENSIVA A NORMAS DIVERSAS') && this.votacionPlanteada.rubrosExtencionInvalidez.length > 0) return this.validateVotacionesInvalidez()

    this.sentenciaService
      .putSentenciaVotacion(this.votacionPlanteada)
      .subscribe((res) => {
        if (res.id) {
          this.toast.showToast(
            'success',
            'Actualización',
            'Se ha actualizado exitosamente la votación'
          );
          this.router
            .navigateByUrl('/', { skipLocationChange: true })
            .then((success) => {
              this.router.navigate(['/sentencia', this.idSentencia]);
            });
        }
      }, error => {
        this.toast.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al actualizar la votación'
        );
      });
  }

  validateVotacioneEfectos() {
    this.toast.showToast(
      'error',
      'Actualización no permitida',
      `Contiene votaciones relacionadas con efectos de la sentencia en campo sentido de la resolución.
       Corrija o elimine votaciones`
    );
  }

  validateVotacionesInvalidez() {
    this.toast.showToast(
      'error',
      'Actualización no permitida',
      `Contiene votaciones relacionadas con la extensión de invalidez en campo tipo de sentencia.
      Corrija o elimine votaciones`
    );
  }

  agregarRubroPrincipal() {
    if (this.rubro.includes('\n')) {
      const linesRubros = this.rubro.split('\n');
      linesRubros.forEach((text) => {
        if (text != '')
          this.votacionPlanteada.rubrosTematicosPrincipal.push(text);
      });
    } else {
      if (this.rubro.trim())
        this.votacionPlanteada.rubrosTematicosPrincipal.push(this.rubro);
    }
    const data = this.votacionPlanteada.rubrosTematicosPrincipal
    this.votacionPlanteada.rubrosTematicosPrincipal = []
    setTimeout(() => {
      this.votacionPlanteada.rubrosTematicosPrincipal = data
    }, 300)
    if (this.isEdit && !this.isView) {
      this.sentenciaService.updateOrderRubro({ id: this.idSentenciaVotacion, rubrosTematicosPrincipal: data }).subscribe(data => {
        if (data) {
          this.toast.showToast('success', 'Creación', 'Rubros temáticos se ha creado con éxito')
        }
      })
    }
    this.rubro = '';
  }

  editRubroTematico(rubro: string, i: number) {
    this.indexOfRubro = i;
    this.rubro = rubro;
  }

  orderRubro() {
    if (this.isEdit && !this.isView) {
      this.sentenciaService.updateOrderRubro({ id: this.idSentenciaVotacion, rubrosTematicosPrincipal: this.votacionPlanteada.rubrosTematicosPrincipal }).subscribe(data => {
        if (data) {
          this.toast.showToast('success', 'Actualización', 'Rubros temáticos se ha actualizado con éxito')
        }
      })
    }
  }

  deleteRubroPrincipal(event: Event, index: number) {
    this.confirmationService.confirm({
      icon: 'pi pi-trash',
      message: '¿Estás seguro de eliminar permanentemente el rubro temático?',
      target: event.target,
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.votacionPlanteada.rubrosTematicosPrincipal.splice(index, 1);
        const actual = this.votacionPlanteada.rubrosTematicosPrincipal;
        this.sentenciaService.updateOrderRubro({
          id: this.idSentenciaVotacion, rubrosTematicosPrincipal:
            this.votacionPlanteada.rubrosTematicosPrincipal
        }).subscribe(
          data => {
            if (data) {
              this.votacionPlanteada.rubrosTematicosPrincipal = [];
              setTimeout(() => {
                this.votacionPlanteada.rubrosTematicosPrincipal = actual;
              }, 300)
              this.toast.showToast(
                'success',
                'Rubro temático eliminado',
                'Ha sido eliminado con éxito'
              );
            }
          }
        )
      },
    });
  }

  actualizarRubro() {
    this.votacionPlanteada.rubrosTematicosPrincipal[this.indexOfRubro] =
      this.rubro;
    this.rubro = '';
    const data = this.votacionPlanteada.rubrosTematicosPrincipal
    this.votacionPlanteada.rubrosTematicosPrincipal = []
    setTimeout(() => {
      this.votacionPlanteada.rubrosTematicosPrincipal = data
    }, 300)
    if (this.isEdit && !this.isView) {
      this.sentenciaService.updateOrderRubro({ id: this.idSentenciaVotacion, rubrosTematicosPrincipal: data }).subscribe(data => {
        if (data) {
          this.toast.showToast('success', 'Actualización', 'Rubros temáticos se ha actualizado con éxito')
        }
      })
    }
    this.indexOfRubro = -1;
  }

  getVotacion() {
    this.sentenciaService
      .getSentenciaVotacion(this.idSentenciaVotacion)
      .subscribe((res) => {
        this.votacionPlanteada = res;
      });
  }

  edicionRubro(sentencia: RubrosEfectoSentencia, tipoRubro: string) {
    if (tipoRubro === 'efectos') {
      this.indexOfRubrosSentencia =
        this.votacionPlanteada.rubrosEfectoSentencia.indexOf(sentencia);
    } else {
      this.indexOfRubrosSentencia =
        this.votacionPlanteada.rubrosExtencionInvalidez.indexOf(sentencia);
    }
    this.rubrosEdit = {
      rubros: sentencia,
      tipo: tipoRubro,
      open: true,
      edicion: true
    };
  }

  deleteRubro(event: Event, rubro: RubrosEfectoSentencia, tipoRubro: string) {
    this.confirmationService.confirm({
      icon: 'pi pi-trash',
      message: '¿Estás seguro de eliminar permanentemente el rubro temático?',
      target: event.target,
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.sentenciaService
          .deleteRubroTematico(rubro.id, this.idSentenciaVotacion, this.idSentencia)
          .subscribe(
            (res) => {
              this.toast.showToast(
                'success',
                'Rubro temático eliminado',
                'Ha sido eliminado con éxito'
              );
              let indexOfRubro = -1;
              if (tipoRubro === 'efectos') {
                indexOfRubro =
                  this.votacionPlanteada.rubrosEfectoSentencia.indexOf(rubro);
                this.votacionPlanteada.rubrosEfectoSentencia.splice(
                  indexOfRubro,
                  1
                );
              } else {
                indexOfRubro =
                  this.votacionPlanteada.rubrosExtencionInvalidez.indexOf(
                    rubro
                  );
                this.votacionPlanteada.rubrosExtencionInvalidez.splice(
                  indexOfRubro,
                  1
                );
              }
            },
            (error) => { }
          );
      },
    });
  }

  getModalUpdateRubro(response: {
    tipo: string;
    open: boolean;
    rubro: RubrosEfectoSentencia;
  }) {
    if (this.votacionPlanteada.sentidoResolucion) response.rubro.sentido = this.votacionPlanteada.sentidoResolucion;
    if (this.votacionPlanteada.tipoSentencia) response.rubro.tipoSentencia = this.votacionPlanteada.tipoSentencia;
    this.sentenciaService.putRubroTematico(response.rubro).subscribe(
      (res) => {
        if (response.tipo == 'efectos') {
          this.votacionPlanteada.rubrosEfectoSentencia[
            this.indexOfRubrosSentencia
          ] = res;
        } else {
          this.votacionPlanteada.rubrosExtencionInvalidez[
            this.indexOfRubrosSentencia
          ] = res;
        }
        this.toast.showToast(
          'success',
          'Rubro temático',
          'Ha sido actualizado con éxito'
        );
        this.indexOfRubrosSentencia = -1;
      },
      (error) => {
        this.toast.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al actualizar el rubro temático'
        );
      }
    );
  }

  processText() {
    if (this.votacionPlanteada.textoVotacion) {
      let data = {
        text: this.votacionPlanteada.textoVotacion,
        fechaResolucion: '',
      };
      this.textProcService.processTex(data).subscribe(
        (resp) => {
          this.votacionPlanteada.ministros = resp.ministros;
          this.votacionPlanteada.votacionEnContra = resp.ministrosEnContra
            ? resp.ministrosEnContra
            : [];
          this.votacionPlanteada.votacionFavor = resp.ministrosAFavor
            ? resp.ministrosAFavor
            : [];
        },
        (error) => {
          console.log('Ha ocurrdio algo inesperado');
        }
      );
    }
  }

  markAllFavor({ checked }) {
    if (checked) {
      this.votacionPlanteada.votacionFavor = this.votacionPlanteada.ministros
      this.votacionPlanteada.votacionEnContra = []
    } else {
      this.votacionPlanteada.votacionFavor = []
    }
  }

  markAllContra({ checked }) {
    if (checked) {
      this.votacionPlanteada.votacionEnContra = this.votacionPlanteada.ministros
      this.votacionPlanteada.votacionFavor = []
    } else {
      this.votacionPlanteada.votacionEnContra = []
    }
  }
}
