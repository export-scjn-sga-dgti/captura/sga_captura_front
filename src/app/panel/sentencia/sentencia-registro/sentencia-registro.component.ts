import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ScrollTop } from 'src/app/helper/scrollTop';
import { Validate } from 'src/app/helper/validateExpediente';
import { DatosSentencia } from 'src/app/models/sentenciaModel';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { SentenciaApiService } from 'src/app/services/sentencia-api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-sentencia-registro',
  templateUrl: './sentencia-registro.component.html',
  styleUrls: ['./sentencia-registro.component.scss'],
})
export class SentenciaRegistroComponent implements OnInit {
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    idField: 'nombre',
    textField: 'nombre',
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  scroll: ScrollTop = new ScrollTop();
  catalogos: any = {};
  sentenciaModel: DatosSentencia = new DatosSentencia();
  requerid = {
    asunto: false,
    expediente: false,
    organo: false,
    fecha: false,
    ponente: false,
  };

  constructor(
    private router: Router,
    private catalogoService: CatalogosService,
    private sentenciaService: SentenciaApiService,
    private toast: ToastService
  ) {
    this.scroll.scrollTo();
  }
  ngOnInit(): void {
    this.catalogoService.getCatalogosSentencias().subscribe((data) => {
      this.catalogos = data;
    });
  }

  redirectTo(path: string) {
    this.router
      .navigateByUrl('/', { skipLocationChange: true })
      .then((nav) => this.router.navigate(['/sentencia/registro/', path]));
  }

  crearSentencia() {
    const validate = Validate(this.sentenciaModel.sentencia.numeroExpediente);
    if (
      !this.sentenciaModel.sentencia.tipoAsunto ||
      !this.sentenciaModel.sentencia.numeroExpediente ||
      !this.sentenciaModel.sentencia.fechaResolucion ||
      !this.sentenciaModel.sentencia.ponente ||
      !this.sentenciaModel.sentencia.organoRadicacion
    ) {
      this.requerid.ponente = this.sentenciaModel.sentencia.ponente
        ? false
        : true;
      this.requerid.expediente = this.sentenciaModel.sentencia.numeroExpediente
        ? false
        : true;
      this.requerid.asunto = this.sentenciaModel.sentencia.tipoAsunto
        ? false
        : true;
      this.requerid.organo = this.sentenciaModel.sentencia.organoRadicacion
        ? false
        : true;
      this.requerid.fecha = this.sentenciaModel.sentencia.fechaResolucion
        ? false
        : true;
      this.toast.showToast(
        'error',
        'Campos obligatorios',
        'Favor de llenar los campos requeridos'
      );
    } else {
      if (validate) {
        this.requerid.expediente = false;
        this.sentenciaService
          .postSentencia(this.sentenciaModel.sentencia)
          .subscribe(
            (res) => {
              if (res['idSentencia']) {
                this.toast.showToast(
                  'success',
                  'Sentencia',
                  'Ha sido creado con éxito'
                );
                this.router
                  .navigateByUrl('/', { skipLocationChange: true })
                  .then((nav) =>
                    this.router.navigate(['/sentencia', res['idSentencia']])
                  );
              }
            },
            (error) => {
              this.toast.showToast(
                'error',
                'Error',
                'Ha ocurrido algo inesperado al crear la sentencia'
              );
            }
          );
      } else {
        this.toast.showToast(
          'warn',
          'Alerta',
          'Número de expediente no válido'
        );
      }
    }
  }
}
