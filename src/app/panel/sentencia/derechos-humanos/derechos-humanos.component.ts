import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfirmationService } from 'primeng/api';
import { ConverStringArray } from 'src/app/helper/converArrayObject';
import { ScrollTop } from 'src/app/helper/scrollTop';
import { DataHojas } from 'src/app/models/hojasModel';
import {
  RubrosEfectoSentencia,
  VotacionDerechosHumanos,
} from 'src/app/models/votacionDerechosHumanos';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ProcesamientoTextoService } from 'src/app/services/procesamiento-texto.service';
import { SentenciaApiService } from 'src/app/services/sentencia-api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-derechos-humanos',
  templateUrl: './derechos-humanos.component.html',
  styleUrls: ['./derechos-humanos.component.scss'],
  providers: [ConverStringArray],
})
export class DerechosHumanosComponent implements OnInit {
  scroll: ScrollTop = new ScrollTop();
  configRubros: { tipo: string; open: boolean };
  catalogos: any = {};
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  actos: string[] = [];
  derechosHumanos: VotacionDerechosHumanos = new VotacionDerechosHumanos();
  rubro: string = '';
  rubrosEdit: any;
  idSentencia: string = '';
  idSentenciaVotacion: string = '';
  isEdit: boolean = false;
  indexOfRubro: number = -1;
  indexOfRubrosSentencia: number = -1;
  asunto: string = '';
  isView: boolean = false

  constructor(
    private catalogosService: CatalogosService,
    private conver: ConverStringArray,
    private sentenciaService: SentenciaApiService,
    private param: ActivatedRoute,
    private route: Router,
    private confirmationService: ConfirmationService,
    private toast: ToastService,
    private textProcService: ProcesamientoTextoService
  ) {
    this.scroll.scrollTo();

    this.param.paramMap.subscribe((route) => {
      this.idSentencia = route.get('id');
      this.sentenciaService
        .getSentenciaByID(this.idSentencia)
        .subscribe((data) => {
          this.textProcService.setFechaResolucion(data.fechaResolucion);
          this.asunto =
            ' ' + data.tipoAsunto + ' ' + data.numeroExpediente + ' - ';

          const reclamado: DataHojas = {
            tipoAsunto: data.tipoAsunto,
            anio: Number(data.numeroExpediente.split('/')[1]),
            consecutivo: Number(data.numeroExpediente.split('/')[0]),
          };
          this.sentenciaService.getReclamacion(reclamado).subscribe((res) => {
            let acto: string[] = [];
            acto.push(res);
            this.actos = acto;
          });

        });
      this.idSentenciaVotacion = route.get('idVotacion');

      if (route.has('idVotacion')) {
        this.getVotacion();
        this.isEdit = true;
      }

      this.param.queryParamMap.subscribe(query => {
        if (query.has('visualizar')) {
          this.isView = true
        }
      })

    });


  }


  ngOnInit(): void {
    this.catalogosService.getCatalogosSentencias().subscribe((res) => {
      res.derechosHumanos = this.conver.proccessArray(res.derechosHumanos);
      res.tipoSentencia = this.conver.proccessArray(res.tipoSentencia);
      this.catalogos = res;
    });
  }

  displayRubros(rubroRelacion: string) {
    const rubro = {
      tipo: rubroRelacion,
      open: true,
      edicion : false
    };
    this.configRubros = rubro;
  }

  getModal(response: {
    tipo: string;
    open: boolean;
    rubro: RubrosEfectoSentencia;
  }) {
    if(this.derechosHumanos.votacion.sentidoResolucion) response.rubro.sentido = this.derechosHumanos.votacion.sentidoResolucion;
    if(this.derechosHumanos.votacion.tipoSentencia) response.rubro.tipoSentencia = this.derechosHumanos.votacion.tipoSentencia;
    let data = {
      idVotacion: this.idSentenciaVotacion,
      idSentencia: this.idSentencia,
      rubro: response.rubro,
    };
    this.sentenciaService.postRubroTematico(data).subscribe(
      (res) => {
        if (response.tipo == 'efectos') {
          this.derechosHumanos.votacion.rubrosEfectoSentencia.push(res);
        } else {
          this.derechosHumanos.votacion.rubrosExtencionInvalidez.push(res);
        }
        this.toast.showToast(
          'success',
          'Rubro temático',
          'Ha sido creado con éxito'
        );
      },
      (error) => {
        this.toast.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al crear el rubro temático'
        );
      }
    );
  }

  creacionDerechosHumanos() {
    this.derechosHumanos.idSentencia = this.idSentencia;
    if (this.derechosHumanos.votacion.votacionFavor.length > 0 || this.derechosHumanos.votacion.votacionEnContra.length > 0) {
      this.sentenciaService
        .postSentenciaVotacion(this.derechosHumanos)
        .subscribe((res) => {
          if (res.id) {
            this.toast.showToast(
              'success',
              'Creación',
              'Se ha creado exitosamenten la votación'
            );
            this.route
              .navigateByUrl('/', { skipLocationChange: true })
              .then((success) => {
                if (success)
                  this.route.navigate([
                    '/sentencia',
                    this.idSentencia,
                    'derechos-humanos',
                    res.id,
                  ]);
              });
          }
        });

    } else {
      this.toast.showToast(
        'error',
        'Votación requerida',
        'Al menos una votación se requiere'
      );
    }
  }

  edicionDerechosHumanos() {
    this.derechosHumanos.votacion.idSentencia = this.idSentencia;

    if (this.derechosHumanos.votacion.sentidoResolucion != 'FUNDADA' && this.derechosHumanos.votacion.rubrosEfectoSentencia.length > 0) return this.validateVotacioneEfectos()
    if (!this.derechosHumanos.votacion.tipoSentencia.includes('EXTENSIVA A NORMAS DIVERSAS') && this.derechosHumanos.votacion.rubrosExtencionInvalidez.length > 0) return this.validateVotacionesInvalidez()

    this.sentenciaService
      .putSentenciaVotacion(this.derechosHumanos.votacion)
      .subscribe(
        (res) => {
          if (res.id) {
            this.toast.showToast(
              'success',
              'Actualización',
              'Se ha actualizado exitosamenten la votación'
            );
            this.route
              .navigateByUrl('/', { skipLocationChange: true })
              .then((success) => {
                if (success)
                  this.route.navigate(['/sentencia', this.idSentencia]);
              });
          }
        },
        (error) => {
          this.toast.showToast(
            'error',
            'Error',
            'Ha ocurrido algo inesperado al actualizar la votación'
          );
         }
      );
  }


  
  validateVotacioneEfectos() {
    this.toast.showToast(
      'error',
      'Actualización no permitida',
      `Contiene votaciones relacionadas con efectos de la sentencia en campo sentido de la resolución.
       Corrija o elimine votaciones`
    );
  }

  validateVotacionesInvalidez() {
    this.toast.showToast(
      'error',
      'Actualización no permitida',
      `Contiene votaciones relacionadas con la extensión de invalidez en campo tipo de sentencia.
      Corrija o elimine votaciones`
    );
  }

  agregarRubroPrincipal() {
    if (this.rubro.includes('\n')) {
      const linesRubros = this.rubro.split('\n');
      linesRubros.forEach((text) => {
        if (text != '')
          this.derechosHumanos.votacion.rubrosTematicosPrincipal.push(text);
      });
    } else {
      if (this.rubro.trim())
        this.derechosHumanos.votacion.rubrosTematicosPrincipal.push(this.rubro);
    }
    const data = this.derechosHumanos.votacion.rubrosTematicosPrincipal
    this.derechosHumanos.votacion.rubrosTematicosPrincipal = []
    setTimeout(() => {
      this.derechosHumanos.votacion.rubrosTematicosPrincipal = data
    }, 300)

    if (this.isEdit && !this.isView) {
      this.sentenciaService.updateOrderRubro({ id: this.idSentenciaVotacion, rubrosTematicosPrincipal: data }).subscribe(data => {
        if (data) {
          this.toast.showToast('success', 'Creación', 'Rubros temáticos se ha creado con éxito')
        }
      })
    }

    this.rubro = '';
  }

  getVotacion() {
    this.sentenciaService
      .getSentenciaVotacion(this.idSentenciaVotacion)
      .subscribe(
        (data) => {
          this.derechosHumanos.votacion = data;
        },
        (error) => { }
      );
  }

  editRubroTematico(rubro: string, i: number) {
    this.indexOfRubro = i;
    this.rubro = rubro;
  }

  actualizarRubro() {
    this.derechosHumanos.votacion.rubrosTematicosPrincipal[this.indexOfRubro] =
      this.rubro;
    this.rubro = '';
    const data = this.derechosHumanos.votacion.rubrosTematicosPrincipal
    this.derechosHumanos.votacion.rubrosTematicosPrincipal = []
    setTimeout(() => {
      this.derechosHumanos.votacion.rubrosTematicosPrincipal = data
    }, 300)

    if (this.isEdit && !this.isView) {
      this.sentenciaService.updateOrderRubro({ id: this.idSentenciaVotacion, rubrosTematicosPrincipal: data }).subscribe(data => {
        if (data) {
          this.toast.showToast('success', 'Actualización', 'Rubros temáticos se ha actualizado con éxito')
        }
      })
    }
    this.indexOfRubro = -1;
  }

  orderRubro() {
    if (this.isEdit && !this.isView) {
      this.sentenciaService.updateOrderRubro({ id: this.idSentenciaVotacion, rubrosTematicosPrincipal: this.derechosHumanos.votacion.rubrosTematicosPrincipal }).subscribe(data => {
        if (data) {
          this.toast.showToast('success', 'Actualización', 'Rubros temáticos se ha actualizado con éxito')
        }
      })
    }
  }

  deleteRubroPrincipal(event: Event, index: number) {
    this.confirmationService.confirm({
      icon: 'pi pi-trash',
      message: '¿Estás seguro de eliminar permanentemente el rubro temático?',
      target: event.target,
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.derechosHumanos.votacion.rubrosTematicosPrincipal.splice(index, 1);
        const actual = this.derechosHumanos.votacion.rubrosTematicosPrincipal;
        this.sentenciaService.updateOrderRubro({
          id: this.idSentenciaVotacion, rubrosTematicosPrincipal:
            this.derechosHumanos.votacion.rubrosTematicosPrincipal
        }).subscribe(
          data => {
            if (data) {
              this.derechosHumanos.votacion.rubrosTematicosPrincipal = [];
              setTimeout(() => {
                this.derechosHumanos.votacion.rubrosTematicosPrincipal = actual;
              }, 300)
              this.toast.showToast(
                'success',
                'Rubro temático eliminado',
                'Ha sido eliminado con éxito'
              );
            }
          }
        )
      },
    });
  }


  edicionRubro(sentencia: RubrosEfectoSentencia, tipoRubro: string) {
    if (tipoRubro === 'efectos') {
      this.indexOfRubrosSentencia =
        this.derechosHumanos.votacion.rubrosEfectoSentencia.indexOf(sentencia);
    } else {
      this.indexOfRubrosSentencia =
        this.derechosHumanos.votacion.rubrosExtencionInvalidez.indexOf(
          sentencia
        );
    }
    this.rubrosEdit = {
      rubros: sentencia,
      tipo: tipoRubro,
      open: true,
      edicion : true
    };
  }

  deleteRubro(event: Event, rubro: RubrosEfectoSentencia, tipoRubro: string) {
    this.confirmationService.confirm({
      icon: 'pi pi-trash',
      message: '¿Estás seguro de eliminar permanentemente el rubro temático?',
      target: event.target,
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.sentenciaService
          .deleteRubroTematico(rubro.id, this.idSentenciaVotacion, this.idSentencia)
          .subscribe(
            (res) => {
              this.toast.showToast(
                'success',
                'Rubro temático eliminado',
                'Ha sido eliminado con éxito'
              );
              let indexOfRubro = -1;
              if (tipoRubro === 'efectos') {
                indexOfRubro =
                  this.derechosHumanos.votacion.rubrosEfectoSentencia.indexOf(
                    rubro
                  );
                this.derechosHumanos.votacion.rubrosEfectoSentencia.splice(
                  indexOfRubro,
                  1
                );
              } else {
                indexOfRubro =
                  this.derechosHumanos.votacion.rubrosExtencionInvalidez.indexOf(
                    rubro
                  );
                this.derechosHumanos.votacion.rubrosExtencionInvalidez.splice(
                  indexOfRubro,
                  1
                );
              }
            },
            (error) => { }
          );
      },
    });
  }

  getModalUpdateRubro(response: {
    tipo: string;
    open: boolean;
    rubro: RubrosEfectoSentencia;
  }) {
    if(this.derechosHumanos.votacion.sentidoResolucion) response.rubro.sentido = this.derechosHumanos.votacion.sentidoResolucion;
    if(this.derechosHumanos.votacion.tipoSentencia) response.rubro.tipoSentencia = this.derechosHumanos.votacion.tipoSentencia;
    this.sentenciaService.putRubroTematico(response.rubro).subscribe(
      (res) => {
        if (response.tipo == 'efectos') {
          this.derechosHumanos.votacion.rubrosEfectoSentencia[
            this.indexOfRubrosSentencia
          ] = res;
        } else {
          this.derechosHumanos.votacion.rubrosExtencionInvalidez[
            this.indexOfRubrosSentencia
          ] = res;
        }
        this.toast.showToast(
          'success',
          'Rubro temático',
          'Ha sido actualizado con éxito'
        );
        this.indexOfRubrosSentencia = -1;
      },
      (error) => {
        this.toast.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al actualizar el rubro temático'
        );
      }
    );
  }

  processText() {
    if (this.derechosHumanos.votacion.textoVotacion) {
      let data = {
        text: this.derechosHumanos.votacion.textoVotacion,
        fechaResolucion: '',
      };
      this.textProcService.processTex(data).subscribe(
        (resp) => {
          this.derechosHumanos.votacion.ministros = resp.ministros;
          this.derechosHumanos.votacion.votacionEnContra =
            resp.ministrosEnContra ? resp.ministrosEnContra : [];
          this.derechosHumanos.votacion.votacionFavor = resp.ministrosAFavor
            ? resp.ministrosAFavor
            : [];
        },
        (error) => {
          console.log('Ha ocurrdio algo inesperado');
        }
      );
    }
  }

  markAllFavor({ checked }) {
    if (checked) {
      this.derechosHumanos.votacion.votacionFavor = this.derechosHumanos.votacion.ministros
      this.derechosHumanos.votacion.votacionEnContra = []
    } else {
      this.derechosHumanos.votacion.votacionFavor = []
    }
  }

  markAllContra({ checked }) {
    if (checked) {
      this.derechosHumanos.votacion.votacionEnContra = this.derechosHumanos.votacion.ministros
      this.derechosHumanos.votacion.votacionFavor = []
    } else {
      this.derechosHumanos.votacion.votacionEnContra = []
    }
  }

}
