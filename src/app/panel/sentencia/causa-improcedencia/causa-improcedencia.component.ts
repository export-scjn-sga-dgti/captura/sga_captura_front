import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfirmationService } from 'primeng/api';
import { ConverStringArray } from 'src/app/helper/converArrayObject';
import { ScrollTop } from 'src/app/helper/scrollTop';
import { DataHojas } from 'src/app/models/hojasModel';
import {
  CausasImprocedenciaSobreseimientoAnalizadas,
  RubrosTematico,
} from 'src/app/models/sentenciaModel';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ProcesamientoTextoService } from 'src/app/services/procesamiento-texto.service';
import { SentenciaApiService } from 'src/app/services/sentencia-api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-causa-improcedencia',
  templateUrl: './causa-improcedencia.component.html',
  styleUrls: ['./causa-improcedencia.component.scss'],
  providers: [ConverStringArray],
})
export class CausaImprocedenciaComponent implements OnInit {
  isEdit: boolean = false;
  scroll: ScrollTop = new ScrollTop();
  viewDisplay: boolean = false;
  catalogos: any = {};
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  actos: string[] = [];
  rubros: string = '';
  causasImprocedencia: CausasImprocedenciaSobreseimientoAnalizadas =
    new CausasImprocedenciaSobreseimientoAnalizadas();
  votacionRubro: RubrosTematico = new RubrosTematico();
  idSentencia: string = '';
  idSentenciaVotacion: string = '';
  indexOfRubro = -1;
  indexText = -1;
  asunto: string = '';
  isView:boolean = false
  constructor(
    private catalogosService: CatalogosService,
    private param: ActivatedRoute,
    private sentenciaService: SentenciaApiService,
    private router: Router,
    private conver: ConverStringArray,
    private confirmationService: ConfirmationService,
    private toast: ToastService,
    private textProcService: ProcesamientoTextoService
  ) {
    this.scroll.scrollTo();

    this.param.paramMap.subscribe((route) => {
      this.idSentencia = route.get('id');
      this.sentenciaService
        .getSentenciaByID(this.idSentencia)
        .subscribe((data) => {
          this.textProcService.setFechaResolucion(data.fechaResolucion);
          this.asunto =
            ' ' + data.tipoAsunto + ' ' + data.numeroExpediente + ' - ';

          const reclamado: DataHojas = {
            tipoAsunto: data.tipoAsunto,
            anio: Number(data.numeroExpediente.split('/')[1]),
            consecutivo: Number(data.numeroExpediente.split('/')[0]),
          };
          this.sentenciaService.getReclamacion(reclamado).subscribe((res) => {
            let acto: string[] = [];
            acto.push(res);
            this.actos = acto;
          });
        });
      this.idSentenciaVotacion = route.get('idVotacion');
      if (route.has('idVotacion')) {
        this.getVotacion();
        this.isEdit = true;
      }

      this.param.queryParamMap.subscribe(query=>{
        if(query.has('visualizar')){
          this.isView = true
        }
      })
    });
  }

  ngOnInit(): void {
    this.catalogosService.getCatalogosSentencias().subscribe((data) => {
      data.causaImprocedencia = this.conver.proccessArray(
        data.causaImprocedencia
      );
      this.catalogos = data;
    });
  }

  openModal() {
    this.votacionRubro = new RubrosTematico();
    this.indexOfRubro = -1;
    this.rubros = '';
    this.viewDisplay = true;
  }

  agregarRubrosTematicos() {
    if (this.rubros.includes('\n')) {
      const partRubros = this.rubros.split('\n');
      partRubros.forEach((rubro) => {
        if (rubro != '') this.votacionRubro.rubrosTematicos.push(rubro);
      });
    } else {
      if (this.rubros.trim())
        this.votacionRubro.rubrosTematicos.push(this.rubros);
    }
    this.rubros = '';
  }

  guardarCausas() {
    this.causasImprocedencia.idSentencia = this.idSentencia;
    if(this.causasImprocedencia.votacion.votacionFavor.length > 0 || this.causasImprocedencia.votacion.votacionEnContra.length){
      this.sentenciaService
      .postSentenciaVotacion(this.causasImprocedencia)
      .subscribe(
        (res) => {
          if (res.id) {
            this.toast.showToast(
              'success',
              'Creación',
              'Se ha creado exitosamente la votación'
            );
            this.router
              .navigateByUrl('/', { skipLocationChange: true })
              .then((change) => {
                this.router.navigate([
                  '/sentencia',
                  this.idSentencia,
                  'causas-improcedencia',
                  res.id,
                ]);
              });
          }
        },
        (error) => {}
      );
    } else {
      this.toast.showToast(
        'error',
        'Votación requerida',
        'Al menos una votación se requiere'
      );
    }
   
  }

  crearRubro() {
    if (
      this.votacionRubro.analisisProcedencia.length >= 1 ||
      this.votacionRubro.rubrosTematicos.length >= 1
    ) {
      const data = {
        idVotacion: this.idSentenciaVotacion,
        idSentencia: this.idSentencia,
        rubro: this.votacionRubro,
      };
      this.sentenciaService.postRubroTematico(data).subscribe(
        (resp) => {
          if (resp.id) {
            this.causasImprocedencia.votacion.rubrosTematicos.push(resp);
            this.getVotacion();
            this.votacionRubro = new RubrosTematico();
            this.rubros = '';
            this.viewDisplay = false;
            this.toast.showToast(
              'success',
              'Rubro temático',
              'Ha sido creado con éxito'
            );
          }
        },
        (error) => {
          this.votacionRubro = new RubrosTematico();
          this.rubros = '';
          this.viewDisplay = false;

          this.toast.showToast(
            'error',
            'Error',
            'Ha ocurrido algo inesperado al crear el rubro temático'
          );
        }
      );
    }
  }

  actualizarRubro() {
    this.sentenciaService.putRubroTematico(this.votacionRubro).subscribe(
      (res) => {
        this.causasImprocedencia.votacion.rubrosTematicos[this.indexOfRubro] =
          res;

        this.viewDisplay = false;
        this.toast.showToast(
          'success',
          'Rubro temático',
          'Ha sido actualizado con éxito'
        );
        this.indexOfRubro = -1;
      },
      (error) => {
        this.toast.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al actualizar el rubro temático'
        );
      }
    );
  }

  getVotacion() {
    this.sentenciaService
      .getSentenciaVotacion(this.idSentenciaVotacion)
      .subscribe((res) => {
        this.causasImprocedencia.votacion = res;
      }),
      (error) => {};
  }

  actualizarCausa() {
    this.causasImprocedencia.votacion.id = this.idSentenciaVotacion;
    this.causasImprocedencia.votacion.idSentencia = this.idSentencia;

    this.sentenciaService
      .putSentenciaVotacion(this.causasImprocedencia.votacion)
      .subscribe(
        (res) => {
          if (res.id) {
            this.toast.showToast(
              'success',
              'Actualización',
              'Se ha actualizado exitosamente la votación'
            );
            this.router
              .navigateByUrl('/', { skipLocationChange: true })
              .then((change) => {
                this.router.navigate(['/sentencia', this.idSentencia]);
              });
          }
        },
        (error) => {}
      );
  }

  editRubroTematico(rubro: RubrosTematico, index: number) {
    this.rubros = '';
    this.indexOfRubro = index;
    this.votacionRubro = rubro;
    this.viewDisplay = true;
  }

  editRubroTematicoText(rubrosTem: string, i: number) {
    this.indexText = i;
    this.rubros = rubrosTem;
  }

  updateRubro() {
    this.votacionRubro.rubrosTematicos[this.indexText] = this.rubros;
    this.indexText = -1;
    this.rubros = '';
  }

  deleteRubro(event: Event, idRubro: string, index: number) {
    this.confirmationService.confirm({
      icon: 'pi pi-trash',
      message: '¿Estás seguro de eliminar permanentemente el rubro temático?',
      target: event.target,
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.sentenciaService
          .deleteRubroTematico(idRubro, this.idSentenciaVotacion,this.idSentencia)
          .subscribe(
            (res) => {
              this.toast.showToast(
                'success',
                'Rubro temático eliminado',
                'Ha sido eliminado con éxito'
              );
              this.causasImprocedencia.votacion.rubrosTematicos.splice(
                index,
                1
              );
            },
            (error) => {
              this.toast.showToast(
                'error',
                'Error',
                'Ha ocurrido algo inesperado al elimimnar el rubro temático'
              );
            }
          );
      },
    });
  }

  processText() {
    if (this.causasImprocedencia.votacion.textoVotacion) {
      let data = {
        text: this.causasImprocedencia.votacion.textoVotacion,
        fechaResolucion: '',
      };
      this.textProcService.processTex(data).subscribe(
        (resp) => {
          this.causasImprocedencia.votacion.ministros = resp.ministros;
          this.causasImprocedencia.votacion.votacionEnContra =
            resp.ministrosEnContra ? resp.ministrosEnContra : [];
          this.causasImprocedencia.votacion.votacionFavor = resp.ministrosAFavor
            ? resp.ministrosAFavor
            : [];
        },
        (error) => {
          console.log('Ha ocurrdio algo inesperado');
        }
      );
    }
  }

  markAllFavor({checked}){
    if(checked){
      this.causasImprocedencia.votacion.votacionFavor = this.causasImprocedencia.votacion.ministros
      this.causasImprocedencia.votacion.votacionEnContra = []
    } else {
      this.causasImprocedencia.votacion.votacionFavor = []
    }
  }

  markAllContra({checked}){
    if(checked){
      this.causasImprocedencia.votacion.votacionEnContra = this.causasImprocedencia.votacion.ministros
      this.causasImprocedencia.votacion.votacionFavor = []
    } else {
      this.causasImprocedencia.votacion.votacionEnContra = []
    }
  }
  
}
