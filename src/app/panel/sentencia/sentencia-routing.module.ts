import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CausaImprocedenciaComponent } from './causa-improcedencia/causa-improcedencia.component';
import { DerechosHumanosComponent } from './derechos-humanos/derechos-humanos.component';
import { SentenciaEdicionComponent } from './sentencia-edicion/sentencia-edicion.component';
import { SentenciaRegistroComponent } from './sentencia-registro/sentencia-registro.component';
import { SentenciaComponent } from './sentencia.component';
import { TipoVicioLegislativoComponent } from './tipo-vicio-legislativo/tipo-vicio-legislativo.component';
import { TipoViolacionEsferasComponent } from './tipo-violacion-esferas/tipo-violacion-esferas.component';
import { TipoViolacionPoderesComponent } from './tipo-violacion-poderes/tipo-violacion-poderes.component';

const routes: Routes = [
  { path: '', component: SentenciaComponent},
  { path: 'registro', component: SentenciaRegistroComponent},
  { path: ':id', component: SentenciaEdicionComponent},
  { path: ':id/causas-improcedencia', component: CausaImprocedenciaComponent},
  { path: ':id/causas-improcedencia/:idVotacion', component: CausaImprocedenciaComponent},
  { path: ':id/derechos-humanos', component: DerechosHumanosComponent},
  { path: ':id/derechos-humanos/:idVotacion', component: DerechosHumanosComponent},
  { path: ':id/tipo-vicio-legislativo', component: TipoVicioLegislativoComponent},
  { path: ':id/tipo-vicio-legislativo/:idVotacion', component: TipoVicioLegislativoComponent},
  { path: ':id/tipo-violacion-esferas', component: TipoViolacionEsferasComponent},
  { path: ':id/tipo-violacion-esferas/:idVotacion', component: TipoViolacionEsferasComponent},
  { path: ':id/tipo-violacion-poderes', component: TipoViolacionPoderesComponent},
  { path: ':id/tipo-violacion-poderes/:idVotacion', component: TipoViolacionPoderesComponent},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SentenciaRoutingModule { }
