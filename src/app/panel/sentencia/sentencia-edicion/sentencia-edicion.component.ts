import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfirmationService } from 'primeng/api';
import { ConverStringArray } from 'src/app/helper/converArrayObject';
import { ScrollTop } from 'src/app/helper/scrollTop';
import { Validate } from 'src/app/helper/validateExpediente';
import { DataHojas, HojasVotacion } from 'src/app/models/hojasModel';
import { RubrosTematico } from 'src/app/models/sentenciaModel';
import { SentenciaRegistro } from 'src/app/models/sentenciaRegistro';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ProcesamientoTextoService } from 'src/app/services/procesamiento-texto.service';
import { SentenciaApiService } from 'src/app/services/sentencia-api.service';
import { ToastService } from 'src/app/services/toast.service';
import { UsuariosApiService } from 'src/app/services/usuarios-api.service';
import { Usuario } from 'src/app/models/usuarioModel';

@Component({
  selector: 'app-sentencia-edicion',
  templateUrl: './sentencia-edicion.component.html',
  styleUrls: ['./sentencia-edicion.component.scss'],
  providers: [ConverStringArray],
})
export class SentenciaEdicionComponent implements OnInit, OnDestroy {
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  userData: Usuario;
  camposRestricted: string[] = [];
  hojasVotacion: HojasVotacion[] = [];
  scroll: ScrollTop = new ScrollTop();
  catalogos: any = {};
  sentenciaModel: SentenciaRegistro = new SentenciaRegistro();
  idSentencia: string = '';
  asunto: string = '';
  isChangeData: boolean = false;
  isEdit: boolean = false;
  username: string;
  usernameInitials: string;
  validCatalogos: boolean[] = [
    true,
    true,
    true,
    true,
    true,
    true,
    true,
    true,
    true,
  ];
  editInput: boolean = false;
  currentData: SentenciaRegistro;
  isView: boolean = false
  constructor(
    private router: Router,
    private catalogoService: CatalogosService,
    private param: ActivatedRoute,
    private sentenciaService: SentenciaApiService,
    private conver: ConverStringArray,
    private confirmationService: ConfirmationService,
    private textProcesService: ProcesamientoTextoService,
    private usuarioService: UsuariosApiService,
    private toast: ToastService
  ) {
    this.scroll.scrollTo();

    this.username = localStorage.getItem('username');
    this.usernameInitials = localStorage.getItem('usernameInitials');

    this.param.paramMap.subscribe((param) => {
      if (param.has('id')) {
        this.idSentencia = param.get('id');
        this.sentenciaService.getSentenciaByID(this.idSentencia).subscribe(
          (res) => {
            this.sentenciaModel = res;
            localStorage.setItem('current', JSON.stringify(res));
            this.countVotacion(res);
            this.asunto = ' ' + res.tipoAsunto + ' ' + res.numeroExpediente;
            if (this.sentenciaService.dataSentencia.id === this.idSentencia) {
              this.getDataInputs();
            }
            const data: DataHojas = {
              anio: Number(res.numeroExpediente.split('/')[1]),
              consecutivo: Number(res.numeroExpediente.split('/')[0]),
              tipoAsunto: res.tipoAsunto,
            };
            this.sentenciaService.getHojasVotacion(data).subscribe((res) => {
              this.hojasVotacion = res;
            });
          },
          (error) => { }
        );
      }

      this.param.queryParamMap.subscribe(querys => {
        if (querys.has('visualizar')) {
          this.isView = true
        }
      })
    });


  }
  ngOnInit(): void {
    this.catalogoService.getCatalogosSentencias().subscribe((data) => {
      console.log(data);
      data.derechosHumanos = this.conver.proccessArray(data.derechosHumanos);
      data.violacionOrganicaAnalizada = this.conver.proccessArray(
        data.violacionOrganicaAnalizada
      );
      data.vicioProcesoLegislativo = this.conver.proccessArray(
        data.vicioProcesoLegislativo
      );
      this.catalogos = data;
    });

    this.usuarioService.getUsuarioByUsername(this.username).subscribe((data) => {
      this.userData = data;
      if (this.userData) {
        for (let index in data.modulos) {
          if (data.modulos[index].modulo === 'sentencia') {
            if (data.modulos[index].camposRestricted.length > 0) {
              this.camposRestricted = data.modulos[index].camposRestricted;
            }
          }
        }
      }
    });

    this.observer()
  }



  observer() {
    const observer = new IntersectionObserver(function (item, observ) {
      item.forEach(element => {
        if (element.isIntersecting) {
          document.getElementById('top-update')?.classList.add('collapse')
        } else {
          document.getElementById('top-update')?.classList.remove('collapse')
        }
      })
    }, { root: null, threshold: 0, rootMargin: '100px 0px 0px 0px' })
    const buttonEnd = document.querySelector('div#end-update')
    observer.observe(buttonEnd);
  }

  public saveDate() {
    this.sentenciaService.dataSentencia = {
      id: this.idSentencia,
      model: this.sentenciaModel,
      modific: this.editInput,
    };
  }

  public redirectTo(path: string) {
    this.textProcesService.setFechaResolucion(
      this.sentenciaModel.fechaResolucion
    );
    this.saveDate();
    this.router
      .navigateByUrl('/', { skipLocationChange: true })
      .then((nav) =>
        this.router.navigate(['/sentencia/', this.idSentencia, path])
      );
  }

  public actualizarSentencia() {
    const validate = Validate(this.sentenciaModel.numeroExpediente);
    if (validate) {
      delete this.sentenciaModel
        .votacionCausasImprocedenciaySobreseimientoAnalizadas;
      delete this.sentenciaModel
        .votacionDerechosHumanosCuyaViolacionAnalizaSentencia;
      delete this.sentenciaModel
        .votacionTipoVicioProcesoLegislativoAnalizadoenSentencia;
      delete this.sentenciaModel
        .votacionTipoViolacionInvacionEsferasAnalizadoenSentencia;
      delete this.sentenciaModel
        .votacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
      this.sentenciaService.putSentencia(this.sentenciaModel).subscribe(
        (resp) => {
          this.toast.showToast(
            'success',
            'Actualizado',
            'Se ha actualizado exitosamente'
          );
          this.sentenciaService.dataSentencia = {
            id: '',
            model: {} as SentenciaRegistro,
            modific: this.editInput,
          };
          this.router
            .navigateByUrl('/', { skipLocationChange: true })
            .then((succees) => {
              this.router.navigate(['/sentencia']);
            });
        },
        (error) => { }
      );
    } else {
    }
  }

  ngOnDestroy(): void { }

  public currentValues(causas: RubrosTematico[], key: string): string[] {
    let data: string[] = [];
    causas?.forEach((rubros) => {
      rubros[key].forEach((procedencia) => {
        if (!data.includes(procedencia)) {
          data.push(procedencia);
        }
      });
    });
    return data;
  }

  public downloadPDF(hoja: HojasVotacion) {
    window.open(
      `http://172.16.214.22:8080/hoja-votacion/v1/hoja-votacion/documento?documentoId=${hoja.documentoID}
      &asuntoId=${hoja.asuntoID}&origen=${hoja.origen}&name=${hoja.documentoID}`
    );
  }

  public deleteVotacion(event: Event, votacion: any, tipoVotacion: string) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar permanentemente esta votación?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.sentenciaService
          .deleteSentenciaVotacion(votacion.id, this.idSentencia)
          .subscribe(
            (data) => {
              const key = Object.keys(data);
              this.sentenciaModel[key[0]] = data[key[0]];

              this.removeVotacion(votacion, tipoVotacion);
              this.toast.showToast(
                'success',
                'Eliminado',
                'Se ha eliminado con éxito la votación'
              );
            },
            (error) => {
              this.toast.showToast(
                'error',
                'Error',
                'Ha ocurrido algo inesperado al eliminar la votación'
              );
            }
          );
      },
      reject: () => { },
    });
  }

  private removeVotacion(votacion: any, tipoVotacion: string) {
    let indexOfVotacion: number = -1;
    switch (tipoVotacion) {
      case 'causasImprocedencia':
        indexOfVotacion =
          this.sentenciaModel.votacionCausasImprocedenciaySobreseimientoAnalizadas.indexOf(
            votacion
          );
        this.sentenciaModel.votacionCausasImprocedenciaySobreseimientoAnalizadas.splice(
          indexOfVotacion,
          1
        );
        break;
      case 'derechosHumanos':
        indexOfVotacion =
          this.sentenciaModel.votacionDerechosHumanosCuyaViolacionAnalizaSentencia.indexOf(
            votacion
          );
        this.sentenciaModel.votacionDerechosHumanosCuyaViolacionAnalizaSentencia.splice(
          indexOfVotacion,
          1
        );
        break;
      case 'procesoLegislativo':
        indexOfVotacion =
          this.sentenciaModel.votacionTipoVicioProcesoLegislativoAnalizadoenSentencia.indexOf(
            votacion
          );
        this.sentenciaModel.votacionTipoVicioProcesoLegislativoAnalizadoenSentencia.splice(
          indexOfVotacion,
          1
        );
        break;
      case 'violacionEsferas':
        indexOfVotacion =
          this.sentenciaModel.votacionTipoViolacionInvacionEsferasAnalizadoenSentencia.indexOf(
            votacion
          );
        this.sentenciaModel.votacionTipoViolacionInvacionEsferasAnalizadoenSentencia.splice(
          indexOfVotacion,
          1
        );
        break;
      case 'violacionPoderes':
        indexOfVotacion =
          this.sentenciaModel.votacionTipoViolacionInvacionPoderesAnalizadoenSentencia.indexOf(
            votacion
          );
        this.sentenciaModel.votacionTipoViolacionInvacionPoderesAnalizadoenSentencia.splice(
          indexOfVotacion,
          1
        );
        break;
    }
  }

  public validateCatalogo(campo: string) {
    let index: number;
    switch (campo) {
      case 'promovente':
        if (this.sentenciaModel.tipoPersonaPromovente) {
          index = this.catalogos.personaPromovente.findIndex(
            (p) => p.nombre == this.sentenciaModel.tipoPersonaPromovente
          );
          this.validCatalogos[0] = index != -1 ? true : false;
        } else {
          this.validCatalogos[0] = true;
        }
        break;
      case 'colectiva':
        if (this.sentenciaModel.tipoPersonaJuridicoColectiva) {
          index = this.catalogos.personaJuridicoColectiva.findIndex(
            (p) => p.nombre == this.sentenciaModel.tipoPersonaJuridicoColectiva
          );
          this.validCatalogos[1] = index != -1 ? true : false;
        } else {
          this.validCatalogos[1] = true;
        }
        break;
      case 'demanda':
        if (this.sentenciaModel.tipoViolacionPlanteadaenDemanda) {
          index = this.catalogos.violacionPlanteadaDemanda.findIndex(
            (p) =>
              p.nombre == this.sentenciaModel.tipoViolacionPlanteadaenDemanda
          );
          this.validCatalogos[2] = index != -1 ? true : false;
        } else {
          this.validCatalogos[2] = true;
        }
        break;
      case 'esferas':
        if (this.sentenciaModel.tipoViolacionInvacionEsferas) {
          index = this.catalogos.violacionInvacionEsferas.findIndex(
            (p) => p.nombre == this.sentenciaModel.tipoViolacionInvacionEsferas
          );
          this.validCatalogos[3] = index != -1 ? true : false;
        } else {
          this.validCatalogos[3] = true;
        }
        break;
      case 'poderes':
        if (this.sentenciaModel.tipoViolacionInvacionPoderes) {
          index = this.catalogos.violacionInvacionPoderes.findIndex(
            (p) => p.nombre == this.sentenciaModel.tipoViolacionInvacionPoderes
          );
          this.validCatalogos[4] = index != -1 ? true : false;
        } else {
          this.validCatalogos[4] = true;
        }
        break;
      case 'invasion':
        if (
          this.sentenciaModel
            .tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia
        ) {
          index = this.catalogos.violacionInvacionEsferasAnalizada.findIndex(
            (p) =>
              p.nombre ==
              this.sentenciaModel
                .tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia
          );
          this.validCatalogos[5] = index != -1 ? true : false;
        } else {
          this.validCatalogos[5] = true;
        }
        break;
      case 'analizada':
        if (
          this.sentenciaModel.tipoViolacionInvacionPoderesAnalizadoenSentencia
        ) {
          index = this.catalogos.violacionInvacionPoderesAnalizado.findIndex(
            (p) =>
              p.nombre ==
              this.sentenciaModel
                .tipoViolacionInvacionPoderesAnalizadoenSentencia
          );
          this.validCatalogos[6] = index != -1 ? true : false;
        } else {
          this.validCatalogos[6] = true;
        }
        break;
      case 'efectos':
        if (this.sentenciaModel.momentoSurteEfectoSentencia) {
          index = this.catalogos.declaracionInvalidez.findIndex(
            (p) => p.nombre == this.sentenciaModel.momentoSurteEfectoSentencia
          );
          this.validCatalogos[7] = index != -1 ? true : false;
        } else {
          this.validCatalogos[7] = true;
        }
        break;
      case 'tramite':
        if (this.sentenciaModel.tramiteEngrose) {
          index = this.catalogos.tramiteEngrose.findIndex(
            (p) => p.nombre == this.sentenciaModel.tramiteEngrose
          );
          this.validCatalogos[8] = index != -1 ? true : false;
        } else {
          this.validCatalogos[8] = true;
        }
        break;
      case 'materia-esferas':
        if (this.sentenciaModel.materiaAnalizadaInvasionEsferas) {
          index = this.catalogos.violacionInvacionEsferas.findIndex(
            (p) => p.nombre == this.sentenciaModel.materiaAnalizadaInvasionEsferas
          );
          this.validCatalogos[9] = index != -1 ? true : false;
        } else {
          this.validCatalogos[9] = true;
        }
        break;
      case 'materia-poderes':
        if (this.sentenciaModel.materiaAnalizadaInvasionPoderes) {
          index = this.catalogos.violacionInvacionEsferas.findIndex(
            (p) => p.nombre == this.sentenciaModel.materiaAnalizadaInvasionPoderes
          );
          this.validCatalogos[10] = index != -1 ? true : false;
        } else {
          this.validCatalogos[10] = true;
        }
        break;
    }
  }

  private countVotacion(data: SentenciaRegistro) {
    const total =
      data.totalVotacionCausasImprocedenciaySobreseimientoAnalizadas +
      data.totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia +
      data.totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia +
      data.totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia +
      data.totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia;
    this.sentenciaModel.numeroVotacionesRegistradas = total;
  }

  private getDataInputs() {
    const data = this.sentenciaService.dataSentencia.model;
    this.sentenciaModel.tipoPersonaPromovente = data.tipoPersonaPromovente;
    this.sentenciaModel.tipoPersonaJuridicoColectiva =
      data.tipoPersonaJuridicoColectiva;
    this.sentenciaModel.tipoViolacionPlanteadaenDemanda =
      data.tipoViolacionPlanteadaenDemanda;
    this.sentenciaModel.derechosHumanosCuyaViolacionPlantea =
      data.derechosHumanosCuyaViolacionPlantea;
    this.sentenciaModel.violacionOrganicaPlanteada =
      data.violacionOrganicaPlanteada;
    this.sentenciaModel.tipoViciosProcesoLegislativo =
      data.tipoViciosProcesoLegislativo;
    this.sentenciaModel.tipoViolacionInvacionEsferas =
      data.tipoViolacionInvacionEsferas;
    this.sentenciaModel.tipoViolacionInvacionPoderes =
      data.tipoViolacionInvacionPoderes;
    this.sentenciaModel.derechosHumanosCuyaViolacionPlanteaByDerechosHumanos =
      data.derechosHumanosCuyaViolacionPlanteaByDerechosHumanos;
    this.sentenciaModel.violacionOrganicaAnalizadaenSentencia =
      data.violacionOrganicaAnalizadaenSentencia;
    this.sentenciaModel.tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia =
      data.tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia;
    this.sentenciaModel.tipoViolacionInvacionPoderesAnalizadoenSentencia =
      data.tipoViolacionInvacionPoderesAnalizadoenSentencia;
    this.sentenciaModel.momentoSurteEfectoSentencia =
      data.momentoSurteEfectoSentencia;
    this.sentenciaModel.tramiteEngrose = data.tramiteEngrose;
    this.sentenciaModel.diasTranscurridosEntreDictadoSentenciayFirmaEngrose =
      data.diasTranscurridosEntreDictadoSentenciayFirmaEngrose;
    this.sentenciaModel.fechaSentencia = data.fechaSentencia;
    this.sentenciaModel.fechaFirmaEngrose = data.fechaFirmaEngrose;
    this.editInput = this.sentenciaService.dataSentencia.modific;
    this.sentenciaModel.numeroVotacionesRealizadas = data.numeroVotacionesRealizadas
    this.sentenciaModel.numeroVotacionesRegistradas = data.numeroVotacionesRegistradas
  }

  activeAlert() {
    this.editInput = true;


  }

  detectDias() {


    if ((this.sentenciaModel.fechaSentencia != undefined && this.sentenciaModel.fechaSentencia != '')
      && (this.sentenciaModel.fechaFirmaEngrose != undefined && this.sentenciaModel.fechaFirmaEngrose != '')) {
     /*  console.log(this.sentenciaModel.fechaSentencia);
      console.log(this.sentenciaModel.fechaFirmaEngrose); */
      let inicio = this.sentenciaModel.fechaSentencia.split('-')[2] + "-" + this.sentenciaModel.fechaSentencia.split('-')[1] + "-" + this.sentenciaModel.fechaSentencia.split('-')[0];
      let fin = this.sentenciaModel.fechaFirmaEngrose.split('-')[2] + "-" + this.sentenciaModel.fechaFirmaEngrose.split('-')[1] + "-" + this.sentenciaModel.fechaFirmaEngrose.split('-')[0];

      let fechaInicio = new Date(inicio).getTime();
      let fechaFin = new Date(fin).getTime();
      let diff = fechaFin - fechaInicio;
      console.log(diff / (1000 * 60 * 60 * 24));
      let dias = diff / (1000 * 60 * 60 * 24);

      dias > 0 ? this.sentenciaModel.diasTranscurridosEntreDictadoSentenciayFirmaEngrose = String(dias) : this.sentenciaModel.diasTranscurridosEntreDictadoSentenciayFirmaEngrose = "0";
    }
    else {
      this.sentenciaModel.diasTranscurridosEntreDictadoSentenciayFirmaEngrose = "0";
    }

  }

  resetDate(option) {
    option == 1 ? this.sentenciaModel.fechaSentencia = '' : this.sentenciaModel.fechaFirmaEngrose = '';
    this.detectDias();
  }


  discardChanges() {
    const data = JSON.parse(
      window.localStorage.getItem('current')
    ) as SentenciaRegistro;
    this.sentenciaModel.tipoPersonaPromovente = data.tipoPersonaPromovente;
    this.sentenciaModel.tipoPersonaJuridicoColectiva =
      data.tipoPersonaJuridicoColectiva;
    this.sentenciaModel.tipoViolacionPlanteadaenDemanda =
      data.tipoViolacionPlanteadaenDemanda;
    this.sentenciaModel.derechosHumanosCuyaViolacionPlantea =
      data.derechosHumanosCuyaViolacionPlantea;
    this.sentenciaModel.violacionOrganicaPlanteada =
      data.violacionOrganicaPlanteada;
    this.sentenciaModel.tipoViciosProcesoLegislativo =
      data.tipoViciosProcesoLegislativo;
    this.sentenciaModel.tipoViolacionInvacionEsferas =
      data.tipoViolacionInvacionEsferas;
    this.sentenciaModel.tipoViolacionInvacionPoderes =
      data.tipoViolacionInvacionPoderes;
    this.sentenciaModel.derechosHumanosCuyaViolacionPlanteaByDerechosHumanos =
      data.derechosHumanosCuyaViolacionPlanteaByDerechosHumanos;
    this.sentenciaModel.violacionOrganicaAnalizadaenSentencia =
      data.violacionOrganicaAnalizadaenSentencia;
    this.sentenciaModel.tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia =
      data.tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia;
    this.sentenciaModel.tipoViolacionInvacionPoderesAnalizadoenSentencia =
      data.tipoViolacionInvacionPoderesAnalizadoenSentencia;
    this.sentenciaModel.momentoSurteEfectoSentencia =
      data.momentoSurteEfectoSentencia;
    this.sentenciaModel.tramiteEngrose = data.tramiteEngrose;
    this.sentenciaModel.diasTranscurridosEntreDictadoSentenciayFirmaEngrose =
      data.diasTranscurridosEntreDictadoSentenciayFirmaEngrose;
    this.sentenciaModel.fechaSentencia = data.fechaSentencia;
    this.sentenciaModel.fechaFirmaEngrose = data.fechaFirmaEngrose;
    this.sentenciaModel.numeroVotacionesRealizadas = data.numeroVotacionesRealizadas
    this.sentenciaModel.numeroVotacionesRegistradas = data.numeroVotacionesRegistradas
    this.editInput = false;
  }


  public cleanData(type: string, { value }) {
    switch (type) {
      case 'violacion':
        switch (value) {
          case 'SÓLO VIOLACIONES ORGÁNICAS':
            this.sentenciaModel.derechosHumanosCuyaViolacionPlantea = [];
            break;
          case 'SÓLO VIOLACIONES A DERECHOS HUMANOS':
            this.sentenciaModel.violacionOrganicaPlanteada = [];
            this.sentenciaModel.tipoViciosProcesoLegislativo = [];
            this.sentenciaModel.tipoViolacionInvacionEsferas = null;
            this.sentenciaModel.tipoViolacionInvacionPoderes = null;
            this.sentenciaModel.violacionOrganicaAnalizadaenSentencia = [];
            break;
          case 'NINGUNO':
            this.sentenciaModel.violacionOrganicaPlanteada = [];
            this.sentenciaModel.tipoViciosProcesoLegislativo = [];
            this.sentenciaModel.tipoViolacionInvacionEsferas = null;
            this.sentenciaModel.tipoViolacionInvacionPoderes = null;
            this.sentenciaModel.derechosHumanosCuyaViolacionPlantea = [];
            this.sentenciaModel.violacionOrganicaAnalizadaenSentencia = [];
            break;
        }
        break;
      case 'promovente':
        if (!(value == 'JURÍDICO COLECTIVA OFICIAL' || value == 'JURÍDICO COLECTIVA PRIVADA'))
          this.sentenciaModel.tipoPersonaJuridicoColectiva = null
        break;
    }
  }

}
