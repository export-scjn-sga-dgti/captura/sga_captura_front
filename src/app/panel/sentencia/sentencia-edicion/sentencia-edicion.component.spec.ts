import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SentenciaEdicionComponent } from './sentencia-edicion.component';

describe('SentenciaEdicionComponent', () => {
  let component: SentenciaEdicionComponent;
  let fixture: ComponentFixture<SentenciaEdicionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SentenciaEdicionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SentenciaEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
