import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfirmationService } from 'primeng/api';
import { ConverStringArray } from 'src/app/helper/converArrayObject';
import { ScrollTop } from 'src/app/helper/scrollTop';
import { DataHojas } from 'src/app/models/hojasModel';
import {
  RubrosEfectoSentencia,
  VotacionTipoViolacionInvacionEsferasAnalizadoenSentencia,
} from 'src/app/models/sentenciaRegistro';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ProcesamientoTextoService } from 'src/app/services/procesamiento-texto.service';
import { SentenciaApiService } from 'src/app/services/sentencia-api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-tipo-violacion-esferas',
  templateUrl: './tipo-violacion-esferas.component.html',
  styleUrls: ['./tipo-violacion-esferas.component.scss'],
  providers: [ConverStringArray],
})
export class TipoViolacionEsferasComponent implements OnInit {
  scroll: ScrollTop = new ScrollTop();
  configRubros: any;
  catalogos: any = {};
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  violacionEsferas: VotacionTipoViolacionInvacionEsferasAnalizadoenSentencia =
    new VotacionTipoViolacionInvacionEsferasAnalizadoenSentencia();
  rubro: string = '';
  indexOfRubro: number = -1;
  idSentencia: string = '';
  idSentenciaVotacion: string = '';
  isEdit: boolean = false;
  rubrosEdit: any;
  indexOfRubrosSentencia: number = -1;
  asunto: string = '';
  actos: string[] = [];
  isView: boolean = false
  fecha: string
  constructor(
    private catalogosService: CatalogosService,
    private param: ActivatedRoute,
    private route: Router,
    private sentenciaService: SentenciaApiService,
    private conver: ConverStringArray,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private textProcService: ProcesamientoTextoService
  ) {
    this.scroll.scrollTo();

    this.param.paramMap.subscribe((param) => {
      this.idSentencia = param.get('id');
      this.sentenciaService
        .getSentenciaByID(this.idSentencia)
        .subscribe((data) => {
          this.textProcService.setFechaResolucion(data.fechaResolucion)
          this.asunto =
            ' ' + data.tipoAsunto + ' ' + data.numeroExpediente + ' - ';

          const reclamado: DataHojas = {
            tipoAsunto: data.tipoAsunto,
            anio: Number(data.numeroExpediente.split('/')[1]),
            consecutivo: Number(data.numeroExpediente.split('/')[0]),
          };
          this.sentenciaService.getReclamacion(reclamado).subscribe((res) => {
            let acto: string[] = [];
            acto.push(res);
            this.actos = acto;
          });
        });
      if (param.has('idVotacion')) {
        this.isEdit = true;
        this.idSentenciaVotacion = param.get('idVotacion');
        this.getVotacion();
      }
      
      this.param.queryParamMap.subscribe(query=>{
        if(query.has('visualizar')){
          this.isView = true
        }
      })
    });
  }

  ngOnInit(): void {
    this.catalogosService.getCatalogosSentencias().subscribe((data) => {
      data.tipoSentencia = this.conver.proccessArray(data.tipoSentencia);
      this.catalogos = data;
    });
  }

  displayRubros(rubroRelacion: string) {
    const rubro = {
      tipo: rubroRelacion,
      open: true,
      edicion : false
    };
    this.configRubros = rubro;
  }
  getModal(response: {
    tipo: string;
    open: boolean;
    rubro: RubrosEfectoSentencia;
  }) {
    if(this.violacionEsferas.sentidoResolucion) response.rubro.sentido = this.violacionEsferas.sentidoResolucion;
    if(this.violacionEsferas.tipoSentencia) response.rubro.tipoSentencia = this.violacionEsferas.tipoSentencia;
    let data = {
      idVotacion: this.idSentenciaVotacion,
      idSentencia: this.idSentencia,
      rubro: response.rubro,
    };
    this.sentenciaService.postRubroTematico(data).subscribe(
      (res) => {
        if (response.tipo == 'efectos') {
          this.violacionEsferas.rubrosEfectoSentencia.push(res);
        } else {
          this.violacionEsferas.rubrosExtencionInvalidez.push(res);
        }
        this.toast.showToast(
          'success',
          'Rubro temático',
          'Ha sido creado con éxito'
        );
      },
      (error) => {
        this.toast.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al crear el rubro temático'
        );
      }
    );
  }

  crearVotacion() {
    const data = {
      idSentencia: this.idSentencia,
      votacion: this.violacionEsferas,
    };

    if (this.violacionEsferas.votacionFavor.length > 0 || this.violacionEsferas.votacionEnContra.length > 0) {
      this.sentenciaService.postSentenciaVotacion(data).subscribe((res) => {
        this.toast.showToast(
          'success',
          'Creación',
          'Se ha creado exitosamente la votación'
        );
        this.route
          .navigateByUrl('/', { skipLocationChange: true })
          .then((success) => {
            this.route.navigate([
              'sentencia',
              this.idSentencia,
              'tipo-violacion-esferas',
              res.id,
            ]);
          });
      });

    } else {
      this.toast.showToast(
        'error',
        'Votación requerida',
        'Al menos una votación se requiere'
      );
    }
  }

  updateVotacion() {
    this.violacionEsferas.idSentencia = this.idSentencia;

    if (this.violacionEsferas.sentidoResolucion != 'FUNDADA' && this.violacionEsferas.rubrosEfectoSentencia.length > 0) return this.validateVotacioneEfectos()
    if (!this.violacionEsferas.tipoSentencia.includes('EXTENSIVA A NORMAS DIVERSAS') && this.violacionEsferas.rubrosExtencionInvalidez.length > 0) return this.validateVotacionesInvalidez()

    this.sentenciaService.putSentenciaVotacion(this.violacionEsferas).subscribe(
      (res) => {
        this.toast.showToast(
          'success',
          'Actualización',
          'Se ha actualizado exitosamente la votación'
        );
        this.route
          .navigateByUrl('/', { skipLocationChange: true })
          .then((success) => {
            this.route.navigate(['sentencia', this.idSentencia]);
          });
      },
      (error) => {
        this.toast.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al actualizar la votación'
        );
       }
    );
  }

  
  validateVotacioneEfectos() {
    this.toast.showToast(
      'error',
      'Actualización no permitida',
      `Contiene votaciones relacionadas con efectos de la sentencia en campo sentido de la resolución.
       Corrija o elimine votaciones`
    );
  }

  validateVotacionesInvalidez() {
    this.toast.showToast(
      'error',
      'Actualización no permitida',
      `Contiene votaciones relacionadas con la extensión de invalidez en campo tipo de sentencia.
      Corrija o elimine votaciones`
    );
  }

  agregarRubroPrincipal() {
    if (this.rubro.includes('\n')) {
      const linesRubros = this.rubro.split('\n');
      linesRubros.forEach((text) => {
        if (text != '')
          this.violacionEsferas.rubrosTematicosPrincipal.push(text);
      });
    } else {
      if (this.rubro.trim())
        this.violacionEsferas.rubrosTematicosPrincipal.push(this.rubro);
    }
    const data = this.violacionEsferas.rubrosTematicosPrincipal
    this.violacionEsferas.rubrosTematicosPrincipal = []
    setTimeout(()=>{
      this.violacionEsferas.rubrosTematicosPrincipal = data
    },300)

    if(this.isEdit && !this.isView){
      this.sentenciaService.updateOrderRubro({id: this.idSentenciaVotacion,rubrosTematicosPrincipal: data}).subscribe(data =>{
        if(data){
          this.toast.showToast('success','Creación','Rubros temáticos se ha creado con éxito')
        }
      })
    }
    this.rubro = '';
  }

  deleteRubroPrincipal(event: Event, index: number) {
    this.confirmationService.confirm({
      icon: 'pi pi-trash',
      message: '¿Estás seguro de eliminar permanentemente el rubro temático?',
      target: event.target,
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.violacionEsferas.rubrosTematicosPrincipal.splice(index, 1);
        const actual = this.violacionEsferas.rubrosTematicosPrincipal;
        this.sentenciaService.updateOrderRubro({
          id: this.idSentenciaVotacion, rubrosTematicosPrincipal:
            this.violacionEsferas.rubrosTematicosPrincipal
        }).subscribe(
          data => {
            if (data) {
              this.violacionEsferas.rubrosTematicosPrincipal = [];
              setTimeout(() => {
                this.violacionEsferas.rubrosTematicosPrincipal = actual;
              }, 300);
              this.toast.showToast(
                'success',
                'Rubro temático eliminado',
                'Ha sido eliminado con éxito'
              );
            }
          }
        )
      },
    });
  }


  orderRubro(){
    if(this.isEdit && !this.isView){
      this.sentenciaService.updateOrderRubro({id: this.idSentenciaVotacion,rubrosTematicosPrincipal: this.violacionEsferas.rubrosTematicosPrincipal}).subscribe(data =>{
        if(data){
          this.toast.showToast('success','Actualización','Rubros temáticos se ha actualizado con éxito')
        }
      })
    }
  }

  actualizarRubro() {
    this.violacionEsferas.rubrosTematicosPrincipal[this.indexOfRubro] =
      this.rubro;
    this.rubro = '';
    const data = this.violacionEsferas.rubrosTematicosPrincipal
    this.violacionEsferas.rubrosTematicosPrincipal = []
    setTimeout(()=>{
      this.violacionEsferas.rubrosTematicosPrincipal = data
    },300)
    if(this.isEdit && !this.isView){
      this.sentenciaService.updateOrderRubro({id: this.idSentenciaVotacion,rubrosTematicosPrincipal: data}).subscribe(data =>{
        if(data){
          this.toast.showToast('success','Actualización','Rubros temáticos se ha actualizado con éxito')
        }
      })
    }
    this.indexOfRubro = -1;
  }

  editRubroTematico(rubro: string, i: number) {
    this.indexOfRubro = i;
    this.rubro = rubro;
  }

  getVotacion() {
    this.sentenciaService
      .getSentenciaVotacion(this.idSentenciaVotacion)
      .subscribe(
        (data) => {
          this.violacionEsferas = data;
        },
        (error) => { }
      );
  }
  edicionRubro(sentencia: RubrosEfectoSentencia, tipoRubro: string) {
    if (tipoRubro === 'efectos') {
      this.indexOfRubrosSentencia =
        this.violacionEsferas.rubrosEfectoSentencia.indexOf(sentencia);
    } else {
      this.indexOfRubrosSentencia =
        this.violacionEsferas.rubrosExtencionInvalidez.indexOf(sentencia);
    }
    this.rubrosEdit = {
      rubros: sentencia,
      tipo: tipoRubro,
      open: true,
      edicion : true
    };
  }

  deleteRubro(event: Event, rubro: RubrosEfectoSentencia, tipoRubro: string) {
    this.confirmationService.confirm({
      icon: 'pi pi-trash',
      message: '¿Estás seguro de eliminar permanentemente el rubro temático?',
      target: event.target,
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.sentenciaService
          .deleteRubroTematico(rubro.id, this.idSentenciaVotacion,this.idSentencia)
          .subscribe(
            (res) => {
              this.toast.showToast(
                'success',
                'Rubro temático eliminado',
                'Ha sido eliminado con éxito'
              );
              let indexOfRubro = -1;
              if (tipoRubro === 'efectos') {
                indexOfRubro =
                  this.violacionEsferas.rubrosEfectoSentencia.indexOf(rubro);
                this.violacionEsferas.rubrosEfectoSentencia.splice(
                  indexOfRubro,
                  1
                );
              } else {
                indexOfRubro =
                  this.violacionEsferas.rubrosExtencionInvalidez.indexOf(rubro);
                this.violacionEsferas.rubrosExtencionInvalidez.splice(
                  indexOfRubro,
                  1
                );
              }
            },
            (error) => { }
          );
      },
    });
  }

  getModalUpdateRubro(response: {
    tipo: string;
    open: boolean;
    rubro: RubrosEfectoSentencia;
  }) {
    if(this.violacionEsferas.sentidoResolucion) response.rubro.sentido = this.violacionEsferas.sentidoResolucion;
    if(this.violacionEsferas.tipoSentencia) response.rubro.tipoSentencia = this.violacionEsferas.tipoSentencia;
    let data = {
      idVotacion: this.idSentenciaVotacion,
      idSentencia: this.idSentencia,
      rubro: response.rubro,
    };
    this.sentenciaService.putRubroTematico(data).subscribe(
      (res) => {
        if (response.tipo == 'efectos') {
          this.violacionEsferas.rubrosEfectoSentencia[
            this.indexOfRubrosSentencia
          ] = res;
        } else {
          this.violacionEsferas.rubrosExtencionInvalidez[
            this.indexOfRubrosSentencia
          ] = res;
        }
        this.toast.showToast(
          'success',
          'Rubro temático',
          'Ha sido actualizado con éxito'
        );
        this.indexOfRubrosSentencia = -1;
      },
      (error) => {
        this.toast.showToast(
          'error',
          'Error',
          'Ha ocurrido algo inesperado al actualizar el rubro temático'
        );
      }
    );
  }

  processText() {
    if (this.violacionEsferas.textoVotacion) {
      let data = {
        text: this.violacionEsferas.textoVotacion,
        fechaResolucion: '',
      };
      this.textProcService.processTex(data).subscribe(
        (resp) => {
          this.violacionEsferas.ministros = resp.ministros;
          this.violacionEsferas.votacionEnContra = resp.ministrosEnContra
            ? resp.ministrosEnContra
            : [];
          this.violacionEsferas.votacionFavor = resp.ministrosAFavor
            ? resp.ministrosAFavor
            : [];
        },
        (error) => {
          console.log('Ha ocurrdio algo inesperado');
        }
      );
    }
  }

  markAllFavor({checked}){
    if(checked){
      this.violacionEsferas.votacionFavor = this.violacionEsferas.ministros
      this.violacionEsferas.votacionEnContra = []
    } else {
      this.violacionEsferas.votacionFavor = []
    }
  }

  markAllContra({checked}){
    if(checked){
      this.violacionEsferas.votacionEnContra = this.violacionEsferas.ministros
      this.violacionEsferas.votacionFavor = []
    } else {
      this.violacionEsferas.votacionEnContra = []
    }
  }
}
