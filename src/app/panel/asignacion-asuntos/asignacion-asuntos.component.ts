import { Component, OnInit } from '@angular/core';
import { Content, DataAsuntos } from 'src/app/models/asignacion';
import { Modulos } from 'src/app/models/usuarioModel';
import { UsuariosApiService } from 'src/app/services/usuarios-api.service';

@Component({
  selector: 'app-asignacion-asuntos',
  templateUrl: './asignacion-asuntos.component.html',
  styleUrls: ['./asignacion-asuntos.component.scss']
})
export class AsignacionAsuntosComponent implements OnInit {

  constructor(private users: UsuariosApiService) { }

  data: Content[] = []
  asuntos: DataAsuntos = new DataAsuntos
  ngOnInit(): void {
    this.users.getAllUsuarios().subscribe(data => {
      this.data = data['content']
    })
  }

  viewAsuntos(data: Modulos[]) {

    data.forEach(x => {
      if (x.modulo == 'taquigrafica' || x.modulo == 'sentencia') {
        if (x.modulo == 'taquigrafica') {
          this.asuntos.taqui = []
          x.asuntos.forEach(data => {
            if (data.tipoAsunto != null && data.numExpediente) {
              this.asuntos.taqui.push(data.tipoAsunto + ' ' + data.numExpediente)
            }
          })
        }
        if (x.modulo == 'sentencia') {
          this.asuntos.sentencia = []
          x.asuntos.forEach(data => {
            if (data.tipoAsunto != null && data.numExpediente) {
              this.asuntos.sentencia.push(data.tipoAsunto + ' ' + data.numExpediente)
            }

          })
        }
      }
    })
    this.close()
  }

  close() {
    document.getElementById('asig').classList.toggle('fadein')
  }

}
