import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsignacionAsuntosComponent } from './asignacion-asuntos.component';
import { EdicionAsuntosComponent } from './edicion-asuntos/edicion-asuntos.component';

const routes: Routes = [
  { path: '', component: AsignacionAsuntosComponent },
  { path: 'edicion/:id', component: EdicionAsuntosComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsignacionAsuntosRoutingModule {}
