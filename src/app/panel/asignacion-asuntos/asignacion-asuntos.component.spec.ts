import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionAsuntosComponent } from './asignacion-asuntos.component';

describe('AsignacionAsuntosComponent', () => {
  let component: AsignacionAsuntosComponent;
  let fixture: ComponentFixture<AsignacionAsuntosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignacionAsuntosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionAsuntosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
