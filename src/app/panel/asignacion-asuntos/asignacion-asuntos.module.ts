import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsignacionAsuntosRoutingModule } from './asignacion-asuntos-routing.module';
import { EdicionAsuntosComponent } from './edicion-asuntos/edicion-asuntos.component';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';


@NgModule({
  declarations: [EdicionAsuntosComponent],
  imports: [
    CommonModule,
    AsignacionAsuntosRoutingModule,
    BreadcrumbModule,
    ReactiveFormsModule,
    FormsModule,
    ButtonModule,
    DropdownModule
  ]
})
export class AsignacionAsuntosModule { }
