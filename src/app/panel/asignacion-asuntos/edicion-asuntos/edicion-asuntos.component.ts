import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Asunto } from 'src/app/models/asignacion';
import { Usuario } from 'src/app/models/usuarioModel';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ToastService } from 'src/app/services/toast.service';
import { UsuariosApiService } from 'src/app/services/usuarios-api.service';

@Component({
  selector: 'app-edicion-asuntos',
  templateUrl: './edicion-asuntos.component.html',
  styleUrls: ['./edicion-asuntos.component.scss']
})
export class EdicionAsuntosComponent implements OnInit {


  idUser: string;
  userInfo: Usuario = new Usuario();
  sentenciaAsuntos: Asunto[] = [];
  taquigraficasAsuntos: Asunto[] = [];
  catalogos: any;
  newAsunto: Asunto = {} as Asunto
  type: string

  constructor(private param: ActivatedRoute,
    private user: UsuariosApiService,
    private router: Router,
    private toastService: ToastService,
    private catalogo: CatalogosService) {
    this.catalogo.getCatalogosSentencias().subscribe(data => {
      this.catalogos = data
    })
  }

  ngOnInit(): void {
    this.param.paramMap.subscribe(key => {
      this.idUser = key.get('id')
      this.user.getUsuarioByID(this.idUser).subscribe(
        data => {
          this.userInfo = data
          data.modulos.forEach(m => {
            this.getData(m.modulo, m.asuntos);
          })
        }
      )
    })
  }


  private getData(type: string, asuntos: any[]) {
    if (type == 'sentencia') {
      this.sentenciaAsuntos = asuntos
    } else if (type == 'taquigrafica') {
      this.taquigraficasAsuntos = asuntos
    }
  }

  addAsunto() {
    switch (this.type) {
      case 'Sentencias':
        this.sentenciaAsuntos.push(this.newAsunto)
        break;
      case 'Versiones Taquigráficas':
        this.taquigraficasAsuntos.push(this.newAsunto)
        break;
      default:
        break;
    }

    this.newAsunto = {} as Asunto
    this.type = null
  }

  updateUser(){
    this.userInfo.modulos.forEach(m => {
      switch (m.modulo) {
        case 'sentencia':
          m.asuntos = this.sentenciaAsuntos
          break;
        case 'taquigrafica':
          m.asuntos = this.taquigraficasAsuntos
          break;
        default:
          break;
      }
    })
    this.user.updateUsuario(this.userInfo).subscribe(data =>{
      if(data.id){
        this.router.navigate(['asignacion-asuntos']);
        this.toastService.showToast('success','Actualización','Se ha actualizado la asignación de asuntos con éxito')
      }
    })
  }

}
