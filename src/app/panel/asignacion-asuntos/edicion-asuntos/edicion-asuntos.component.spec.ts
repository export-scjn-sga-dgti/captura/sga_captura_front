import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdicionAsuntosComponent } from './edicion-asuntos.component';

describe('EdicionAsuntosComponent', () => {
  let component: EdicionAsuntosComponent;
  let fixture: ComponentFixture<EdicionAsuntosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdicionAsuntosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdicionAsuntosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
