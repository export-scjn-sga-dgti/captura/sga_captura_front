import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanelComponent } from './panel.component';

const routes: Routes = [
  {
    path: '',
    component: PanelComponent,
    children: [
      {
        path: 'sentencia',
        loadChildren: () =>
          import('./sentencia/sentencia.module').then((m) => m.SentenciaModule),
      },
      {
        path: 'v-taquigraficas',
        loadChildren: () =>
          import('./taquigraficas/taquigraficas.module').then(
            (m) => m.TaquigraficasModule
          ),
      },
      {
        path: 'votos',
        loadChildren: () =>
          import('./votos/votos.module').then((m) => m.VotosModule),
      },
      {
        path: 'acuerdos-generales',
        loadChildren: () =>
          import('./acuerdos-generales/acuerdos-generales.module').then(
            (m) => m.AcuerdosGeneralesModule
          ),
      },
      {
        path: 'usuarios',
        loadChildren: () =>
          import('./usuarios/usuarios.module').then((m) => m.UsuariosModule),
      },
      {
        path: 'asignacion-asuntos',
        loadChildren: () =>
          import('./asignacion-asuntos/asignacion-asuntos.module').then(
            (m) => m.AsignacionAsuntosModule
          ),
      },
      {
        path: 'exploracion-datos',
        loadChildren: () =>
          import('./exploracion-datos/exploracion-datos.module').then(
            (m) => m.ExploracionDatosModule
          ),
      },
      {
        path: 'diversa-normativa',
        loadChildren: () =>
          import('./diversa-normativa/diversa-normativa.module').then(
            (m) => m.DiversaNormativaModule
          ),
      },
      {
        path: 'catalogos',
        loadChildren: () =>
          import('./catalogos/catalogos.module').then((m) => m.CatalogosModule),
      },
      {
        path: 'bitacora',
        loadChildren: () =>
          import('./bitacora/bitacora.module').then((m) => m.BitacoraModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelRoutingModule {}
