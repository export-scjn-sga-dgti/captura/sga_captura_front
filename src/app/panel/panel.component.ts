import { Component, OnInit } from '@angular/core';
import { Router,NavigationEnd  } from '@angular/router'
import { KeycloakService } from '../services/keycloak.service';
import { UsuariosApiService } from 'src/app/services/usuarios-api.service';
import { Usuario } from 'src/app/models/usuarioModel';
import { ConverStringArray } from '../helper/converArrayObject';
import { environment } from 'src/environments/environment';
import { Modules } from '../enum/modules.enum';
@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
  providers: [ConverStringArray],
})
export class PanelComponent implements OnInit {

  entity: string = ""
  isLogin: boolean = false
  username: string;
  usernameInitials: string;
  userData: Usuario;
  modulos: string[] = [];
  tagModule = Modules

  constructor(private param: Router,
    private keycloak: KeycloakService,
    private usuarioService: UsuariosApiService,
    private converString: ConverStringArray,
    ) {
   this.param.events.subscribe(ev =>{
     if(ev instanceof NavigationEnd){
      const module = this.param.url.split('/')[1]
      this.entity = module
     }
   });

   this.username = window.localStorage.getItem('username');
   this.usernameInitials = window.localStorage.getItem('usernameInitials');
  }


  ngOnInit(): void {
    let nav = document.getElementById('side-menu');
    nav.addEventListener('click', function (event) {
      const tag = event.target as HTMLElement;
      if (tag.tagName == 'A' || tag.tagName == 'SPAN') {
        if (tag.classList.contains('dropdown-toggle')) {
          tag.parentElement.classList.toggle('active-container');
          tag.classList.add('active-toggle', 'active-control');
          tag.nextElementSibling.classList.toggle('view');
        } else {
          if(tag.tagName == 'SPAN'){
            tag.offsetParent.parentElement.classList.toggle('active-container');
            tag.offsetParent.classList.add('active-toggle', 'active-control');
            tag?.offsetParent?.nextElementSibling?.classList.toggle('view');
          } else {
            const toggle = document.querySelectorAll('a.dropdown-toggle');
            toggle.forEach((items) => {
              items.parentElement.classList.remove('active-container');
              items.classList.remove('active-toggle', 'active-control');
              items?.nextElementSibling?.classList.remove('view');
            });
          }
        }
      }
    });
    
    const active = environment.activeKeycloak

    this.usuarioService.getUsuarioByUsername(active ? this.username: 'ggarciab').subscribe((data) => {
      this.userData = data;
      this.modulos = this.converString.proccessArrayModulo(
        data.modulos
      );
    });

  }

  logout() {
    this.param.navigate(['/']);
    this.entity == '';
    localStorage.removeItem('username');
    localStorage.removeItem('initialname');
    localStorage.removeItem('active');
    localStorage.removeItem('ministro');
    this.keycloak.logout();
  }

  openMain() {
    let main = document.getElementById('main-ds') as HTMLElement;
    if (main.classList.contains('compacted')) {
      main.classList.remove('compacted', 'js-compact');
    } else {
      main.classList.add('compacted', 'js-compact');
    }
  }
}
