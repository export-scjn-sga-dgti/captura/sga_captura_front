import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VotosRoutingModule } from './votos-routing.module';
import { VotosEdicionComponent } from './votos-edicion/votos-edicion.component';
import { VotosRegistroComponent } from './votos-registro/votos-registro.component';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { TreeTableModule } from 'primeng/treetable';
import { ToastModule } from 'primeng/toast';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { TooltipModule } from 'primeng/tooltip';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    VotosEdicionComponent, 
    VotosRegistroComponent
  ],
  imports: [
    CommonModule,
    VotosRoutingModule,
    CalendarModule,
    FormsModule,
    BreadcrumbModule,
    NgMultiSelectDropDownModule.forRoot(),
    TreeTableModule,
    ToastModule,
    ConfirmPopupModule,
    TooltipModule,
    DropdownModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VotosModule { }
