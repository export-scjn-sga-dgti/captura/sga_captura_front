import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VotosEdicionComponent } from './votos-edicion.component';

describe('VotosEdicionComponent', () => {
  let component: VotosEdicionComponent;
  let fixture: ComponentFixture<VotosEdicionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VotosEdicionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VotosEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
