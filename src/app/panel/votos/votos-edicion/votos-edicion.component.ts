import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Validate } from 'src/app/helper/validateExpediente';
import { Voto } from 'src/app/models/votosModel';
import { ScrollTop } from 'src/app/helper/scrollTop';

import { VotosApiService } from 'src/app/services/votos-api.service';
import { ConverStringArray } from "../../../helper/converArrayObject"
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-votos-edicion',
  templateUrl: './votos-edicion.component.html',
  styleUrls: ['./votos-edicion.component.scss'],
  providers: [ConverStringArray]
})
export class VotosEdicionComponent implements OnInit {

  idVoto: string = "";
  scroll: ScrollTop = new ScrollTop()
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    idField: 'nombre',
    textField: 'nombre',
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar'
  }
  breadcrumb: any = []
  catalogos: any = {}
  temaAbordado: string = ""
  votosModel: Voto = new Voto()
  TemaProcesalSobreElVoto: string[] = []
  TemaSustantivoSobreElVoto: string[] = []
  campos: number = 0
  requerid = {
    tipoVoto: false,
    ministro: false,
    tipoTemaProcesalSobreElVoto: false,
    temaProcesalSobreElVoto: false,
    tipoTemaSustantivoSobreElVoto: false,
    temaSustantivoSobreElVoto: false,
    tipoTema: false,
    tipoAsunto: false,
    numExpediente: false,
    fechaResolucion: false,
    metodologiaAnalisis: false
  }

  isView: boolean = false

  configDropdownTipoVoto: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    textField: 'nombre',
    idField: 'nombre',
    allowSearchFilter: true,
    enableCheckAll: false,
    searchPlaceholderText: 'Buscar'
  }

  derechosHumanos = [];
  sentidoResolucion = [];
  tipoEfectos = [];
  analisisConstitucionlidad = [];
  violacionOrganicaAnalizada = [];
  vicioProcesoLegislativo = [];
  violacionInvacionEsferasAnalizada = [];
  violacionInvacionPoderesAnalizado = [];
  IsTemasSustantivos = false;
  causaImprocedencia = [];
  violacionInvacionEsferas = [];
  violacionInvacionEsferasFederal = [];
  violacionInvacionEsferasMunicipal = [];
  ministros = [];
  tipoVoto = [];

  constructor(private params: ActivatedRoute,
    private toasService: ToastService,
    private votosService: VotosApiService,
    private catalogoService: CatalogosService,
    private router: Router,
    private converString: ConverStringArray) {
    this.scroll.scrollTo()
  }

  editarVoto() {
    this.votosService.updateVoto(this.votosModel).subscribe(
      res => {
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(nav => this.router.navigate(['/votos']))
        this.toasService.showToast(
          'success',
          'Voto editado',
          'Edición de voto exitosa')
      }
      , error => {
      }
    )
    /* this.campos = 0
    const validate = Validate(this.votosModel.numExpediente)
    for (const property in this.requerid) {
      this.requerid[property] = !this.votosModel[property] ? true : false
      if (!this.requerid[property]) this.campos += 1
    }

    this.requerid.tipoVoto = this.votosModel.tipoVoto ? false : true
    this.requerid.ministro = this.votosModel.ministro ? false : true
    this.requerid.tipoTema = this.votosModel.tipoTema ? false : true
    this.requerid.tipoAsunto = this.votosModel.tipoAsunto ? false : true
    this.requerid.numExpediente = this.votosModel.numExpediente ? false : true
    this.requerid.fechaResolucion = this.votosModel.fechaResolucion ? false : true
    this.requerid.metodologiaAnalisis = this.votosModel.metodologiaAnalisis ? false : true

    if (this.campos < 9) {
      this.toasService.showToast(
        'error',
        'Campos obligatorios',
        'Favor de llenar los campos requeridos'
      )

    } else {
      if (validate) {
        this.votosService.updateVoto(this.votosModel).subscribe(
          res => {
            this.router.navigateByUrl('/', { skipLocationChange: true }).then(nav => this.router.navigate(['/votos']))
            this.toasService.showToast(
              'success',
              'Voto editado',
              'Edición de voto exitosa')
          }
          , error => {
          }
        )
      } else {
        this.toasService.showToast(
          'warn',
          'Alerta',
          'Número de expediente no válido'
        )
      }
    } */

  }

  toggleTag(field: string): void {
    this.votosModel.temaProcesalSobreElVoto = null
    this.votosModel.tipoTemaProcesalSobreElVoto = null
    this.votosModel.temaSustantivoSobreElVoto = null
    this.votosModel.tipoTemaSustantivoSobreElVoto = null
    if (field != "") this.getTemaProcesalSobreElVoto(this.votosModel.tipoTema);
    if (field != "") this.getTemaSustantivoSobreElVoto(this.votosModel.tipoTema);
  }

  getTemaProcesalSobreElVoto(field: string): void {
    this.catalogoService.getCatalogosByColeccionCampo('', 'bd_sga_voto', 'temaProcesalSobreElVoto').subscribe(
      (data) => {
        this.TemaProcesalSobreElVoto = data;
        // this.TemaProcesalSobreElVoto = this.converString.proccessArrayColeccionCampo(
        //   data
        // );
      },
      (error) => { }
    );
  }

  getTemaSustantivoSobreElVoto(field: string): void {
    this.catalogoService.getCatalogosByColeccionCampo('', 'bd_sga_voto', 'temaSustantivoSobreElVoto').subscribe(
      (data) => {
        this.TemaSustantivoSobreElVoto = data;
        // this.TemaSustantivoSobreElVoto = this.converString.proccessArrayColeccionCampo(
        //   data
        // );
      },
      (error) => { }
    );
  }

 ngOnInit() {

    this.params.paramMap.subscribe(responseData => {
      this.idVoto = responseData.get("idVoto")
      this.params.queryParamMap.subscribe(querys => {
        if (querys.has('visualizar')) {
          this.isView = true
        }
      });
    });

    this.catalogoService.getCatalogosVotos().subscribe(data => {
      this.catalogos = data;
      //
    //console.log(data);

      this.tipoVoto = [];
      this.catalogos.tipoVoto.forEach(xx => {
        this.tipoVoto.push(xx.nombre);
      });

      this.ministros = [];
      this.catalogos.ministros.forEach(xx => {
        this.ministros.push(xx.nombre);
      });

      this.causaImprocedencia = [];
      this.catalogos.causaImprocedencia.forEach(xx => {
        this.causaImprocedencia.push(xx.nombre);
      });

      this.derechosHumanos = [];
      this.catalogos.derechosHumanos.forEach(xx => {
        this.derechosHumanos.push(xx.nombre);
      });

      this.sentidoResolucion = [];
      this.catalogos.sentidoResolucion.forEach(xx => {
        this.sentidoResolucion.push(xx.nombre);
      });

      this.tipoEfectos = [];
      this.catalogos.tipoSentencia.forEach(xx => {
        this.tipoEfectos.push(xx.nombre);
      });

      this.analisisConstitucionlidad = [];
      this.catalogos.analisisConstitucionlidad.forEach(xx => {
        this.analisisConstitucionlidad.push(xx.nombre);
      });

      this.violacionOrganicaAnalizada = [];
      this.catalogos.violacionOrganicaAnalizada.forEach(xx => {
        this.violacionOrganicaAnalizada.push(xx.nombre);
      });

      this.vicioProcesoLegislativo = [];
      this.catalogos.vicioProcesoLegislativo.forEach(xx => {
        this.vicioProcesoLegislativo.push(xx.nombre);
      });

      this.violacionInvacionEsferasAnalizada = [];
      this.catalogos.violacionInvacionEsferasAnalizada.forEach(xx => {
        this.violacionInvacionEsferasAnalizada.push(xx.nombre);
      });

      this.violacionInvacionPoderesAnalizado = [];
      this.catalogos.violacionInvacionPoderesAnalizado.forEach(xx => {
        this.violacionInvacionPoderesAnalizado.push(xx.nombre);
      });

      this.violacionInvacionEsferas = [];
      this.catalogos.violacionInvacionEsferas.forEach(xx => {
        this.violacionInvacionEsferas.push(xx.nombre);
      });

      this.violacionInvacionEsferasFederal = [];
      this.catalogos.violacionInvacionEsferasFederal.forEach(xx => {
        this.violacionInvacionEsferasFederal.push(xx.nombre);
      });

      this.violacionInvacionEsferasMunicipal = [];
      this.catalogos.violacionInvacionEsferasMunicipal.forEach(xx => {
        this.violacionInvacionEsferasMunicipal.push(xx.nombre);
      });

      this.votosService.getVotoByID(this.idVoto).subscribe(
        res => {
          this.votosModel = res
          //console.log(this.votosModel);

          this.getTemaProcesalSobreElVoto(this.votosModel.tipoTema);
          this.getTemaSustantivoSobreElVoto(this.votosModel.tipoTema);
          this.breadcrumb = [
            { label: this.votosModel.tipoAsunto + " " + this.votosModel.numExpediente, routerLink: "./", queryParams: this.isView?{visualizar:true}:{} }
          ]
        }
        , error => {
        }
      );

    });
  }


  onItemDeSelectViOrganicaAnalizada(item: any) {

    if (this.votosModel.violacionOrganicaAnalizada?.includes('INVASIÓN DE PODERES')) {
      this.votosModel.violacionInvacionPoderesAnalizado = null;
    }

    if (this.votosModel.violacionOrganicaAnalizada?.includes('INVASIÓN DE ESFERAS')) {
      this.votosModel.violacionInvacionEsferasAnalizada = null;
    }

    if (this.votosModel.violacionOrganicaAnalizada?.includes('PROCESO LEGISLATIVO')) {
      this.votosModel.vicioProcesoLegislativo = null;
    }

  }

  changeSelectInvasionEsferas(item: any) {
    //console.log(item);

  }

  onItemDeSelectSentidoResol(item: any) {
    if (!this.votosModel.sentidoResolucion?.includes('FUNDADA')) {
      this.votosModel.tipoEfectos = null;
    }
  }

  onItemDeSelectViolacionInvacionEsferasAnalizada(item: any) {

    if (this.votosModel.violacionInvacionEsferasAnalizada?.includes('FEDERAL')) {
      this.votosModel.violacionInvacionEsferasFederal = null;
    }

    if (this.votosModel.violacionInvacionEsferasAnalizada?.includes('LOCAL')) {
      this.votosModel.violacionInvacionEsferas = null;
    }

    if (this.votosModel.violacionInvacionEsferasAnalizada?.includes('MUNICIPAL')) {
      this.votosModel.violacionInvacionEsferasMunicipal = null;
    }

  }

}
