import { Component, OnInit } from '@angular/core';
import { Search, Voto, AllVotoModel } from 'src/app/models/votosModel';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { VotosApiService } from 'src/app/services/votos-api.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import { ConfirmationService } from 'primeng/api';
import { Validate } from 'src/app/helper/validateExpediente';
import { SaveFiltersService } from 'src/app/services/save-filters.service';
import { SaveFilters } from 'src/app/models/save-filters';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-votos',
  templateUrl: './votos.component.html',
  styleUrls: ['./votos.component.scss'],
  providers: [ConfirmationService],
})
export class VotosComponent implements OnInit {
  tipoAsuntoArray: { id: string; catalogo: string; nombre: string }[] = [];
  tipoVotoArray: { id: string; catalogo: string; nombre: string }[] = [];
  page: number = 1;
  votos: Voto[] = [];
  busqueda: Search = new Search();
  fecha: Date = new Date();
  filtrado: string = '';
  rows: number = 10;
  totalVotos: number = 0;
  viewSearch: boolean = false;
  totalPages: number = 0;
  requerid = {
    tipoVoto: false,
    numExpediente: false,
    tipoAsunto: false,
  };
  votoAllMod: AllVotoModel = new AllVotoModel();
  constructor(
    private catalogoService: CatalogosService,
    private votoService: VotosApiService,
    private spinner: SpinnerService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private saveFilters: SaveFiltersService
  ) {
    this.catalogoService.getCatalogosVotos().subscribe((catalogos) => {
      this.tipoAsuntoArray = catalogos.voto_tipoAsunto;
      this.tipoVotoArray = catalogos.tipoVoto;
    });
  }

  confirm(event: Event, votos: Voto) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar el documento?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        let indice: number = this.votos.indexOf(votos);
        this.votoService.deleteVotoByID(votos.id).subscribe(
          (res) => {
            if (indice != -1) this.votos.splice(indice, 1);
            this.toast.showToast(
              'success',
              'Voto',
              'Ha sido eliminado con éxito',
            );
          },
          (error) => {
            this.toast.showToast(
              'error',
              'Error',
              'Ha ocurrido algo inesperado al eliminar el voto',
            );
          }
        );
      },
      reject: () => {
        this.toast.showToast(
          'info',
          'Voto',
          'Acción cancelada',
        );
      },
    });
  }

  ngOnInit(): void {
    if(this.saveFilters.filters.module == 'votos'){
      const data = this.saveFilters.filters.filter.split(',')
      data.forEach(keys =>{
        const k = keys.split(':')
        this.busqueda[k[0]] = k[1]
      })
      this.viewSearch = true
      this.filtrado = this.saveFilters.filters.filter
      this.allVotos(1, this.rows, this.filtrado);
    } else {
      this.saveFilters.filters = {} as SaveFilters
      this.realizaBusqueda();
    }
    
  }

  construyeFiltro(): void {
    this.filtrado = '';
    const validate = Validate(this.busqueda.numExpediente);

    if (
      !this.busqueda.numExpediente &&
      !this.busqueda.tipoAsunto &&
      !this.busqueda.tipoVoto
    ) {
      this.requerid.numExpediente = true;
      this.requerid.tipoAsunto = true;
      this.requerid.tipoVoto = true;
      this.toast.showToast(
        'error',
        'Búsqueda',
        'Se requiere al menos un parámetro para realizar la búsqueda',
      );
    } else {
      this.requerid.numExpediente = false;
      this.requerid.tipoAsunto = false;
      this.requerid.tipoVoto = false;
      this.filtrado += this.busqueda.tipoAsunto
        ? 'tipoAsunto: ' + this.busqueda.tipoAsunto + ','
        : '';
      if (this.busqueda.numExpediente) {
        this.requerid.numExpediente = !validate;
        if (validate) {
          this.filtrado += this.busqueda.numExpediente
            ? 'numExpediente: ' + this.busqueda.numExpediente + ','
            : '';
        } else {
          this.toast.showToast(
            'error',
            'Búsqueda',
            'Número de expediente no válido',
          );
        }
      }
      this.filtrado += this.busqueda.tipoVoto
        ? 'tipoVoto: ' + this.busqueda.tipoVoto + ','
        : '';
      this.viewSearch = true;
      this.saveFilters.filters.module = "votos"
      this.saveFilters.filters.filter = this.filtrado
      this.realizaBusqueda(1, this.rows, this.filtrado);
    }
  }

  restauraTabla(): void {
    this.filtrado = '';
    this.viewSearch = false;
    this.busqueda.numExpediente = null;
    this.busqueda.tipoAsunto = null;
    this.busqueda.tipoVoto = null;
    this.saveFilters.filters = {} as SaveFilters;
    this.realizaBusqueda();
  }

  public calculateSizePage(){
    if(this.votoAllMod.size*(1+this.votoAllMod.number)>=this.votoAllMod.totalElements){
      return this.votoAllMod.totalElements;
    }else{
      return (this.votoAllMod.size*(1+this.votoAllMod.number));
    }
  }

  onChangeSize({ value }: any) {
    this.votoAllMod.size = value;
    this.allVotos(this.votoAllMod.number + 1, this.votoAllMod.size, this.filtrado);
  }

  private allVotos(page: number, size: number, filtros?: string) {
    this.votoService.getAllVotos(page, size, filtros).subscribe(
      (data) => {
        this.votoAllMod = data;
        if (data.content.length > 0) {
          this.votos = data.content;
          this.totalVotos = filtros ? data.content.length : data.totalElements;
          this.totalPages = data.totalPages;
        } else {
          this.votos = [];
          this.totalVotos = 0;
          this.totalPages = 0;
          this.toast.showToast(
            'info',
            'Búsqueda',
            'No se encontraron resultados sobre la búsqueda',
          );
        }
        document.querySelector('.p-datatable-scrollable-body').scroll(0, 0);
      },
      (error) => {}
    );
  }

  realizaBusqueda(
    page: number = 1,
    rows: number = 10,
    filtro: string = ''
  ): void {
    this.votoService.getAllVotos(page, rows, filtro).subscribe(
      (data) => {
        this.votoAllMod = data;
        if (data.content.length > 0) {
          this.votos = data.content;
          this.totalVotos = filtro ? data.content.length : data.totalElements;
          this.totalPages = data.totalPages;
        } else {
          this.votos = [];
          this.totalVotos = 0;
          this.totalPages = 0;
          this.toast.showToast(
            'info',
            'Búsqueda',
            'No se encontraron resultados sobre la búsqueda',
          );
        }
      },
      (error) => {}
    );
  }
}
