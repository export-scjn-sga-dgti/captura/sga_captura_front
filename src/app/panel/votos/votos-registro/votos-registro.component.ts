import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Validate } from 'src/app/helper/validateExpediente';
import { Voto } from 'src/app/models/votosModel';
import { ScrollTop } from 'src/app/helper/scrollTop';

import { CatalogosService } from 'src/app/services/catalogos.service';
import { VotosApiService } from 'src/app/services/votos-api.service';
import { ConverStringArray } from "../../../helper/converArrayObject";
import { ToastService } from 'src/app/services/toast.service';
import { Sentencia, Votacion } from 'src/app/models/sentenciaModel';

@Component({
  selector: 'app-votos-registro',
  templateUrl: './votos-registro.component.html',
  styleUrls: ['./votos-registro.component.scss'],
  providers: [ConverStringArray]
})

export class VotosRegistroComponent implements OnInit {

  constructor(private toastService: ToastService,
    private votosService: VotosApiService,
    private catalogoService: CatalogosService,
    private router: Router,
    private converString: ConverStringArray,
  ) {
    this.scroll.scrollTo();
  }

  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    textField: 'nombre',
    allowSearchFilter: true,
    enableCheckAll: false,
    searchPlaceholderText: 'Buscar'
  }

  configDropdownVi: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    allowSearchFilter: true,
    enableCheckAll: false,
    searchPlaceholderText: 'Buscar'
  }

  configDropdownTipoVoto: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    textField: 'nombre',
    idField: 'nombre',
    allowSearchFilter: true,
    enableCheckAll: false,
    searchPlaceholderText: 'Buscar'
  }

  scroll: ScrollTop = new ScrollTop()
  showMinistros: boolean = true;
  catalogos: any = {}
  tipoVotosAny = [];
  votosModel: Voto = new Voto()
  TemaProcesalSobreElVoto: string[] = []
  TemaSustantivoSobreElVoto: string[] = []
  autocompletado: string[] = []
  campos: number = 0
  requerid = {
    tipoVoto: false,
    ministro: false,
    tipoTemaProcesalSobreElVoto: false,
    temaProcesalSobreElVoto: false,
    tipoTemaSustantivoSobreElVoto: false,
    temaSustantivoSobreElVoto: false,
    tipoTema: false,
    tipoAsunto: false,
    numExpediente: false,
    fechaResolucion: false,
    metodologiaAnalisis: false
  }

  sentenciaValida = false;
  resolucionSentencia = false;
  /*Datos concernientes a la sentencia*/
  /*sentenciaEncontrada: Sentencia;
  votoEncontrado: Voto;
  sentenciaValida = false;
  violacionesADH = false;
  violacionesOrganicas = false;
  votacionesCausas: Votacion[] = [];
  votacionesDH: Votacion[] = [];
  votacionesTiposvicio: Votacion[] = [];
  votacionesPorInvasionEsferas: Votacion[] = [];
  votacionesPorInvasionPoderes: Votacion[] = [];*/

  derechosHumanos = [];
  sentidoResolucion = [];
  tipoEfectos = [];
  analisisConstitucionlidad = [];
  violacionOrganicaAnalizada = [];
  vicioProcesoLegislativo = [];
  violacionInvacionEsferasAnalizada = [];
  violacionInvacionPoderesAnalizado = [];
  IsTemasSustantivos = false;
  causaImprocedencia = [];
  violacionInvacionEsferas = [];
  violacionInvacionEsferasFederal = [];
  violacionInvacionEsferasMunicipal = [];

  crearVoto() {
    //console.log(this.votosModel);

    this.votosService.postVoto(this.votosModel).subscribe(
      res => {
        console.log(res);
        if (res['id']) {
          this.toastService.showToast('success', 'Voto', 'Ha sido creado con éxito.')
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(nav => this.router.navigate(['/votos']))
        }
      }
      , error => {
        this.toastService.showToast('error', 'Error', 'Ha ocurrido algo inesperado al crear el voto.')
      })

    /* this.campos = 0
    const validate = Validate(this.votosModel.numExpediente)
    for (const property in this.requerid) {
      this.requerid[property] = !this.votosModel[property] ? true : false
      if (!this.requerid[property]) this.campos += 1
    }
    console.log(this.campos);
     console.log(this.votosModel);
    if (this.campos < 9) {
      this.toastService.showToast(
        'error',
        'Campos obligatorios',
        'Favor de llenar los campos requeridos'
      )
    } else {
      if (validate) {
        console.log(this.votosModel);
         this.votosService.postVoto(this.votosModel).subscribe(
           res => {
             if (res['id']) {
               this.toastService.showToast('success', 'Voto', 'Ha sido creado con éxito.')
               this.router.navigateByUrl('/', { skipLocationChange: true }).then(nav => this.router.navigate(['/votos']))
             }
           }
           , error => {
             this.toastService.showToast('error', 'Error', 'Ha ocurrido algo inesperado al crear el voto.')
           })
      } else {
        this.toastService.showToast('warn', 'Alerta', 'Número de expediente no válido')
      }
    } */
  }

  ngOnInit(): void {
    this.catalogoService.getCatalogosVotos().subscribe(data => {
      this.catalogos = data;
      //console.log(this.catalogos);

      this.catalogos.ministros = this.converString.proccessArray(this.catalogos.ministros);
      this.catalogos.tipoVoto.forEach(xx => {
        this.tipoVotosAny.push(xx.nombre);
      });

      this.catalogos.derechosHumanos.forEach(xx => {
        this.derechosHumanos.push(xx.nombre);
      });

      this.catalogos.sentidoResolucion.forEach(xx => {
        this.sentidoResolucion.push(xx.nombre);
      });

      this.catalogos.tipoSentencia.forEach(xx => {
        this.tipoEfectos.push(xx.nombre);
      });

      this.catalogos.analisisConstitucionlidad.forEach(xx => {
        this.analisisConstitucionlidad.push(xx.nombre);
      });

      this.catalogos.violacionOrganicaAnalizada.forEach(xx => {
        this.violacionOrganicaAnalizada.push(xx.nombre);
      });

      this.catalogos.vicioProcesoLegislativo.forEach(xx => {
        this.vicioProcesoLegislativo.push(xx.nombre);
      });

      this.catalogos.violacionInvacionEsferasAnalizada.forEach(xx => {
        this.violacionInvacionEsferasAnalizada.push(xx.nombre);
      });

      this.catalogos.violacionInvacionPoderesAnalizado.forEach(xx => {
        this.violacionInvacionPoderesAnalizado.push(xx.nombre);
      });

      this.catalogos.violacionInvacionEsferas.forEach(xx => {
        this.violacionInvacionEsferas.push(xx.nombre);
      });

      this.catalogos.violacionInvacionEsferasFederal.forEach(xx => {
        this.violacionInvacionEsferasFederal.push(xx.nombre);
      });

      this.catalogos.violacionInvacionEsferasMunicipal.forEach(xx => {
        this.violacionInvacionEsferasMunicipal.push(xx.nombre);
      });


    });

    this.catalogoService.getCatalogosVotos().subscribe((data) => {
      //console.log(data);
      data.causaImprocedencia
        .forEach(xx => {
          this.causaImprocedencia
            .push(xx.nombre);
        });
      //console.log( this.causaImprocedencia);

    });

  }

  getMinistroByDate(fecha: string) {
    this.catalogoService.getCatalogosVotos(fecha).subscribe(data => {
      this.catalogos = data
      this.catalogos.ministros = this.converString.proccessArray(this.catalogos.ministros)
    })
  }

  toggleTag(field: string): void {
    this.votosModel.temaProcesalSobreElVoto = ""
    this.votosModel.temaSustantivoSobreElVoto = ""
    this.votosModel.tipoTemaSustantivoSobreElVoto = ""
    this.votosModel.tipoTemaProcesalSobreElVoto = ""
    if (field != "") this.getTemaProcesalSobreElVoto(this.votosModel.tipoTema);
    if (field != "") this.getTemaSustantivoSobreElVoto(this.votosModel.tipoTema);


    if (field["value"] == "SUSTANTIVO" || field["value"] == "PROCESAL Y SUSTANTIVO") {
      this.IsTemasSustantivos = true;
    }
    else {
      this.IsTemasSustantivos = false;
      this.tipoTemaSustantivo = "";
    }

  }


  tipoTemaSustantivo = "";

  toggleTagTemaSustantivo(field: string): void {
    this.tipoTemaSustantivo = field["value"];
    //console.log(this.tipoTemaSustantivo);
  }

  getTemaProcesalSobreElVoto(field: string): void {
    this.catalogoService.getCatalogosByColeccionCampo('', 'bd_sga_voto', 'temaProcesalSobreElVoto').subscribe(
      (data) => {
        this.TemaProcesalSobreElVoto = data;
        // this.TemaProcesalSobreElVoto = this.converString.proccessArrayColeccionCampo(
        //   data
        // );
      },
      (error) => { }
    );
  }

  getTemaSustantivoSobreElVoto(field: string): void {
    this.catalogoService.getCatalogosByColeccionCampo('', 'bd_sga_voto', 'temaSustantivoSobreElVoto').subscribe(
      (data) => {
        this.TemaSustantivoSobreElVoto = data;
        // this.TemaSustantivoSobreElVoto = this.converString.proccessArrayColeccionCampo(
        //   data
        // );
      },
      (error) => { }
    );
  }

  validarSentencia(): void {
    if ((this.votosModel.tipoAsunto != null && this.votosModel.tipoAsunto) &&
      (this.votosModel.numExpediente != null && this.votosModel.numExpediente)) {
      this.votosService.validaSentencia(this.votosModel.tipoAsunto, this.votosModel.numExpediente).subscribe(data => {
        let sentenciaEncontrada = data;
        let validacion = false;

        sentenciaEncontrada ? validacion = true
          : validacion = false;

        if (validacion) {
          this.votosService.getVotoBySentencia(this.votosModel.tipoAsunto, this.votosModel.numExpediente).subscribe(
            data => {
              let votoEncontrado = data;
              //console.log(data);

              if (votoEncontrado) {
                this.router.navigateByUrl('/', { skipLocationChange: true }).then(nav => this.router.navigate(['/votos/edicion/' + votoEncontrado.id]))
              } else {
                this.sentenciaValida = true;
                if (sentenciaEncontrada.fechaResolucion) {
                  this.votosModel.fechaResolucion = this.convertToDate(sentenciaEncontrada.fechaResolucion);
                  //this.getMinistroByDate(sentenciaEncontrada.fechaResolucion);
                  this.resolucionSentencia = true;
                }
              }
            }
          );
        } else {
          this.toastService.showToast('error', 'Sentencia no registrada', 'La sentencia no se encuentra registrada en el sistema.');
        }
      }
      );
    } else {
      this.toastService.showToast('error', 'Error', 'Ingrese los valores para Tipo de Asunto y Número de Expediente.');
    }
  }

  convertToDate(responseDate: any) {

    let parts = responseDate.split('-');

    return new Date(parts[0], parts[1] - 1, parts[2]);
  }

  convertToLocalDate(responseDate: any) {
    try {
      if (responseDate != null) {
        if (typeof (responseDate) === 'string') {
          if (String(responseDate.indexOf('T') >= 0)) {
            responseDate = responseDate.split('T')[0];
          }
          if (String(responseDate.indexOf('+') >= 0)) {
            responseDate = responseDate.split('+')[0];
          }
        }

        responseDate = new Date(responseDate);
        const newDate = new Date(responseDate.getFullYear(), responseDate.getMonth(), responseDate.getDate(), 0, 0, 0);
        const userTimezoneOffset = newDate.getTimezoneOffset() * 60000;

        const finalDate: Date = new Date(newDate.getTime());
        return responseDate;
      } else {
        return null;
      }
    } catch (error) {
      return responseDate;
    }
  }

  onItemDeSelectViOrganicaAnalizada(item: any) {

    if (this.votosModel.violacionOrganicaAnalizada?.includes('INVASIÓN DE PODERES')) {
      this.votosModel.violacionInvacionPoderesAnalizado = null;
    }

    if (this.votosModel.violacionOrganicaAnalizada?.includes('INVASIÓN DE ESFERAS')) {
      this.votosModel.violacionInvacionEsferasAnalizada = null;
    }

    if (this.votosModel.violacionOrganicaAnalizada?.includes('PROCESO LEGISLATIVO')) {
      this.votosModel.vicioProcesoLegislativo = null;
    }

  }

  changeSelectInvasionEsferas(item: any) {
    //console.log(item);

  }

  onItemDeSelectSentidoResol(item: any) {
    if (!this.votosModel.sentidoResolucion?.includes('FUNDADA')) {
      this.votosModel.tipoEfectos = null;
    }
  }

  onItemDeSelectViolacionInvacionEsferasAnalizada(item: any) {

    if (this.votosModel.violacionInvacionEsferasAnalizada?.includes('FEDERAL')) {
      this.votosModel.violacionInvacionEsferasFederal = null;
    }

    if (this.votosModel.violacionInvacionEsferasAnalizada?.includes('LOCAL')) {
      this.votosModel.violacionInvacionEsferas = null;
    }

    if (this.votosModel.violacionInvacionEsferasAnalizada?.includes('MUNICIPAL')) {
      this.votosModel.violacionInvacionEsferasMunicipal = null;
    }

  }

}
