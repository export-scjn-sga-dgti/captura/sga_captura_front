import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VotosRegistroComponent } from './votos-registro.component';

describe('VotosRegistroComponent', () => {
  let component: VotosRegistroComponent;
  let fixture: ComponentFixture<VotosRegistroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VotosRegistroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VotosRegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
