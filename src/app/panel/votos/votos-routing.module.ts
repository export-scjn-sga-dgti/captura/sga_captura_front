import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VotosEdicionComponent } from './votos-edicion/votos-edicion.component';
import { VotosRegistroComponent } from './votos-registro/votos-registro.component';
import { VotosComponent } from './votos.component';

const routes: Routes = [  
  {path:"",component:VotosComponent},
  {path:"registro",component:VotosRegistroComponent},
  {path:"edicion/:idVoto",component:VotosEdicionComponent} 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VotosRoutingModule { }
