import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcuerdosGeneralesComponent } from './acuerdos-generales.component';

describe('AcuerdosGeneralesComponent', () => {
  let component: AcuerdosGeneralesComponent;
  let fixture: ComponentFixture<AcuerdosGeneralesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcuerdosGeneralesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcuerdosGeneralesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
