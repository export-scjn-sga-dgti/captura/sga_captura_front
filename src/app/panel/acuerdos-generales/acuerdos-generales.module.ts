import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcuerdosGeneralesRoutingModule } from './acuerdos-generales-routing.module';
import { AcuerdoGRegistroComponent } from './acuerdo-g-registro/acuerdo-g-registro.component';
import { AcuerdoGEdicionComponent } from './acuerdo-g-edicion/acuerdo-g-edicion.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ConfirmationService, MessageService } from 'primeng/api';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog'
import { ConfirmPopupModule } from 'primeng/confirmpopup';

import { TemasEspecificosRegulacionComponent } from 'src/app/components/temas-especificos-regulacion/temas-especificos-regulacion.component';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [AcuerdoGRegistroComponent, AcuerdoGEdicionComponent,TemasEspecificosRegulacionComponent],
  imports: [
    CommonModule,
    AcuerdosGeneralesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    CalendarModule,
    NgMultiSelectDropDownModule.forRoot(),
    BreadcrumbModule,
    ToastModule,
    DialogModule,
    ConfirmPopupModule,
    DropdownModule
  ],
  providers: [MessageService, ConfirmationService],
})
export class AcuerdosGeneralesModule {}
