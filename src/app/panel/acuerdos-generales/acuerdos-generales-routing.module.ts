import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcuerdoGEdicionComponent } from './acuerdo-g-edicion/acuerdo-g-edicion.component';
import { AcuerdoGRegistroComponent } from './acuerdo-g-registro/acuerdo-g-registro.component';
import { AcuerdosGeneralesComponent } from './acuerdos-generales.component';

const routes: Routes = [
  { path: '', component: AcuerdosGeneralesComponent },
  { path: 'registro', component: AcuerdoGRegistroComponent },
  { path: 'edicion/:id', component: AcuerdoGEdicionComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcuerdosGeneralesRoutingModule {}
