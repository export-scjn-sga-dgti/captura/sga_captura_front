import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { AcuerdosApiService } from 'src/app/services/acuerdos-api.service';
import { Acuerdo } from 'src/app/models/acuerdosModel';
import { ConfirmationService } from 'primeng/api';
import { ConverStringArray } from '../../../helper/converArrayObject';
import { ScrollTop } from 'src/app/helper/scrollTop';

@Component({
  selector: 'app-acuerdo-g-edicion',
  templateUrl: './acuerdo-g-edicion.component.html',
  styleUrls: ['./acuerdo-g-edicion.component.scss'],
  providers: [ConverStringArray],
})
export class AcuerdoGEdicionComponent implements OnInit {
  idAcuerdo: string = '';
  campos: number = 0;
  catalogos: any = {};
  configModal: { open: boolean };
  materiaEdit: any;
  acuerdoModel: Acuerdo = new Acuerdo();
  scroll: ScrollTop = new ScrollTop();
  breadcrumb: any = [];
  materia: string[] = [];
  temasEspecificosRegulacion: string[] = [];
  clasificacionMateriaRegulacion: string[] = [];
  votacionFavor: string[] = [];
  votacionContra: string[] = [];
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  requerid = {
    rubro: false,
    fechaAprobacion: false,
    ambitoIncide: false,
    vigencia: false,
    fechaPublicacionDOF: false,
    clasificacionMateriaRegulacion: false,
    organoEmisor: false,
    ministroPresidente: false,
    votacion: false,
    temasEspecificosRegulacion: false,
  };
  isView: boolean = false

  constructor(
    private params: ActivatedRoute,
    private catalogoService: CatalogosService,
    private acuerdoService: AcuerdosApiService,
    private converString: ConverStringArray,
    private router: Router,
    private datePipe: DatePipe,
    private confirmationService: ConfirmationService
  ) {
    this.scroll.scrollTo();
  }

  ngOnInit(): void {
    this.params.paramMap.subscribe((responseData) => {
      this.idAcuerdo = responseData.get('id');
      this.params.queryParamMap.subscribe(querys => {
        if (querys.has('visualizar')) {
          this.isView = true
        }
      })
    });

    this.acuerdoService.getAcuerdoByID(this.idAcuerdo).subscribe((res) => {
      this.catalogoService.getCatalogosAcuerdos(res.fechaAprobacion).subscribe(
        (data) => {
          this.catalogos = data;
          this.catalogos['materiaRegulacion'] = this.converString.proccessArray(
            this.catalogos['materiaRegulacion']
          );

          this.catalogos['ministros'] = this.converString.proccessArray(
            data['ministros']
          );
          this.catalogoService.getCatalogosByColeccionCampo('', 'bd_sga_acuerdos_generales', 'temasEspecificosRegulacion').subscribe(
            (data) => {
              this.catalogos.temasEspecificosRegulacion = data;
              this.catalogos['temasEspecificosRegulacion'] = this.converString.proccessArrayColeccionCampo(
                this.catalogos['temasEspecificosRegulacion']
              );
            },
            (error) => {}
          );
        },
        (error) => {}
      );
      this.acuerdoModel = res;
      // this.acuerdoModel.fechaAprobacion = res.fechaAprobacion.split('-').reverse().join('-')
      this.votacionFavor = res.votacionAcuerdoFavor;
      this.votacionContra = res.votacionAcuerdoEnContra;
      let crumb = '';
      if (this.acuerdoModel.rubro.length > 80) {
        crumb = this.acuerdoModel.rubro.slice(0, 80) + '...';
      } else {
        crumb = this.acuerdoModel.rubro;
      }
      this.breadcrumb = [{ label: crumb, routerLink: './' ,queryParams: this.isView?{visualizar:true}:{}}];
    });
  }

  editarAcuerdo() {
    this.campos = 0;
    for (const property in this.requerid) {
      this.requerid[property] = !this.acuerdoModel[property] ? true : false;
      if (!this.requerid[property]) this.campos += 1;
    }
    if (this.campos > 1)
      this.acuerdoService.updateAcuerdo(this.acuerdoModel).subscribe((res) => {
        if (res['statusCode'] == 200) {
          this.router
            .navigateByUrl('/', { skipLocationChange: true })
            .then((nav) => this.router.navigate(['/acuerdos-generales']));
        }
      });
  }

  selectMinistro(event: any, tipo: string) {
    console.log(event);

    this.acuerdoModel.ministrosAusentes.includes(event)
      ? this.acuerdoModel.ministrosAusentes.splice(
          this.acuerdoModel.ministrosAusentes.indexOf(event),
          1
        )
      : null;
    switch (tipo) {
      case 'favor':
        this.votacionContra.includes(event)
          ? this.votacionFavor.push(
              this.votacionContra
                .splice(this.votacionContra.indexOf(event), 1)
                .toString()
            )
          : null;
        this.acuerdoModel.votacionAcuerdoEnContra = [];
        break;
      case 'contra':
        this.votacionFavor.includes(event)
          ? this.votacionContra.push(
              this.votacionFavor
                .splice(this.votacionFavor.indexOf(event), 1)
                .toString()
            )
          : null;
        this.acuerdoModel.votacionAcuerdoFavor = [];
        break;
      default:
    }

    setTimeout(() => {
      this.acuerdoModel.votacionAcuerdoFavor = this.votacionFavor;
      this.acuerdoModel.votacionAcuerdoEnContra = this.votacionContra;
    }, 100);
  }

  displayModal() {
    console.log(this.catalogos);
    const config = {
      catalogo: this.catalogos['temasEspecificosRegulacion'],
      open: true,
    };
    this.configModal = config;
  }

  getModal(response: { index: number; tema: string; open: boolean }) {
    this.temasEspecificosRegulacion = this.acuerdoModel.temasEspecificosRegulacion
      ? this.acuerdoModel.temasEspecificosRegulacion
      : [];
    this.temasEspecificosRegulacion.push(response.tema);
    this.acuerdoModel.temasEspecificosRegulacion = this.temasEspecificosRegulacion;
  }

  getModalUpdateTema(response: { index: number; tema: string; open: boolean }) {
    if (this.acuerdoModel.temasEspecificosRegulacion) {
      this.temasEspecificosRegulacion[response.index] = response.tema;
      this.acuerdoModel.temasEspecificosRegulacion = this.temasEspecificosRegulacion;
    } else {
      this.getModal(response);
    }
  }

  edicionMateria(materia: string, index: number) {
    this.materiaEdit = {
      materia: materia,
      index: index,
      catalogo: this.catalogos['temasEspecificosRegulacion'],
      open: true,
    };
    console.log(this.materiaEdit);
  }

  confirm(index: number) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar el documento?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.temasEspecificosRegulacion.splice(index, 1);
        this.acuerdoModel.temasEspecificosRegulacion = this.temasEspecificosRegulacion;
      },
    });
  }
}
