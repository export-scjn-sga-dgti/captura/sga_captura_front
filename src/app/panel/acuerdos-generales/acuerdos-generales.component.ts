import { Component, OnInit } from '@angular/core';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ConfirmationService } from 'primeng/api';
import { AcuerdosApiService } from 'src/app/services/acuerdos-api.service';
import { Search, Acuerdo, AllAcuerdoModel } from 'src/app/models/acuerdosModel';
import { DatePipe } from '@angular/common';
import { SaveFilters } from 'src/app/models/save-filters';
import { SaveFiltersService } from 'src/app/services/save-filters.service';

@Component({
  selector: 'app-acuerdos-generales',
  templateUrl: './acuerdos-generales.component.html',
  styleUrls: ['./acuerdos-generales.component.scss'],
  providers: [ConfirmationService]
})
export class AcuerdosGeneralesComponent implements OnInit {

  acuerdoAllMod: AllAcuerdoModel = new AllAcuerdoModel();
  constructor(
    private catalogoService: CatalogosService,
    private confirmationService: ConfirmationService,
    private acuerdoService: AcuerdosApiService,
    private datePipe: DatePipe,
    private saveFilters: SaveFiltersService
    ) { }

  materiaRegulacionArray: {}[] = [  ]
  vigenciaArray: {}[] = []
  acuerdos: Acuerdo[];
  totalAcuerdos: number = 0
  totalPages: number = 0
  filtrado: string = ''
  rows: number = 10
  page:number = 1
  busqueda: Search = new Search();
  viewSearch: boolean = false;
  requerid = {
    rubro:false,
    fecha:false,
    materia:false,
    vigencia:false
  }

  ngOnInit(): void {
    const date = this.datePipe.transform(new Date(), 'dd-MM-yyyy');
    this.catalogoService.getCatalogosAcuerdos(date).subscribe(
      (data)=>{
        this.materiaRegulacionArray = data.materiaRegulacion
        this.vigenciaArray = data.vigencia
      },
      (error)=>{
      }
    )

    if(this.saveFilters.filters.module == 'acuerdos'){
      const data = this.saveFilters.filters.filter.split(',')
      data.forEach(keys =>{
        const k = keys.split(':')
        this.busqueda[k[0]] = k[1]
      })
      this.viewSearch = true
      this.construyeFiltro()
    } else {
      this.saveFilters.filters = {} as SaveFilters
      this.realizaBusqueda()
    }
  
  }

  onChangeSize({ value }: any) {
    this.acuerdoAllMod.size = value;
    this.allAcuerdos(this.acuerdoAllMod.number + 1, this.acuerdoAllMod.size, this.filtrado);
  }

  public onChangePage(page: number) {
    this.allAcuerdos(page, this.acuerdoAllMod.size, this.filtrado);
  }

  private allAcuerdos(page: number, size: number, filtros?: string) {
    this.acuerdoService.getAllAcuerdos(page, size, filtros).subscribe(
      (data) => {
        if (data.content.length > 0) {
          this.acuerdoAllMod = data;
          this.acuerdos = data.content;
          this.totalAcuerdos = filtros ? data.content.length : data.totalElements;
          this.totalPages = data.totalPages;
        } else {
          this.totalAcuerdos = 0;
          this.totalPages = 0;
          this.acuerdos = []
        }
        document.querySelector('.p-datatable-scrollable-body').scroll(0, 0);
      },
      (error) => {}
    );
  }



  public calculateSizePage(){
    if(this.acuerdoAllMod.size*(1+this.acuerdoAllMod.number)>=this.acuerdoAllMod.totalElements){
      return this.acuerdoAllMod.totalElements;
    }else{
      return (this.acuerdoAllMod.size*(1+this.acuerdoAllMod.number));
    }
  }

  construyeFiltro(): void {

    this.filtrado = '';

    this.filtrado += this.busqueda.rubro
    ? 'rubro:' + this.busqueda.rubro+ ','
    : '';

    this.filtrado += this.busqueda.fecha ? 'fechaAprobacion:' +
    this.busqueda.fecha+ ','
    : '';

    this.filtrado += this.busqueda.materiaRegulacion
    ? 'materiaRegulacion:' + this.busqueda.materiaRegulacion+ ','
    : '';

    this.filtrado += this.busqueda.vigencia
    ? 'vigencia:' + this.busqueda.vigencia+ ','
    : '';

    if(this.filtrado){
      this.viewSearch = true
      this.saveFilters.filters.module = 'acuerdos'
      this.saveFilters.filters.filter = this.filtrado
      
      this.realizaBusqueda(1, this.rows, this.filtrado);
      for(let x in this.requerid){
        this.requerid[x]=false
      };
    }else{
      for(let x in this.requerid){
        this.requerid[x]=this.busqueda[x]?false:true
      };
    }



  }

  restauraTabla():void{
    this.filtrado = ""
    this.viewSearch = false
    this.busqueda = {} as Search
    this.requerid = {
      rubro:false,
      fecha:false,
      materia:false,
      vigencia:false
    }
    this.saveFilters.filters = {} as SaveFilters;
    this.realizaBusqueda()
  }

  realizaBusqueda(
    page: number = 1,
    rows: number = 10,
    filtro: string = ''
  ): void {
    this.acuerdoService.getAllAcuerdos(page, rows, filtro).subscribe(
      (data) => {
        if (data.content.length > 0) {
          this.acuerdoAllMod = data;
          this.acuerdos = data.content;
          this.totalAcuerdos = filtro ? data.content.length : data.totalElements;
          this.totalPages = data.totalPages;
        } else {
          this.totalAcuerdos = 0;
          this.totalPages = 0;
          this.acuerdos = []
          this.acuerdoAllMod = data
        }
      },
      (error) => {}
    );
  }

  confirm(event: Event, acuerdo: Acuerdo) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar el documento?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        let indice: number = this.acuerdos.indexOf(acuerdo);
        this.acuerdoService.deleteAcuerdoByID(acuerdo.id).subscribe(
          (res) => {
            if (indice != -1) this.acuerdos.splice(indice, 1);
          },
          (error) => {
          }
        );
      },
      reject: () => {

      },
    });
  }

}
