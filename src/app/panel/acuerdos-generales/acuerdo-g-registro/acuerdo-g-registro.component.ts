import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { AcuerdosApiService } from 'src/app/services/acuerdos-api.service';
import { Acuerdo } from 'src/app/models/acuerdosModel';
import { ConverStringArray } from '../../../helper/converArrayObject';
import { ScrollTop } from 'src/app/helper/scrollTop';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-acuerdo-g-registro',
  templateUrl: './acuerdo-g-registro.component.html',
  styleUrls: ['./acuerdo-g-registro.component.scss'],
  providers: [ConverStringArray, ConfirmationService],
})
export class AcuerdoGRegistroComponent implements OnInit {
  configModal: { open: boolean };
  materiaEdit: any;
  configDropdown: IDropdownSettings = {
    singleSelection: false,
    defaultOpen: false,
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: 'Buscar',
  };
  catalogos: any = {};
  campos: number = 0;
  scroll: ScrollTop = new ScrollTop();
  acuerdoModel: Acuerdo = new Acuerdo();
  temasEspecificosRegulacion: string[] = [];
  showMinistros: boolean = true;
  requerid = {
    rubro: false,
    fechaAprobacion: false,
    ambitoIncide: false,
    vigencia: false,
    fechaPublicacionDOF: false,
    clasificacionMateriaRegulacion: false,
    organoEmisor: false,
    ministroPresidente: false,
    votacion: false,
    temasEspecificosRegulacion: false,
  };
  votacionFavor: string[] = [];
  votacionContra: string[] = [];

  constructor(
    private catalogoService: CatalogosService,
    private acuerdoService: AcuerdosApiService,
    private converString: ConverStringArray,
    private router: Router,
    private datePipe: DatePipe,
    private confirmationService: ConfirmationService
  ) {
    this.scroll.scrollTo();
  }

  ngOnInit(): void {
    this.catalogoService.getCatalogosAcuerdos().subscribe(
      (data) => {
        this.catalogos = data;
        this.catalogos['materiaRegulacion'] = this.converString.proccessArray(
          this.catalogos['materiaRegulacion']
        );
        this.catalogoService.getCatalogosByColeccionCampo('', 'bd_sga_acuerdos_generales', 'temasEspecificosRegulacion').subscribe(
          (data) => {
            this.catalogos.temasEspecificosRegulacion = data;
            this.catalogos['temasEspecificosRegulacion'] = this.converString.proccessArrayColeccionCampo(
              this.catalogos['temasEspecificosRegulacion']
            );
          },
          (error) => {}
        );
      },
      (error) => {}
    );
  }

  crearAcuerdo() {
    this.campos = 0;
    for (const property in this.requerid) {
      this.requerid[property] = !this.acuerdoModel[property] ? true : false;
      if (!this.requerid[property]) this.campos += 1;
    }
    console.log(this.campos);

    if (this.campos > 0)
      this.acuerdoService.postAcuerdo(this.acuerdoModel).subscribe((res) => {
        if (res['statusCode'] == 200) {
          this.router
            .navigateByUrl('/', { skipLocationChange: true })
            .then((nav) => this.router.navigate(['/acuerdos-generales/']));
        }
      });
  }

  selectMinistro(event: any, tipo: string) {
    this.acuerdoModel.ministrosAusentes.includes(event)
      ? this.acuerdoModel.ministrosAusentes.splice(
          this.acuerdoModel.ministrosAusentes.indexOf(event),
          1
        )
      : null;
    switch (tipo) {
      case 'favor':
        this.votacionContra.includes(event)
          ? this.votacionFavor.push(
              this.votacionContra
                .splice(this.votacionContra.indexOf(event), 1)
                .toString()
            )
          : null;
        this.acuerdoModel.votacionAcuerdoEnContra = [];
        break;
      case 'contra':
        this.votacionFavor.includes(event)
          ? this.votacionContra.push(
              this.votacionFavor
                .splice(this.votacionFavor.indexOf(event), 1)
                .toString()
            )
          : null;
        this.acuerdoModel.votacionAcuerdoFavor = [];

        break;
      default:
    }

    setTimeout(() => {
      this.acuerdoModel.votacionAcuerdoFavor = this.votacionFavor;
      this.acuerdoModel.votacionAcuerdoEnContra = this.votacionContra;
    }, 100);
  }

  confirm(index: number) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar el documento?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.temasEspecificosRegulacion.splice(index, 1);
        this.acuerdoModel.clasificacionMateriaRegulacion = this.temasEspecificosRegulacion;
      },
    });
  }

  displayModal() {
    const config = {
      catalogo: this.catalogos['temasEspecificosRegulacion'],
      open: true,
    };
    this.configModal = config;
  }

  getModal(response: { tema: string; open: boolean }) {
    this.temasEspecificosRegulacion = this.acuerdoModel.temasEspecificosRegulacion
      ? this.acuerdoModel.temasEspecificosRegulacion
      : [];
    this.temasEspecificosRegulacion.push(response.tema);
    this.acuerdoModel.temasEspecificosRegulacion = this.temasEspecificosRegulacion;
  }

  getModalUpdateTema(response: { index: number; tema: string; open: boolean }) {
    this.temasEspecificosRegulacion[response.index] = response.tema;
    this.acuerdoModel.clasificacionMateriaRegulacion = this.temasEspecificosRegulacion;
  }

  edicionMateria(materia: string, index: number) {
    this.materiaEdit = {
      materia: materia,
      index: index,
      catalogo: this.catalogos['temasEspecificosRegulacion'],
      open: true,
    };
  }


  getMinistrosByDate(date:string){
    this.catalogoService.getCatalogosAcuerdos(date).subscribe(
      (data) => {
        this.catalogos.ministros = this.converString.proccessArray(data['ministros']);
        this.votacionFavor = data['ministros'] =
          this.converString.proccessArray(data['ministros']);
        this.acuerdoModel.votacionAcuerdoFavor = this.votacionFavor;
      },
      (error) => {}
    );
  }

  
}
