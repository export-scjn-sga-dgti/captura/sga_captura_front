import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BsentenciavotacionComponent } from './bsentenciavotacion.component';

describe('BsentenciavotacionComponent', () => {
  let component: BsentenciavotacionComponent;
  let fixture: ComponentFixture<BsentenciavotacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BsentenciavotacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BsentenciavotacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
