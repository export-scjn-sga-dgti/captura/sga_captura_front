import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BvotosComponent } from './bvotos.component';

describe('BvotosComponent', () => {
  let component: BvotosComponent;
  let fixture: ComponentFixture<BvotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BvotosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BvotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
