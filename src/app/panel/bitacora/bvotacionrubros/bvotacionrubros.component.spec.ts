import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BvotacionrubrosComponent } from './bvotacionrubros.component';

describe('BvotacionrubrosComponent', () => {
  let component: BvotacionrubrosComponent;
  let fixture: ComponentFixture<BvotacionrubrosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BvotacionrubrosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BvotacionrubrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
