import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasuntosComponent } from './basuntos.component';

describe('BasuntosComponent', () => {
  let component: BasuntosComponent;
  let fixture: ComponentFixture<BasuntosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasuntosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasuntosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
