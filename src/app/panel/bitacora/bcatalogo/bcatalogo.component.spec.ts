import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BcatalogoComponent } from './bcatalogo.component';

describe('BcatalogoComponent', () => {
  let component: BcatalogoComponent;
  let fixture: ComponentFixture<BcatalogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BcatalogoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BcatalogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
