import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Bitacora } from 'src/app/models/btaquigraficas';
import { BitacoraService } from 'src/app/services/bitacora.service';

@Component({
  selector: 'app-bcatalogo',
  templateUrl: './bcatalogo.component.html',
  styleUrls: ['./bcatalogo.component.scss'],
})
export class BcatalogoComponent implements OnInit {
  info: Bitacora = {} as Bitacora;

  constructor(
    private param: ActivatedRoute,
    private bitacoraService: BitacoraService
  ) {
    this.param.paramMap.subscribe((data) => {
      this.getBitacora(data.get('id'));
    });
  }
  ngOnInit(): void {}

  getBitacora(id: string) {
    this.bitacoraService.getBitacoraById(id).subscribe((data: any) => {
      this.info = data;
    });
  }
}
