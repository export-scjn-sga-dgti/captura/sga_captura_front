import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BacuerdosComponent } from './bacuerdos.component';

describe('BacuerdosComponent', () => {
  let component: BacuerdosComponent;
  let fixture: ComponentFixture<BacuerdosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BacuerdosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BacuerdosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
