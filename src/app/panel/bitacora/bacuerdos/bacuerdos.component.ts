import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Bitacora } from 'src/app/models/btaquigraficas';
import { BitacoraService } from 'src/app/services/bitacora.service';

@Component({
  selector: 'app-bacuerdos',
  templateUrl: './bacuerdos.component.html',
  styleUrls: ['./bacuerdos.component.scss'],
})
export class BacuerdosComponent implements OnInit {
  info: Bitacora = {} as Bitacora;

  constructor(
    private param: ActivatedRoute,
    private bitacoraService: BitacoraService
  ) {
    this.param.paramMap.subscribe((data) => {
      this.getBitacora(data.get('id'));
    });
  }
  ngOnInit(): void {}

  getBitacora(id: string) {
    this.bitacoraService.getBitacoraById(id).subscribe((data: Bitacora) => {
      this.info = data;
    });
  }
}
