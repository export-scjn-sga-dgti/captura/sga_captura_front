import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BsentenciasComponent } from './bsentencias.component';

describe('BsentenciasComponent', () => {
  let component: BsentenciasComponent;
  let fixture: ComponentFixture<BsentenciasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BsentenciasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BsentenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
