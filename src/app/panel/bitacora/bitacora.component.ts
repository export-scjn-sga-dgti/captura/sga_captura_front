import { Component, OnInit } from '@angular/core';
import { BitacoraModel } from 'src/app/models/bitacora';
import { FilterBitacora } from 'src/app/models/filtersBitacora';
import { SaveFilters } from 'src/app/models/save-filters';
import { BitacoraService } from 'src/app/services/bitacora.service';
import { SaveFiltersService } from 'src/app/services/save-filters.service';

@Component({
  selector: 'app-bitacora',
  templateUrl: './bitacora.component.html',
  styleUrls: ['./bitacora.component.scss'],
})
export class BitacoraComponent implements OnInit {
  windows: any[] = [
    { name: 'Sentencias' },
    { name: 'Catalogos' },
    { name: 'Votos' },
    { name: 'Diversa Normativa' },
    { name: 'Asuntos Abordados' },
    { name: 'Versiones Taquigraficas' },
    { name: 'Acuerdos' },
  ];
  size = 10;
  filtros: string = '';
  bitacora: BitacoraModel = new BitacoraModel();
  filters: FilterBitacora = new FilterBitacora();
  isFilter: boolean = false;
  constructor(private bitacoraService: BitacoraService,
    private saveFilters: SaveFiltersService) {}

  ngOnInit(): void {
    if(this.saveFilters.filters.module == 'bitacora'){
      const data = this.saveFilters.filters.filter.split(',')
      data.forEach(keys =>{
        if(keys != ''){
          const k = keys.split(':')
          if(k[0].includes('.')){
            this.filters[k[0].split('.')[0]][k[0].split('.')[1]] = k[1]
          }else {
            this.filters[k[0]] = k[1]
          }          
        }        
      })
      this.isFilter = true
      this.searchFilter()
    } else {
      this.saveFilters.filters = {} as SaveFilters
      this.getAllBitacora();  
    }
    
  }

  getAllBitacora(page: number = 1, size: number = 10, filtros?: string) {
    this.bitacoraService
      .getAllBitacora(page, size, filtros)
      .subscribe((data) => {
        this.bitacora = data;
      });
  }

  onChangePage(page: number) {
    this.getAllBitacora(page, this.size, this.filtros);
  }

  onChangeSize({ value }) {
    this.getAllBitacora(this.bitacora.number + 1, value, this.filtros);
  }

  searchFilter() {
    this.filtros = '';
    if (this.filters.date || this.filters.commitMetaData.author || this.filters.seccion) {
      this.filtros += this.filters.date
        ? `commitMetaData.commitDate:${this.filters.date.split('/').reverse().join('-')},`
        : '';
      this.filtros += this.filters.commitMetaData.author
        ? `commitMetaData.author:${this.filters.commitMetaData.author},`
        : '';
      this.filtros += this.filters.seccion
        ? `seccion:${this.filters.seccion},`
        : '';
      this.isFilter = true;
      this.saveFilters.filters.module = 'bitacora'
      this.saveFilters.filters.filter = this.filtros
      this.getAllBitacora(1, this.size, this.filtros);
    }
  }

  clearFilter() {
    this.filtros = '';
    this.isFilter = false;
    this.filters = new FilterBitacora();
    this.saveFilters.filters = {} as SaveFilters
    this.getAllBitacora(1, this.size);
  }
}
