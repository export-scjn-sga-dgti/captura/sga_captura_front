import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BitacoraRoutingModule } from './bitacora-routing.module';
import { BitacoraComponent } from './bitacora.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { BtaquigraficaComponent } from './btaquigrafica/btaquigrafica.component';
import { BsentenciasComponent } from './bsentencias/bsentencias.component';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { BcatalogoComponent } from './bcatalogo/bcatalogo.component';
import { FieldsetModule } from 'primeng/fieldset';
import { BasuntosComponent } from './basuntos/basuntos.component';
import { BvotosComponent } from './bvotos/bvotos.component';
import { BacuerdosComponent } from './bacuerdos/bacuerdos.component';
import { BnormativaComponent } from './bnormativa/bnormativa.component';
import { BsentenciavotacionComponent } from './bsentenciavotacion/bsentenciavotacion.component';
import { BvotacionrubrosComponent } from './bvotacionrubros/bvotacionrubros.component';

@NgModule({
  declarations: [
    BitacoraComponent,
    BtaquigraficaComponent,
    BsentenciasComponent,
    BcatalogoComponent,
    BasuntosComponent,
    BvotosComponent,
    BacuerdosComponent,
    BnormativaComponent,
    BsentenciavotacionComponent,
    BvotacionrubrosComponent
  ],
  imports: [
    CommonModule,
    BitacoraRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    NgbPaginationModule,
    BreadcrumbModule,
    PipesModule,
    FieldsetModule,
  ],
})
export class BitacoraModule {}
