import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BnormativaComponent } from './bnormativa.component';

describe('BnormativaComponent', () => {
  let component: BnormativaComponent;
  let fixture: ComponentFixture<BnormativaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BnormativaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BnormativaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
