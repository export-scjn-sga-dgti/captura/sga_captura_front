import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtaquigraficaComponent } from './btaquigrafica.component';

describe('BtaquigraficaComponent', () => {
  let component: BtaquigraficaComponent;
  let fixture: ComponentFixture<BtaquigraficaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtaquigraficaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtaquigraficaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
