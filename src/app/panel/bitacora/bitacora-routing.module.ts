import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { BacuerdosComponent } from './bacuerdos/bacuerdos.component';
import { BasuntosComponent } from './basuntos/basuntos.component';
import { BcatalogoComponent } from './bcatalogo/bcatalogo.component';
import { BitacoraComponent } from './bitacora.component';
import { BnormativaComponent } from './bnormativa/bnormativa.component';
import { BsentenciasComponent } from './bsentencias/bsentencias.component';
import { BsentenciavotacionComponent } from './bsentenciavotacion/bsentenciavotacion.component';
import { BtaquigraficaComponent } from './btaquigrafica/btaquigrafica.component';
import { BvotacionrubrosComponent } from './bvotacionrubros/bvotacionrubros.component';
import { BvotosComponent } from './bvotos/bvotos.component';

const routes: Routes = [
  { path: '', component: BitacoraComponent },
  { path: 'Versiones Taquigraficas/:id', component: BtaquigraficaComponent },
  { path: 'Sentencias/:id', component: BsentenciasComponent },
  { path: 'Catalogos/:id', component: BcatalogoComponent },
  { path: 'Asuntos Abordados/:id', component: BasuntosComponent },
  { path: 'Votos/:id', component: BvotosComponent },
  { path: 'Acuerdos/:id', component: BacuerdosComponent },
  { path: 'Diversa Normativa/:id', component: BnormativaComponent },
  { path: 'Sentencias - Votacion/:id', component: BsentenciavotacionComponent},
  { path: 'Sentencias - Votacion - Rubros/:id', component: BvotacionrubrosComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BitacoraRoutingModule {}
