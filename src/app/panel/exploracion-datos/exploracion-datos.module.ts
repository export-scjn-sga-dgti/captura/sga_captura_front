import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExploracionDatosRoutingModule } from './exploracion-datos-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ExploracionDatosRoutingModule
  ]
})
export class ExploracionDatosModule { }
