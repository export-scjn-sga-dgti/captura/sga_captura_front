import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploracionDatosComponent } from './exploracion-datos.component';

describe('ExploracionDatosComponent', () => {
  let component: ExploracionDatosComponent;
  let fixture: ComponentFixture<ExploracionDatosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExploracionDatosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploracionDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
