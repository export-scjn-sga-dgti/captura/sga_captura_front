import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FilterSeguimiento, Seguimiento } from 'src/app/models/seguimiento';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { SeguimientoService } from 'src/app/services/seguimiento.service';

@Component({
  selector: 'app-exploracion-datos',
  templateUrl: './exploracion-datos.component.html',
  styleUrls: ['./exploracion-datos.component.scss']
})
export class ExploracionDatosComponent implements OnInit {

  constructor(private seguimientoService: SeguimientoService,
    private catalagosService: CatalogosService) { }

  @ViewChild('xls', { static: true }) private file: ElementRef<HTMLLinkElement>
  size: number = 10;
  seguimiento: Seguimiento = {} as Seguimiento;
  parrafos: string[] = [];
  actived: boolean = false;
  filtros: string = ""
  filter: FilterSeguimiento = {} as FilterSeguimiento;
  isFilter: boolean = false;
  catalogos: any = {}
  link: string
  name: string

  ngOnInit(): void {
    this.allSeguimiento();

    this.catalagosService.getCatalogosSeguimiento().subscribe(data => {
      this.catalogos = data
    })

  }

  allSeguimiento(page: number = 1, size: number = 10, filtros?: string) {
    this.seguimientoService.getAllSeguimiento(page, size, filtros).subscribe(data => {
      this.seguimiento = data
    })
  }

  onChangePage(page: number) {
    this.allSeguimiento(page, this.size, this.filtros)

  }

  onChangeSize({ value }) {
    this.allSeguimiento(this.seguimiento.number + 1, value, this.filtros);
  }


  searchFilter() {
    if (this.filter.anio || this.filter.asuntoAbordado || this.filter.numeroExpediente || this.filter.secretario) {
      this.filtros = ""
      this.filtros += this.filter.asuntoAbordado ? `asuntoAbordado:${this.filter.asuntoAbordado},` : ''
      this.filtros += this.filter.numeroExpediente ? `numeroExpediente:${this.filter.numeroExpediente},` : ''
      this.filtros += this.filter.secretario ? `secretario:${this.filter.secretario},` : ''
      this.filtros += this.filter.anio ? `anioResolucion:${this.filter.anio}` : ''

      this.seguimientoService.getAllSeguimiento(1, this.size, this.filtros).subscribe(data => (this.seguimiento = data, this.isFilter = true))

    }
  }

  descargaReporte() {
    this.seguimientoService.getReporteExcel().subscribe(data => {
      this.file.nativeElement.href = URL.createObjectURL(new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }));
      const date = new Date();
      let name_date: string = ""
      name_date += date.getFullYear();
      name_date += date.getMonth() >= 9 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)
      name_date += date.getDate() >= 9 ? date.getDate() : '0' + date.getDate()
      this.file.nativeElement['download'] = `ReporteAsuntosTemas_${name_date}` 
      this.file.nativeElement.click();
    });
  }

  clearFilter() {
    this.isFilter = false
    this.allSeguimiento()
    this.filtros = ""
    this.filter = {} as FilterSeguimiento
  }

  public calculateSizePage() {
    if (this.seguimiento.size * (1 + this.seguimiento.number) >= this.seguimiento.totalElements) {
      return this.seguimiento.totalElements;
    } else {
      return (this.seguimiento.size * (1 + this.seguimiento.number));
    }
  }

}
