import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExploracionDatosComponent } from './exploracion-datos.component';

const routes: Routes = [{ path: '', component: ExploracionDatosComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExploracionDatosRoutingModule {}
