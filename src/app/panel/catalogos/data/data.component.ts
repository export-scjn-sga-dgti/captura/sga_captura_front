import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { Catalogo } from 'src/app/models/catalogos';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss'],
})
export class DataComponent implements OnInit {
  catalogos: Catalogo[] = [];
  viewCreate: boolean;
  catalogo: Catalogo = new Catalogo();
  name: string;
  editView: boolean;
  isMinistros: boolean;
  constructor(
    private catalogoService: CatalogosService,
    private param: ActivatedRoute,
    private toast: ToastService,
    private confirmationService: ConfirmationService
  ) {
    this.param.paramMap.subscribe((data) => {
      this.name = data.get('id');
      this.isMinistros = this.name == 'ministros' ? true : false;
      this.getCatalogo(this.name);
    });
  }

  ngOnInit(): void {}

  getCatalogo(id: string) {
    this.catalogoService.getCatalogoByName(id).subscribe((data) => {
      this.catalogos = data;
    });
  }

  openNew() {
    this.catalogo = new Catalogo();
    this.viewCreate = true;
  }

  createCatalogo() {
    if (this.catalogo.nombre.trim()) {
      this.catalogo.catalogo = this.name;
      this.catalogoService.postCatalogo(this.catalogo).subscribe((res) => {
        this.toast.showToast('success', 'Catálogo', 'Se ha creado con éxito');
        this.getCatalogo(this.name);
        this.viewCreate = false;
      });
    }
  }

  updateCatalogo() {
    if (this.catalogo.nombre.trim()) {
      this.catalogo.catalogo = this.name;
      this.catalogoService.updateCatalogo(this.catalogo).subscribe((res) => {
        this.toast.showToast(
          'success',
          'Catálogo',
          'Se ha actualizado con éxito'
        );
        this.editView = false;
        this.getCatalogo(this.name);
      });
    }
  }

  deleteCatalogo(id: string, event: Event) {
    this.confirmationService.confirm({
      target: event.target,
      message: '¿Estás seguro de eliminar el catálogo permanentemente?',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: 'btn-danger',
      accept: () => {
        this.catalogoService.deleteCatalogo(id).subscribe((resp) => {
          if (resp) {
            this.toast.showToast(
              'success',
              'Catálogo',
              'Se ha eliminado exitosamente'
            );
            this.getCatalogo(this.name);
          }
        });
      },
      reject: () => {
        //reject action
      },
    });
  }
}
