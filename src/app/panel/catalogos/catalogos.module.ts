import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogosRoutingModule } from './catalogos-routing.module';
import { CatalogosComponent } from './catalogos.component';
import { DataComponent } from './data/data.component';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmationService } from 'primeng/api';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  declarations: [CatalogosComponent, DataComponent],
  imports: [
    CommonModule,
    CatalogosRoutingModule,
    TableModule,
    TooltipModule,
    ButtonModule,
    InputTextModule,
    DialogModule,
    ReactiveFormsModule,
    FormsModule,
    ConfirmPopupModule,
    CalendarModule,
  ],
  providers: [ConfirmationService],
})
export class CatalogosModule {}
