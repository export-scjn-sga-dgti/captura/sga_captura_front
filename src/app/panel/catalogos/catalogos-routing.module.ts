import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogosComponent } from './catalogos.component';
import { DataComponent } from './data/data.component';

const routes: Routes = [
  { path: '', component: CatalogosComponent },
  { path: ':id', component: DataComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogosRoutingModule {}
