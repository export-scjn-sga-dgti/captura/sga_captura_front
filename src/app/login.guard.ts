import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
  CanActivate,
} from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { KeycloakService } from './services/keycloak.service';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {
  constructor(private keycloak: KeycloakService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    if (environment.activeKeycloak) {
      if (this.keycloak.getActivate()) {
        const username = this.keycloak.tokenParsed().preferred_username;
        const usernameInitials =
          this.keycloak.tokenParsed().given_name.charAt(0) +
          this.keycloak.tokenParsed().family_name.charAt(0);
        window.localStorage.setItem('username', username);
        window.localStorage.setItem('usernameInitials', usernameInitials);
        return true;
      } else {
        window.location.href = this.keycloak.login();
      }
    } else {
      return true;
    }
    return true;
  }
}
