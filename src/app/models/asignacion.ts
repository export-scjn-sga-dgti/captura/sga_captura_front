

export interface Permisos {
    ver: boolean;
    crear: boolean;
    editar: boolean;
    eliminar: boolean;
    acceso: boolean;
    superadmin: boolean;
    asignacionAsuntos: boolean;
}

export interface CatalogosRestricted {
    campo: string;
    valor: string[];
}

export interface Asunto {
    tipoAsunto: string;
    numExpediente: string;
}

export interface Modulo {
    modulo: string;
    permisos: Permisos;
    camposRestricted: string[];
    catalogosRestricted: CatalogosRestricted[];
    asuntos: Asunto[];
}

export interface Content {
    id: string;
    nombre: string;
    correo: string;
    modulos: Modulo[];
    fh_creacion?: any;
    fh_modificacion?: Date;
}

export interface Sort {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
}

export interface Pageable {
    sort: Sort;
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    unpaged: boolean;
}

export interface Sort2 {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
}

export interface AsignacionAsuntos {
    content: Content[];
    pageable: Pageable;
    totalElements: number;
    totalPages: number;
    last: boolean;
    size: number;
    number: number;
    sort: Sort2;
    numberOfElements: number;
    first: boolean;
    empty: boolean;
}

export class DataAsuntos{
    taqui: string[] = []
    sentencia: string[] = []
}