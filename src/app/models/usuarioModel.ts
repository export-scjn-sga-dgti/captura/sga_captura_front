export class Usuario {
	id: string;
	nombre: string;
	correo: string;
	modulos: Modulos[] = [];
	fh_creacion: string;
	fh_modificacion: string;
}

export class Modulos {
	modulo: string;
	permisos: Permisos;
	camposRestricted: string[] = [];
	catalogosRestricted: CatalogosRestricted[] = [];
	asuntos: AsuntoUsuario[] = [];
}

export class Permisos {
	ver: boolean = false;
	crear: boolean = false;
	editar: boolean = false;
	eliminar: boolean = false;
	acceso: boolean = false;
	superadmin: boolean = false;
	asignacionAsuntos: boolean = false;
}

export class CatalogosRestricted {
	campo: string;
	valor: string[] = [];
}

export class AsuntoUsuario {
	tipoAsunto: string;
	numExpediente: string;
}
