export class Catalogo {
  id?: string;
  catalogo: string;
  nombre: string;
  fechaInicio?: string;
  fechaFin?: string;
}
