export class RubrosEfectoSentencia {
  id?: string;
  tipoRubro: string = '';
  rubrosTematicos: string[] = [];
  textoVotacion: string = '';
  ministros: string[] = [];
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  consideracionesAprobadas: boolean = false;
  sentido? : string;
  tipoSentencia? : string[];
}

export class RubrosExtencionInvalidez {
  tipoRubro: string = '';
  rubrosTematicos: string[] = [];
  textoVotacion: string = '';
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  consideracionesAprobadas: boolean = false;
}

export class Votacion {
  idSentencia: string = '';
  tipoVotacion: string = 'votacionDerechosHumanosCuyaViolacionAnalizaSentencia';
  actosImpugnados: string[] = [];
  derechosHumanos: string[] = [];
  rubrosTematicosPrincipal: string[] = [];
  textoVotacion: string = '';
  sentidoResolucion: string = '';
  aprobadasConcideraciones: boolean = false;
  metodologiaAnalisisConstitucionalidad: string = '';
  tipoSentencia: string[] = [];
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  ministros: string[] = [];
  rubrosEfectoSentencia: RubrosEfectoSentencia[] = [];
  rubrosExtencionInvalidez: RubrosEfectoSentencia[] = [];
}

export class VotacionDerechosHumanos {
  idSentencia?: string;
  votacion: Votacion = new Votacion();
}
