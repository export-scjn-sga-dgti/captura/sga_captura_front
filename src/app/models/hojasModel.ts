export class DataHojas {
  tipoAsunto: string;
  consecutivo: number;
  anio: number;
}

export class HojasVotacion {
  anio: number;
  tipoAsunto: string;
  origen: number;
  consecutivo: number;
  documentoID: string;
  asuntoID: number;
}
