export class Procesamiento {
  text: string;
  decision: string;
  numVotacionDecision: number;
  ministros: string[];
  ministrosAFavor: string[];
  ministrosEnContra: string[];
}

export class TextoProcesamiento {
  text: string;
  fechaResolucion: string;
}
