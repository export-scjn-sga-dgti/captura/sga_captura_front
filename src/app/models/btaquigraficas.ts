import { Materia, Normativa } from './normativaModel';

export interface State {
  id?: string;
  fechaSesion?: string;
  duracionSesion?: string;
  numeroMinistrasMinistrosParticiparonEnSesion?: number;
  nombreMinistrasMinistrosPresentesEnSesion?: string[];
  nombreMinistrasMinistrosAusentesEnSesion?: string[];
  hasPresidenteDecano?: boolean;
  nombrePresidenteDecano?: string;
  porcentaje?: number;
  catalogo?: string;
  nombre?: string;
  acumulada?: false;
  acumulados?: [];
  ca?: false;
  inUse?: false;
  statusCode?: number;
  message?: string;
  idVersionTaqui?: string;
  asunto?: Asunto;
  vt_asuntos?: VtAsuntos;
  derechosHumanosCuyaViolacionPlantea?: string[];
  derechosHumanosCuyaViolacionPlanteaByDerechosHumanos?: string[];
  diasTranscurridosEntreDictadoSentenciayFirmaEngrose?: string;
  fechaFirmaEngrose?: string;
  fechaResolucion?: string;
  fechaSentencia?: string;
  momentoSurteEfectoSentencia?: string;
  numeroExpediente?: string;
  numeroVotacionesRealizadas?: number;
  numeroVotacionesRegistradas?: number;
  organoRadicacion?: string;
  origen?: string;
  ponente?: string;
  porcentajeCaptura?: number;
  tipoAsunto?: string;
  tipoPersonaJuridicoColectiva?: string;
  tipoPersonaPromovente?: string;
  tipoViciosProcesoLegislativo?: any;
  tipoViolacionInvacionEsferas?: any;
  tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia?: any;
  tipoViolacionInvacionPoderes?: any;
  tipoViolacionInvacionPoderesAnalizadoenSentencia?: any;
  tipoViolacionPlanteadaenDemanda?: string;
  totalVotacionCausasImprocedenciaySobreseimientoAnalizadas?: number;
  totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia?: number;
  totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia?: number;
  totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia?: number;
  totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia?: number;
  tramiteEngrose?: string;
  violacionOrganicaAnalizadaenSentencia?: string[];
  violacionOrganicaPlanteada?: any;
  votacionCausasImprocedenciaySobreseimientoAnalizadas?: any[];
  votacionDerechosHumanosCuyaViolacionAnalizaSentencia?: any[];
  votacionTipoVicioProcesoLegislativoAnalizadoenSentencia?: any[];
  votacionTipoViolacionInvacionEsferasAnalizadoenSentencia?: any[];
  votacionTipoViolacionInvacionPoderesAnalizadoenSentencia?: any[];
  metodologiaAnalisis?: string;
  ministro?: string[];
  numExpediente?: string;
  temaProcesalSobreElVoto?: string;
  temaSustantivoSobreElVoto?: string;
  tipoTema?: string;
  tipoTemaProcesalSobreElVoto?: string;
  tipoTemaSustantivoSobreElVoto?: string;
  tipoVoto?: string;
  fechaAprobacion?: string;
  numero?: string;
  tipoInstrumento?: string;
  diversaNormativa?: Normativa;
  materias?: Materia[];
  ambitoIncide?: string;
  clasificacionMateriaRegulacion?: string[];
  fechaPublicacionDOF?: string;
  ministroPresidente?: string;
  ministrosAusentes?: string[];
  organoEmisor?: string;
  rubro?: string;
  temasEspecificosRegulacion?: string;
  vigencia?: string;
  votacion?: any;
  votacionAcuerdoEnContra?: string[];
  votacionAcuerdoFavor?: string[];
  actosImpugnados?: string[];
  aprobadasConcideraciones?: boolean;
  articulosImpugnados?: string[];
  derechosHumanos?: string[];
  metodologiaAnalisisConstitucionalidad?: string;
  ministros?: string[];
  numeroVotos?: number;
  textoVotacion?: string
  analisisProcedencia?: string[];
  rubrosEfectoSentencia?: any;
  rubrosExtencionInvalidez?: any[];
  rubrosTematicosPrincipal?: string[];
  sentidoResolucion?: string;
  tipoSentencia?: string[];
  rubrosTematicos?: string[]
  tipoVotacion?: string;
  votacionEnContra?: string[];
  votacionFavor?: string[];

}

export interface CommitMetaData {
  author: string;
  commitDate: string;
}

export interface Bitacora {
  id: string;
  beginState?: any;
  state: State;
  commitMetaData: CommitMetaData;
  type: string;
  seccion: string;
  resumen?: any;
}

export interface Asunto {
  id: string;
  asuntoAbordado: string;
  numeroExpediente: string;
  acumulados: any[];
  acumulada: boolean;
  ca: boolean;
}

export interface VtAsuntos {
  id: string;
  idVtaquigrafica: string;
  idAsunto: string;
  ponente: string;
  temasProcesales: string[];
  temasFondo: string[];
  nombresMinistrosParticiparonEnAsunto: any[];
  numeroMinistrosParticiparonEnAsunto: NumeroMinistrosParticiparonEnAsunto[];
  acumulados: any[];
}

export interface NumeroMinistrosParticiparonEnAsunto {
  nombre: string;
  cantidad: number;
}
