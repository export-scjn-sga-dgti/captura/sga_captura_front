export class Voto {
    id: string;
    ministro: string[] = null;
    tipoTemaProcesalSobreElVoto: string;
    temaProcesalSobreElVoto: string;
    temaSustantivoSobreElVoto: string;
    tipoTemaSustantivoSobreElVoto: string;
    tipoTema: string;
    tipoAsunto: string;
    numExpediente: string;
    porcentaje: number;
    fechaResolucion: Date = null;
    metodologiaAnalisis: string;
    acumulados: string[];
    isAcumulada: boolean;
    isCA: boolean;
    tipoVoto: string[] = null;
    causaImprocedencia: string[] = null;
    derechosHumanos: string[] = null;
    sentidoResolucion: string[] = null;
    tipoEfectos: string[] = null;
    analisisConstitucionlidad: string[] = null;
    violacionOrganicaAnalizada: string[] = null;
    violacionInvacionPoderesAnalizado: string[] = null;
    violacionInvacionEsferasAnalizada: string[] = null;
    vicioProcesoLegislativo: string[] = null;
    violacionInvacionEsferas: string[] = null;
    violacionInvacionEsferasFederal: string[] = null;
    violacionInvacionEsferasMunicipal: string[] = null;

}

export class AllVotoModel {
    content: Voto[] = [];
    empty: boolean;
    first: boolean;
    last: boolean;
    number:number;
    numberOfElements:number;
    pageable:Pageable;
    size:number;
    sort:Sort2;
    totalElements:number;
    totalPages:number;

}

export class Sort {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
}

export class Pageable {
    sort: Sort;
    offset: number;
    pageSize: number;
    pageNumber: number;
    unpaged: boolean;
    paged: boolean;
}

export class Sort2 {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
}

export class Search{

    tipoVoto: string;
    numExpediente: string;
    tipoAsunto: string;

}
