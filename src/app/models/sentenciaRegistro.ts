export class RubrosTematico {
  id?: string;
  tipoRubro: string = '';
  rubrosTematicos: string[] = [];
  textoVotacion: string = '';
  analisisProcedencia: string[] = [];
}

export class VotacionCausasImprocedenciaySobreseimientoAnalizada {
  id?: string;
  tipoVotacion: string = 'votacionCausasImprocedenciaySobreseimientoAnalizadas';
  rubrosTematicos: RubrosTematico[];
  votacionFavor: string[];
  votacionEnContra: string[];
  aprobadasConcideraciones: boolean;
  sentidoResolucion: string;
  textoVotacion: string;
  actosImpugnados: string[];
  articulosImpugnados: any[];
}

export class RubrosEfectoSentencia {
  id?: string;
  tipoRubro: string = '';
  tipoVotacion: string = '';
  rubrosTematicos: string[] = [];
  textoVotacion: string = '';
  ministros: string[] = [];
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  sentido?: string = '';
  tipoSentencia? : string[] = [];
}

export class RubrosExtencionInvalidez {
  id?: string;
  tipoRubro: string = '';
  tipoVotacion: string = '';
  rubrosTematicos: string[] = [];
  textoVotacion: string = '';
  ministros: string[] = [];
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
}

export class VotacionDerechosHumanosCuyaViolacionAnalizaSentencia {
  id?: string;
  tipoVotacion: string = 'votacionDerechosHumanosCuyaViolacionAnalizaSentencia';
  rubrosTematicosPrincipal: string[] = [];
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  ministros: string[] = [];
  aprobadasConcideraciones: boolean = false;
  sentidoResolucion: string = '';
  textoVotacion: string = '';
  tipoSentencia: string[] = [];
  rubrosEfectoSentencia: RubrosEfectoSentencia[] = [];
  rubrosExtencionInvalidez: RubrosExtencionInvalidez[] = [];
  actosImpugnados: string[] = [];
  derechosHumanos: string[] = [];
  metodologiaAnalisisConstitucionalidad: string = '';
}

export class RubrosEfectoSentencia2 {
  id?: string;
  tipoRubro: string = '';
  tipoVotacion: string = '';
  rubrosTematicos: string[] = [];
  textoVotacion: string = '';
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  ministros: string[] = [];
}

export class VotacionTipoVicioProcesoLegislativoAnalizadoenSentencia {
  id?: string;
  idSentencia: string = '';
  tipoVotacion: string =
    'votacionTipoVicioProcesoLegislativoAnalizadoenSentencia';
  tipoVicio: string[] = [] ;
  congresoEmitioDecreto: string = '';
  decretoImpugnado: string = '';
  rubrosTematicosPrincipal: string[] = [];
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  ministros: string[] = [];
  aprobadasConcideraciones: boolean = false;
  sentidoResolucion: string = '';
  textoVotacion: string = '';
  tipoSentencia: string[] = [];
  rubrosEfectoSentencia: RubrosEfectoSentencia2[] = [];
  rubrosExtencionInvalidez: RubrosExtencionInvalidez[] = [];
}

export class RubrosEfectoSentencia3 {
  id?: string;
  tipoRubro: string = '';
  tipoVotacion: string = '';
  rubrosTematicos: string[] = [];
  textoVotacion: string = '';
  ministros: string[] = [];
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
}

export class VotacionTipoViolacionInvacionEsferasAnalizadoenSentencia {
  id?: string;
  idSentencia: string = '';
  tipoVotacion: string = 'votacionTipoViolacionEsferasAnalizadoenSentencia';
  rubrosTematicosPrincipal: string[] = [];
  aprobadasConcideraciones: boolean = false;
  sentidoResolucion: string = '';
  textoVotacion: string = '';
  ministros: string[] = [];
  tipoSentencia: string[] = [];
  rubrosEfectoSentencia: RubrosEfectoSentencia3[] = [];
  rubrosExtencionInvalidez: RubrosExtencionInvalidez[] = [];
  actosImpugnados: string[] = [];
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  metodologiaAnalisisConstitucionalidad: string = '';
  tipoViolacionInvacionEsferas: string = '';
}

export class RubrosEfectoSentencia4 {
  id?: string;
  tipoRubro: string = '';
  tipoVotacion: string = '';
  rubrosTematicos: string[] = [];
  textoVotacion: string = '';
  votacionFavor: string[] = [];
  ministros: string[] = [];
  votacionEnContra: string[] = [];
}

export class VotacionTipoViolacionInvacionPoderesAnalizadoenSentencia {
  id?: string;
  idSentencia: string = '';
  tipoVotacion: string =
    'votacionTipoViolacionInvacionPoderesAnalizadoenSentencia';
  rubrosTematicosPrincipal: string[] = [];
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  aprobadasConcideraciones: boolean = false;
  sentidoResolucion: string = '';
  textoVotacion: string = '';
  ministros: string[] = [];
  tipoSentencia: string[] = [];
  rubrosEfectoSentencia: RubrosEfectoSentencia4[] = [];
  rubrosExtencionInvalidez: RubrosEfectoSentencia4[] = [];
  actosImpugnados: string[] = [];
  metodologiaAnalisisConstitucionalidad: string = '';
  tipoViolacionInvacionPoderes: string = '';
}

export class SentenciaRegistro {
  id?: string;
  tipoAsunto: string = '';
  numeroExpediente: string = '';
  organoRadicacion: string = '';
  fechaResolucion: string = '';
  ponente: string = '';
  tipoPersonaPromovente: string = '';
  tipoPersonaJuridicoColectiva: string = '';
  tipoViolacionPlanteadaenDemanda: string = '';
  derechosHumanosCuyaViolacionPlantea: string[] = [];
  violacionOrganicaPlanteada: string[] = [];
  tipoViciosProcesoLegislativo: string[] = [];
  tipoViolacionInvacionEsferas: string = '';
  tipoViolacionInvacionPoderes: string = '';
  votacionCausasImprocedenciaySobreseimientoAnalizadas: VotacionCausasImprocedenciaySobreseimientoAnalizada[] =
    [];
  votacionDerechosHumanosCuyaViolacionAnalizaSentencia: VotacionDerechosHumanosCuyaViolacionAnalizaSentencia[] =
    [];
  votacionTipoVicioProcesoLegislativoAnalizadoenSentencia: VotacionTipoVicioProcesoLegislativoAnalizadoenSentencia[] =
    [];
  votacionTipoViolacionInvacionEsferasAnalizadoenSentencia: VotacionTipoViolacionInvacionEsferasAnalizadoenSentencia[] =
    [];
  votacionTipoViolacionInvacionPoderesAnalizadoenSentencia: VotacionTipoViolacionInvacionPoderesAnalizadoenSentencia[] =
    [];
  violacionOrganicaAnalizadaenSentencia: string[] = [];
  tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia: string = '';
  tipoViolacionInvacionPoderesAnalizadoenSentencia: string = '';

  materiaAnalizadaInvasionPoderes: string = '';
  materiaAnalizadaInvasionEsferas: string = '';

  momentoSurteEfectoSentencia: string = '';
  numeroVotacionesRealizadas: number = 0;
  numeroVotacionesRegistradas: number = 0;
  tramiteEngrose: string = '';
  diasTranscurridosEntreDictadoSentenciayFirmaEngrose: string = '';
  fechaSentencia: string = '';
  fechaFirmaEngrose: string = '';
  derechosHumanosCuyaViolacionPlanteaByDerechosHumanos: string = '';
  totalVotacionCausasImprocedenciaySobreseimientoAnalizadas: number = 0;
  totalVotacionDerechosHumanosCuyaViolacionAnalizaSentencia: number = 0;
  totalVotacionTipoVicioProcesoLegislativoAnalizadoenSentencia: number = 0;
  totalVotacionTipoViolacionInvacionEsferasAnalizadoenSentencia: number = 0;
  totalVotacionTipoViolacionInvacionPoderesAnalizadoenSentencia: number = 0;
}
