import { RubrosTematico } from "./sentenciaModel"

export class VotacionTipoVicioPlanteado {
    id?: string
    tipoVotacion: string = "votacionTipoVicioProcesoLegislativoAnalizadoenSentencia"
    tipoVicio: string[] = [];
    congresoEmitioDecreto: string = ""
    decretoImpugnado: string = ""
    rubrosTematicosPrincipal: string[] = [];
    rubrosTematicos: RubrosTematico[] = [];
    votacionFavor: string[] = [];
    votacionEnContra: string[] = [];
    aprobadasConcideraciones: boolean = false;
    sentidoResolucion: string = "";
    textoVotacion: string = "";
    tipoSentencia: string[] = [];
    rubrosEfectoSentencia: RubrosTematico[] = [];
    rubrosExtencionInvalidez: RubrosTematico[] = [];
    actosImpugnados: string[] = [];
    votacionRubrosTematicos: string[] = [];
    articulosImpugnados: string[] = [];
    derechosHumanos: string[] = [];
    metodologiaAnalisisConstitucionalidad: string = "";
    tipoViolacionInvacionEsferas: string = "";
    tipoViolacionInvacionPoderes: string = "";
    numeroVotos: number = 0;
}

