export class Sentencia {
  id?: string;
  tipoAsunto: string = '';
  numeroExpediente: string = '';
  organoRadicacion: string = '';
  fechaResolucion: string = '';
  ponente: string = '';
  tipoPersonaPromovente: string = '';
  tipoPersonaJuridicoColectiva: string = '';
  tipoViolacionPlanteadaenDemanda: string = '';
  derechosHumanosCuyaViolacionPlantea: string[] = [];
  derechosHumanosCuyaViolacionPlanteaByDerechosHumanos: string[] = [];
  violacionOrganicaPlanteada: string[] = [];
  tipoViciosProcesoLegislativo: string[] = [];
  tipoViolacionInvacionEsferas: string = '';
  tipoViolacionInvacionPoderes: string = '';
  votacionCausasImprocedenciaySobreseimientoAnalizadas: string[] = [];
  votacionDerechosHumanosCuyaViolacionAnalizaSentencia: string[] = [];
  violacionOrganicaAnalizadaenSentencia: string[] = [];
  votacionTipoVicioProcesoLegislativoAnalizadoenSentencia: string[] = [];
  tipoViolacionInvacionEsferasMateriaAnalizadaenSentencia: string = '';
  votacionTipoViolacionInvacionEsferasAnalizadoenSentencia: string[] = [];
  tipoViolacionInvacionPoderesAnalizadoenSentencia: string;
  votacionTipoViolacionInvacionPoderesAnalizadoenSentencia: string[] = [];
  momentoSurteEfectoSentencia: string = '';
  numeroVotacionesRealizadas: number = 0;
  numeroVotacionesRegistradas: number = 0;
  tramiteEngrose: string = '';
  diasTranscurridosEntreDictadoSentenciayFirmaEngrose: string = '';
  fechaSentencia: string = '';
  fechaFirmaEngrose: string = '';
}

export class Votacion {
  id: string;
  tipoVotacion: string;
  tipoVicio: string[];
  congresoEmitioDecreto: string;
  decretoImpugnado: string;
  rubrosTematicos: string[];
  votacionFavor: string[];
  votacionEnContra: string[];
  aprobadasConcideraciones: boolean;
  sentidoResolucion: string;
  textoVotacion: string;
  tipoSentencia: string[];
  rubrosEfectoSentencia: string[];
  rubrosExtencionInvalidez: string[];
  actosImpugnados: any[];
  votacionRubrosTematicos: any[];
  articulosImpugnados: any[];
  derechosHumanos: any[];
  metodologiaAnalisisConstitucionalidad?: any;
  tipoViolacionInvacionEsferas?: any;
  tipoViolacionInvacionPoderes?: any;
}

export class SentenciaMetadatos {
  id: string;
  idEngrose: string;
  asuntoId: string;
  archivoURL: string;
  idSentencia: string;
}

export class DatosSentencia {
  sentencia: Sentencia = new Sentencia();
  votacion: Votacion = new Votacion();
  sentencia_metadatos: SentenciaMetadatos = new SentenciaMetadatos();
}

export class Sort {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
}

export class Pageable {
  sort: Sort;
  offset: number;
  pageSize: number;
  pageNumber: number;
  unpaged: boolean;
  paged: boolean;
}

export class Sort2 {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
}

export class AllSentenciaModel {
  content: Sentencia[] = [];
  pageable: Pageable;
  totalElements: number;
  totalPages: number;
  last: boolean;
  size: number;
  number: number = 1;
  sort: Sort2;
  numberOfElements: number;
  first: boolean;
  empty: boolean;
}

export class RubrosTematico {
  id?: string;
  tipoRubro: string = 'rubrosTematicos';
  rubrosTematicos: string[] = [];
  analisisProcedencia: string[] = [];
  tipoVotacion?: string;
}

export class VotacionCausa {
  id?: string;
  idSentencia: string
  tipoVotacion: string = 'votacionCausasImprocedenciaySobreseimientoAnalizadas';
  rubrosTematicos: RubrosTematico[] = [];
  articulosImpugnados: string[] = [];
  textoVotacion: string = '';
  sentidoResolucion: string = '';
  votacionFavor: string[] = [];
  votacionEnContra: string[] = [];
  ministros: string[] = [];
}

export class CausasImprocedenciaSobreseimientoAnalizadas {
  idSentencia?: string;
  votacion: VotacionCausa = new VotacionCausa();
}
