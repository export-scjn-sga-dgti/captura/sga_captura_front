    export interface Content {
        tipoAsunto: string;
        numExpediente: string;
        secretarios: string[];
        editores: string[];
        sesiones: string[];
        acumulados: any[];
        temasFondo: string[];
        temasProcesales: string[];
        acumulada: boolean;
        ca: boolean;
    }

    export interface Sort {
        sorted: boolean;
        unsorted: boolean;
        empty: boolean;
    }

    export interface Pageable {
        sort: Sort;
        offset: number;
        pageSize: number;
        pageNumber: number;
        unpaged: boolean;
        paged: boolean;
    }

    export interface Sort2 {
        sorted: boolean;
        unsorted: boolean;
        empty: boolean;
    }

    export interface Seguimiento {
        content: Content[];
        pageable: Pageable;
        totalElements: number;
        totalPages: number;
        last: boolean;
        size: number;
        number: number;
        sort: Sort2;
        numberOfElements: number;
        first: boolean;
        empty: boolean;
    }



export interface FilterSeguimiento{
    asuntoAbordado: string
    numeroExpediente: string
    secretario: string
    anio: string
}
