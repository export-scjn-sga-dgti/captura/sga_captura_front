export class Acuerdo {
	id:string = '';
	rubro:string;
	fechaAprobacion:Date
	ambitoIncide:string;
	vigencia:string;
	fechaPublicacionDOF:Date;
	clasificacionMateriaRegulacion:string[] = [];
	organoEmisor:string ;
	ministroPresidente:string;
	votacion:string;
	votacionAcuerdoFavor:string[] = [];
    votacionAcuerdoEnContra:string[] = [];
	ministrosAusentes:string[] = [];
	temasEspecificosRegulacion:string[] = [];
	porcentaje:number = 0;
}

export class AllAcuerdoModel {
    content: Acuerdo[] = [];
    empty: boolean;
    first: boolean;
    last: boolean;
    number:number;
    numberOfElements:number;
    pageable:Pageable;
    size:number;
    sort:Sort2;
    totalElements:number;
    totalPages:number;

}

export class Sort {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
}

export class Pageable {
    sort: Sort;
    offset: number;
    pageSize: number;
    pageNumber: number;
    unpaged: boolean;
    paged: boolean;
}

export class Sort2 {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
}

export class Search{

    rubro: string;
    fecha:Date;
    materiaRegulacion: string;
    vigencia: string;

}
