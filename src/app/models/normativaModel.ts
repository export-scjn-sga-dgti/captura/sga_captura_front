export class DiversaNormativa{
    diversaNormativa: Normativa;
    materias: Materia[];
}

export class Normativa {
    id:string;
    fechaAprobacion: Date;
    ambitoIncide: string;
    tipoInstrumento: string;
    tipoCircular: string;
    numero: number;
    ministroPresidente: string;
    urlIntranet: string;
    documentos: string[];
}

export class Materia {
    id:string;
	idDiversaNormativa:string;
    materiaRegulacion:string;
    temas:string[];
}

export class AllNormativaModel{

    content : Normativa[] = [];
    empty: boolean;
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    pageable: Pageable;
    size: number;
    sort: Sort2;
    totalElements: number;
    totalPages: number;
}

export class Sort {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
}

export class Pageable {
    sort: Sort;
    offset: number;
    pageSize: number;
    pageNumber: number;
    unpaged: boolean;
    paged: boolean;
}

export class Sort2 {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
}

export class SearchNormativa{
    tipoInstrumento: string;
    numero: number;
}
