export class CommitMetaData {
  author: string;
  commitDate: string;
}

export class Content {
  id: string;
  beginState?: any;
  state: any;
  commitMetaData: CommitMetaData;
  type: string;
  seccion: string;
  resumen: string;
}

export class Sort {
  sorted: boolean;
  unsorted: boolean;
  empty: boolean;
}

export class Pageable {
  sort: Sort;
  offset: number;
  pageNumber: number;
  pageSize: number;
  unpaged: boolean;
  paged: boolean;
}

export class Sort2 {
  sorted: boolean;
  unsorted: boolean;
  empty: boolean;
}

export class BitacoraModel {
  content: Content[];
  pageable: Pageable;
  totalElements: number;
  totalPages: number;
  last: boolean;
  size: number;
  number: number;
  sort: Sort2;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}
