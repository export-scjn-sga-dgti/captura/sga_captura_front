import { Modules } from "../enum/modules.enum"

export class ViewsModules {
    [Modules.normativa]: Normativa = new Normativa;
    [Modules.votos]: Votos = new Votos;
    [Modules.taquigraficas]: Taquigrafica = new Taquigrafica;
    [Modules.acuerdos]: Acuerdos = new Acuerdos;
    [Modules.admin]: Administrador = new Administrador;
    [Modules.sentencia]: Sentencia = new Sentencia;
    [Modules.seguimiento]: Seguimiento = new Seguimiento;
    [Modules.catalogo]: Catalogo = new Catalogo;
}

export class Normativa {
    fechaAprobacion: boolean
    ambitoIncide: boolean
    tipoInstrumento: boolean
    tipoCircular: boolean
    ministroPresidente: boolean
    materiaregulacion: boolean
    temasRegulacion: boolean
    readOnly: boolean
}

export class Votos {
    tipoAsunto: boolean
    numeroExpediente: boolean
    tipoVoto: boolean
    fechaResolucion: boolean
    ministraoElabora: boolean
    temaAborda: boolean
    tipoTemaProcesal: boolean
    metodologia: boolean
    readOnly: boolean
}

export class Taquigrafica {
    tipoInstrumento: boolean
    duracionSesio: boolean
    numeroMinistrasMinistrosParticiparonEnSesio: boolean
    nombreMinistrasMinistrosPresentesEnSesio: boolean
    nombreMinistrasMinistrosAusentesEnSesio: boolean
    asuntoAbordad: boolean
    numeroExpedient: boolean
    temasProcesale: boolean
    temasFond: boolean
    numeroMinistrosParticiparonEnAsunt: boolean
    hasPresidenteDecan: boolean
    ponent: boolean
    readOnl: boolean
}

export class Acuerdos {
    fechaAprobacion: boolean
    ambitoIncide: boolean
    vigencia: boolean
    fechaDOF: boolean
    clasificacionMateria: boolean
    temasRegulacion: boolean
    organoEmisor: boolean
    ministroPresidente: boolean
    votacionFavor: boolean
    votacionContra: boolean
    votacion: boolean
    readOnly: boolean
}

export class Administrador {
    readOnly: boolean
}

export class Sentencia {
    readOnly: boolean
}

export class Seguimiento {
    readOnly: boolean
}

export class Catalogo {
    readOnly: boolean
}