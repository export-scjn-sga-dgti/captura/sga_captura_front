export class Asunto {
  idAsunto?: string;
  tipoAsunto: string;
  numeroExpediente: string;
}

export class Content {
  id: string;
  fechaSesion: string;
  duracionSesion: string;
  numeroAsuntos: number;
  asuntos: Asunto[] = [];
  porcentaje: number;
}

export class Sort {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
}

export class Pageable {
  sort: Sort;
  offset: number;
  pageSize: number;
  pageNumber: number;
  paged: boolean;
  unpaged: boolean;
}

export class Sort2 {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
}

export class AllTaquigraficas {
  content: Content[] = [];
  pageable: Pageable;
  last: boolean;
  totalElements: number;
  totalPages: number;
  size: number;
  number: number;
  sort: Sort2;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}

export class CreateTaquigrafica {
  id?: string;
  fechaSesion: string = '';
  duracionSesion: string = '';
  numeroMinistrasMinistrosParticiparonEnSesion: number = 0;
  nombreMinistrasMinistrosPresentesEnSesion: string[] = [];
  nombreMinistrasMinistrosAusentesEnSesion: string[] = [];
  hasPresidenteDecano: boolean = false;
  nombrePresidenteDecano: string = '';
  asuntosAbordados: any[] = [];
  porcentaje?: string;
}

export class Asuntos {
  id?: string;
  acumulada: boolean = false;
  ca: boolean = false;
  asuntoAbordado: string;
  numeroExpediente: string;
  acumulados: string[] = [];
}

export class NumeroMinistrosParticiparonEnAsunto {
  nombre: string;
  cantidad: number;
}

export class VtAsuntos {
  id?: string;
  idAsunto?: string;
  idVtaquigrafica?: string;
  ponente: string;
  temasProcesales: string[] = [];
  temasFondo: string[] = [];
  acumulados: string[] = [];
  nombresMinistrosParticiparonEnAsunto: string[] = [];
  numeroMinistrosParticiparonEnAsunto: NumeroMinistrosParticiparonEnAsunto[] =
    [];
}

export class CreateAsuntoAbordado {
  idVersionTaqui?: string;
  asunto: Asuntos = new Asuntos();
  vt_asuntos: VtAsuntos = new VtAsuntos();
}

export class Taquigrafica {
  vtaq: CreateTaquigrafica;
  asuntos: Asunto[] = [];
}
