import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'classCommit',
})
export class ClassCommitPipe implements PipeTransform {
  transform(value: string, ...args: unknown[]): string {
    
    switch (value) {
      case 'Save':
        return ('label-bitacora save');
      case 'Update':
        return ('label-bitacora update');
      case 'Delete':
        return ('label-bitacora delete');
    }
  }
}
