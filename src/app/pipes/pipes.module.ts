import { NgModule } from '@angular/core';
import { NameAsuntosPipe } from './name-asuntos.pipe';
import { NameciCommitPipe } from './nameci-commit.pipe';
import { ClassCommitPipe } from './class-commit.pipe';
import { NameTypePipe } from './name-type.pipe';
import { CountAsuntosPipe } from './count-asuntos.pipe';
import { ConvertRomanoPipe } from './convert-romano.pipe';

@NgModule({
  declarations: [
    NameAsuntosPipe,
    NameciCommitPipe,
    ClassCommitPipe,
    NameTypePipe,
    CountAsuntosPipe,
    ConvertRomanoPipe
  ],
  exports: [
    NameAsuntosPipe,
    NameciCommitPipe,
    ClassCommitPipe,
    NameTypePipe,
    CountAsuntosPipe,
    ConvertRomanoPipe
  ],
})
export class PipesModule { }
