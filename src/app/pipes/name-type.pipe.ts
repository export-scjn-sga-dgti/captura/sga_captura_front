import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameType',
})
export class NameTypePipe implements PipeTransform {
  transform(value: string, ...args: unknown[]): unknown {
    switch (value) {
      case 'Update':
        return (value = 'Modificación');
      case 'Save':
        return (value = 'Creación');
      case 'Delete':
        return (value = 'Eliminado');
      case 'Publicacion':
        return (value = 'Publicación');
      case 'Sesion':
        return (value = 'Sesión');
      case 'Catalogos':
        return (value = 'Catálogos');
      case 'sentencia':
        return (value = 'Sentencias');
      case 'votos':
        return (value = 'Votos');
      case 'acuerdos-generales':
        return (value = 'Acuerdos generales');
      case 'diversa-normativa':
        return (value = 'Diversa Normativa');
      case 'v-taquigraficas':
        return (value = 'Versiones Taquigráficas');
      case 'catalogos':
        return (value = 'Catálogos');
      case 'bitacora':
        return (value = 'Bitácora');
      case 'usuarios':
        return (value = 'Usuarios');
      case 'asignacion-asuntos':
        return (value = 'Asignación asuntos');
      case 'exploracion-datos':
        return (value = 'Exploración de datos');
    }

    return value;
  }
}
