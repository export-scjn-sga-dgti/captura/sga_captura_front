import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertRomano'
})
export class ConvertRomanoPipe implements PipeTransform {

  transform(num: number, ...args: unknown[]): unknown {
    var lookup = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1},roman:string = '',i:string;
      for ( i in lookup ) {
        while ( num >= lookup[i] ) {
          roman += i;
          num -= lookup[i];
        }
      }
      return roman;
  }

}
