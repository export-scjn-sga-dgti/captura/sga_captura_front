import { Pipe, PipeTransform } from '@angular/core';
import { Content } from '../models/asignacion';

@Pipe({
  name: 'countAsuntos'
})
export class CountAsuntosPipe implements PipeTransform {

  transform(value: Content, ...args: unknown[]): unknown {
    let count: number = 0
    value.modulos.forEach(
      data =>{
        count += data.asuntos.length
      }
    )
    return count;
  }

}
