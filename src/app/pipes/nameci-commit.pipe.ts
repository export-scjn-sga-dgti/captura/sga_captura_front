import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameciCommit',
})
export class NameciCommitPipe implements PipeTransform {
  names: Map<string, string> = new Map<string, string>();

  constructor() {
    this.names.set('Save', 'Creación');
    this.names.set('Update', 'Modificación');
    this.names.set('Delete', 'Eliminado');
    this.names.set('Publicacion', 'Publicación');
  }
  transform(value: any, ...args: unknown[]): unknown {
    let text = '';
    if (value.type) {
      text =
        this.names.get(value.type) +
        ' - ' +
        value.commitMetaData.commitDate +
        ' - ' +
        value.seccion +
        ' - ' +
        value.commitMetaData.author;
    } else {
      text = '';
    }

    return text;
  }
}
