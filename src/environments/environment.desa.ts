const api: string = 'https://bj.scjn.pjf.gob.mx:8443/sgav2-desa';
const apiProcesamineto: string = 'https://bj.scjn.pjf.gob.mx:8443/sga-analisis-texto/';

export const environment = {
  production: true,
  url: api,
  urlProcesamiento: apiProcesamineto,
  activeKeycloak: true,
};
